﻿using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models;
using Healthgrades.GreyWolfLibrary.Models.Constants;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Healthgrades.GreyWolfLibrary.Models.Constants
{
    internal static class Account
    {
        public const string FamilyNameKey = "FamilyName";
        public const string AccountIdKey = "AccountId";
        public const string IsActiveKey = "AccountIsActive";
    }

    internal static class Person
    {
        public const string RelationshipKey = "PersonRelationship";
        public const string IsActiveKey = "PersonIsActive";
        public const string AddressLine1Key = "PersonAddressLine1";
        public const string AddressLine2Key = "PersonAddressLine2";
        public const string AvatarImagePathKey = "AvatarImagePath";
        public const string CityKey = "PersonCity";
        public const string StateKey = "PersonState";
        public const string ZipCodeKey = "PersonZipCode";
    }

    public class Provider
    {
        public const string PersonProviderInfoTypeKey = "PersonProvider";
        public const string WellnessPlanIdKey = "WellnessPlanId";
        public const string PersonProviderIdKey = "PersonProviderId";
    }

    public class ProviderNotes
    {
        public const string CommentsKey = "Comments";
    }

    public class Facility
    {
        public const string PersonFacilityInfoTypeKey = "PersonFacility";
        public const string PersonFacilityIdKey = "PersonFacilityId";
    }

    public class Insurance
    {
        public const string PersonInsuranceInfoTypeKey = "PersonInsurance";
        public const string CardPathFrontKey = "CardPathFront";
        public const string CardPathBackKey = "CardPathBack";
        public const string InsuranceCodeKey = "InsuranceCode";
        public const string InsuranceNameKey = "InsuranceName";
        public const string InsuranceCarrierKey = "InsuranceCarrier";
        public const string InsurancePlanNameKey = "InsurancePlanName";
        public const string InsuranceSubscriberIdKey = "InsuranceSubscriberId";
        public const string InsuranceIsDefault = "InsuranceIsDefault";
    }

    public class Address
    {
        public const string PersonAddressInfoTypeKey = "PersonAddress";
        public const string PersonAddressNameKey = "AddressName";
        public const string PersonAddressKey = "Address";
        public const string PersonAddressCityKey = "AddressCity";
        public const string PersonAddressStateKey = "AddressState";
        public const string PersonAddressZipKey = "AddressZip";
        public const string PersonAddressIsDefaultKey = "AddressIsDefault";
    }
}

namespace Healthgrades.GreyWolfLibrary.Helpers
{
    public class AccountTranslator : IAccountTranslator
    {
        private readonly IProviderSolrRepository _ProviderSolrRepository;
        private readonly IAutosuggestSolrRepository _AutosuggestSolrRepository;
        private readonly IOASServiceRepository _OASServiceRepository;
        private readonly IPESServiceRepository _PESServiceRepository;
        private readonly ICognitoRepository _CognitoRepository;
        private ILogger<AccountTranslator> LoggingService;

        public AccountTranslator(IProviderSolrRepository providerSolrRepository,
            IAutosuggestSolrRepository autosuggestSolrRepository,
            IOASServiceRepository oasServiceRepository,
            IPESServiceRepository pesServiceRepository,
            ICognitoRepository cognitoRepository,
            ILogger<AccountTranslator> loggingService)
        {
            _ProviderSolrRepository = providerSolrRepository;
            _AutosuggestSolrRepository = autosuggestSolrRepository;
            _OASServiceRepository = oasServiceRepository;
            _PESServiceRepository = pesServiceRepository;
            _CognitoRepository = cognitoRepository;
            LoggingService = loggingService;
        }

        public HealthgradesProfile ToHealthgradesProfile(Models.Account newAccount)
        {
            if (newAccount == null) return null;

            if (newAccount.Persons != null)
                newAccount = GetPersonProviderSpecialties(newAccount);

            var resultProfile = new HealthgradesProfile();

            //TODO: Puru to review HGProfile from Account

            resultProfile.UserProfile.User.FirstName = newAccount.Persons.FirstOrDefault().Identity.FirstName;
            resultProfile.UserProfile.User.LastName = newAccount.Persons.FirstOrDefault().Identity.LastName;
            resultProfile.UserProfile.User.Gender =
                (Gender)Enum.Parse(typeof(Gender), newAccount.Persons.FirstOrDefault().Identity.Gender);
            resultProfile.UserProfile.User.BirthDate = newAccount.Persons.FirstOrDefault().Identity.DOB;

            resultProfile.UserProfile.User.Id = Guid.Empty.ToString();
            resultProfile.UserProfile.AccountData = new List<EntityInfo> { new EntityInfo() };

            resultProfile.DeviceTokens = newAccount.DeviceTokens;
            resultProfile.UserProfile.AccountData.FirstOrDefault().Items = new List<Info>
            {
                new Info
                {
                    Label = Models.Constants.Account.AccountIdKey,
                    Value = Convert.ToString(newAccount.AccountId),
                    TimestampType = TimestampType.Updated,
                    TimestampDate = DateTime.UtcNow
                },
                new Info
                {
                    Label = Models.Constants.Account.FamilyNameKey,
                    Value = newAccount.FamilyName,
                    TimestampType = TimestampType.Updated,
                    TimestampDate = DateTime.UtcNow
                },
                new Info
                {
                    Label = Models.Constants.Account.IsActiveKey,
                    Value = Convert.ToString(newAccount.IsActive),
                    TimestampType = TimestampType.Updated,
                    TimestampDate = DateTime.UtcNow
                }
            };

            resultProfile.UserProfile.User.ProviderIds = newAccount.Persons.Where(x => x.PersonId == Guid.Empty)
                ?.FirstOrDefault()?.Profile?.Providers?.Select(provider => new EntityInfo
                {
                    Id = provider.ProviderCode,
                    InfoType = Provider.PersonProviderInfoTypeKey,
                    TimestampType = TimestampType.Updated,
                    TimestampDate = DateTime.UtcNow,
                    Items = new List<Info>
                    {
                        new Info
                        {
                            Label = Provider.PersonProviderIdKey,
                            Value = Convert.ToString(provider.PersonProviderId),
                            TimestampType = TimestampType.Updated,
                            TimestampDate = DateTime.UtcNow
                        },
                        new Info
                        {
                            LabelId = LabelId.Comments,
                            Label = ProviderNotes.CommentsKey,
                            Value = provider.Note,
                            TimestampType = TimestampType.Updated,
                            TimestampDate = DateTime.UtcNow
                        }
                    }
                }).ToList();

            if (resultProfile.UserProfile.User.ProviderIds == null)
                resultProfile.UserProfile.User.ProviderIds = new List<EntityInfo>();

            resultProfile.UserProfile.User.UserData = new List<EntityInfo>
            {
                new EntityInfo
                {
                    Items = new List<Info>
                    {
                        new Info
                        {
                            Label = Models.Constants.Person.AddressLine1Key,
                            Value = newAccount.Persons.Where(x => x.PersonId == Guid.Empty)?.FirstOrDefault()?.Identity
                                .AddressLine1,
                            TimestampType = TimestampType.Updated,
                            TimestampDate = DateTime.UtcNow,
                            LabelId = LabelId.Address
                        },
                        new Info
                        {
                            Label = Models.Constants.Person.AddressLine2Key,
                            Value = newAccount.Persons.Where(x => x.PersonId == Guid.Empty)?.FirstOrDefault()?.Identity
                                .AddressLine2,
                            TimestampType = TimestampType.Updated,
                            TimestampDate = DateTime.UtcNow,
                            LabelId = LabelId.Address
                        },
                        new Info
                        {
                            Label = Models.Constants.Person.AvatarImagePathKey,
                            Value = newAccount.Persons.Where(x => x.PersonId == Guid.Empty)?.FirstOrDefault()?.Identity
                                .AvatarImagePath,
                            TimestampType = TimestampType.Updated,
                            TimestampDate = DateTime.UtcNow
                        },
                        new Info
                        {
                            Label = Models.Constants.Person.CityKey,
                            Value = newAccount.Persons.Where(x => x.PersonId == Guid.Empty)?.FirstOrDefault()?.Identity
                                .City,
                            TimestampType = TimestampType.Updated,
                            TimestampDate = DateTime.UtcNow,
                            LabelId = LabelId.Address
                        },
                        new Info
                        {
                            Label = Models.Constants.Person.RelationshipKey,
                            Value = newAccount.Persons.Where(x => x.PersonId == Guid.Empty)?.FirstOrDefault()?.Identity
                                .Relationship,
                            TimestampType = TimestampType.Updated,
                            TimestampDate = DateTime.UtcNow
                        },
                        new Info
                        {
                            Label = Models.Constants.Person.StateKey,
                            Value = newAccount.Persons.Where(x => x.PersonId == Guid.Empty)?.FirstOrDefault()?.Identity
                                .State,
                            TimestampType = TimestampType.Updated,
                            TimestampDate = DateTime.UtcNow,
                            LabelId = LabelId.Address
                        },
                        new Info
                        {
                            Label = Models.Constants.Person.ZipCodeKey,
                            Value = newAccount.Persons.Where(x => x.PersonId == Guid.Empty)?.FirstOrDefault()?.Identity
                                .ZipCode,
                            TimestampType = TimestampType.Updated,
                            TimestampDate = DateTime.UtcNow,
                            LabelId = LabelId.Address
                        }
                    }
                }
            };
            foreach (var person in newAccount.Persons.Where(x => x.PersonId != Guid.Empty))
            {
                resultProfile.UserProfile.FamilyMembers.Add(new UserInfo
                {
                    BirthDate = person.Identity.DOB,
                    FirstName = person.Identity.FirstName,
                    LastName = person.Identity.LastName,
                    Gender = (Gender)Enum.Parse(typeof(Gender), person.Identity.Gender),
                    TimestampDate = DateTime.UtcNow,
                    TimestampType = TimestampType.Updated,
                    Id = Convert.ToString(person.PersonId),
                    UserData = new List<EntityInfo>
                    {
                        new EntityInfo
                        {
                            Items = new List<Info>
                            {
                                new Info
                                {
                                    Label = Models.Constants.Person.AddressLine1Key,
                                    Value = person.Identity.AddressLine1,
                                    TimestampType = TimestampType.Updated,
                                    TimestampDate = DateTime.UtcNow,
                                    LabelId = LabelId.Address
                                },
                                new Info
                                {
                                    Label = Models.Constants.Person.AddressLine2Key,
                                    Value = person.Identity.AddressLine2,
                                    TimestampType = TimestampType.Updated,
                                    TimestampDate = DateTime.UtcNow,
                                    LabelId = LabelId.Address
                                },
                                new Info
                                {
                                    Label = Models.Constants.Person.AvatarImagePathKey,
                                    Value = person.Identity.AvatarImagePath,
                                    TimestampType = TimestampType.Updated,
                                    TimestampDate = DateTime.UtcNow
                                },
                                new Info
                                {
                                    Label = Models.Constants.Person.CityKey,
                                    Value = person.Identity.City,
                                    TimestampType = TimestampType.Updated,
                                    TimestampDate = DateTime.UtcNow,
                                    LabelId = LabelId.Address
                                },
                                new Info
                                {
                                    Label = Models.Constants.Person.RelationshipKey,
                                    Value = person.Identity.Relationship,
                                    TimestampType = TimestampType.Updated,
                                    TimestampDate = DateTime.UtcNow
                                },
                                new Info
                                {
                                    Label = Models.Constants.Person.StateKey,
                                    Value = person.Identity.State,
                                    TimestampType = TimestampType.Updated,
                                    TimestampDate = DateTime.UtcNow,
                                    LabelId = LabelId.Address
                                },
                                new Info
                                {
                                    Label = Models.Constants.Person.ZipCodeKey,
                                    Value = person.Identity.ZipCode,
                                    TimestampType = TimestampType.Updated,
                                    TimestampDate = DateTime.UtcNow,
                                    LabelId = LabelId.Address
                                }
                            }
                        }
                    },

                    ProviderIds = person.Profile?.Providers?.Select(provider => new EntityInfo
                    {
                        Id = provider.ProviderCode,
                        InfoType = Provider.PersonProviderInfoTypeKey,
                        TimestampType = TimestampType.Updated,
                        TimestampDate = DateTime.UtcNow,

                        Items = new List<Info>
                        {
                            new Info
                            {
                                Label = Provider.PersonProviderIdKey,
                                Value = Convert.ToString(provider.PersonProviderId),
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            },
                            new Info
                            {
                                LabelId = LabelId.Comments,
                                Label = ProviderNotes.CommentsKey,
                                Value = provider.Note,
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            }
                        }
                    }).ToList(),

                    HospitalIds = person.Profile?.Facilities?.Select(hospital => new EntityInfo
                    {
                        Items = new List<Info>
                        {
                            new Info
                            {
                                Label = Facility.PersonFacilityIdKey,
                                Value = Convert.ToString(hospital.PersonFacilityId),
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            },
                            new Info
                            {
                                Label = Facility.PersonFacilityInfoTypeKey,
                                Value = hospital.FacilityCode,
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            }
                        }
                    }).ToList(),

                    Addresses = person.Profile?.Addresses?.Select(address => new EntityInfo
                    {
                        Items = new List<Info>
                        {
                            new Info
                            {
                                Label = Address.PersonAddressInfoTypeKey,
                                Value = Convert.ToString(address.PersonAddressId),
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            },
                            new Info
                            {
                                Label = Address.PersonAddressNameKey,
                                Value = address.AddressName,
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            },
                            new Info
                            {
                                Label = Address.PersonAddressKey,
                                Value = address.Address,
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            },
                            new Info
                            {
                                Label = Address.PersonAddressCityKey,
                                Value = address.City,
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            },
                            new Info
                            {
                                Label = Address.PersonAddressStateKey,
                                Value = address.State,
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            },
                            new Info
                            {
                                Label = Address.PersonAddressZipKey,
                                Value = address.Zip,
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            },
                            new Info
                            {
                                Label = Address.PersonAddressIsDefaultKey,
                                Value = address.IsDefault.ToString(),
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            }
                        }
                    }).ToList(),

                    Payors = person.Profile?.Insurances?.Select(insurance => new EntityInfo
                    {
                        Items = new List<Info>
                        {
                            new Info
                            {
                                Label = Insurance.PersonInsuranceInfoTypeKey,
                                Value = Convert.ToString(insurance.PersonInsuranceId),
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            },
                            new Info
                            {
                                Label = Insurance.InsuranceNameKey,
                                Value = insurance.InsuranceName,
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            },
                            new Info
                            {
                                Label = Insurance.InsuranceCodeKey,
                                Value = insurance.InsuranceCode,
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            },
                            new Info
                            {
                                Label = Insurance.CardPathFrontKey,
                                Value = insurance.CardPathFront,
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            },
                            new Info
                            {
                                Label = Insurance.CardPathBackKey,
                                Value = insurance.CardPathBack,
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            },
                            new Info
                            {
                                Label = Insurance.InsuranceCarrierKey,
                                Value = insurance.InsuranceCarrier,
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            },
                            new Info
                            {
                                Label = Insurance.InsurancePlanNameKey,
                                Value = insurance.InsurancePlanName,
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            },
                            new Info
                            {
                                Label = Insurance.InsuranceSubscriberIdKey,
                                Value = insurance.InsuranceSubscriberId,
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            },
                            new Info
                            {
                                Label = Insurance.InsuranceIsDefault,
                                Value = insurance.IsDefault.ToString(),
                                TimestampType = TimestampType.Updated,
                                TimestampDate = DateTime.UtcNow
                            }
                        }
                    }).ToList()
                });

                if (person.Profile?.Providers == null)
                    resultProfile.UserProfile.FamilyMembers.FindLast(x => x.Id != Guid.Empty.ToString()).ProviderIds =
                        new List<EntityInfo>();

                if (person.Profile?.Insurances == null)
                    resultProfile.UserProfile.FamilyMembers.FindLast(x => x.Id != Guid.Empty.ToString()).Payors =
                        new List<EntityInfo>();

                if (person.Profile?.Facilities == null)
                    resultProfile.UserProfile.FamilyMembers.FindLast(x => x.Id != Guid.Empty.ToString()).HospitalIds =
                        new List<EntityInfo>();
            }

            var userProfileProviders = new List<EntityInfo>();
            foreach (var familyMember in resultProfile.UserProfile.FamilyMembers)
                if (familyMember.ProviderIds != null && familyMember.ProviderIds.Count > 0)
                    userProfileProviders = Merge(userProfileProviders, familyMember.ProviderIds,
                        new ProviderEntityInfoMerger());

            if (userProfileProviders.Count > 0) resultProfile.UserProfile.Providers = userProfileProviders;

            return resultProfile;
        }

        public Models.Account ToAccount(HealthgradesProfile hgProfile)
        {
            if (hgProfile == null) return null;

            var resultAccount = new Models.Account
            {
                AccountId = GetGuidValueForKeyInEntityInfoList(Models.Constants.Account.AccountIdKey,
                    hgProfile.UserProfile.AccountData),
                IsActive = true,
                AuthId = hgProfile.CognitoId,
                SessionToken = hgProfile.SessionToken,
                AccessToken = hgProfile.AccessToken,
                IdToken = hgProfile.IdToken,
                UserId = hgProfile.UserId,
                Email = hgProfile.Email,
                RefreshToken = hgProfile.RefreshToken,
                DeviceTokens = hgProfile.DeviceTokens
            };

            if (resultAccount.AccountId == Guid.Empty)
                resultAccount.AccountId = Guid.NewGuid();

            var selfPerson = ToPerson(hgProfile.UserProfile.User, hgProfile.UserProfile.Providers,
                resultAccount.AccountId, true);
            if (selfPerson != null)
            {
                selfPerson.IsActive = true;
                if (resultAccount.Persons == null) resultAccount.Persons = new List<Models.Person>();

                resultAccount.Persons.Add(selfPerson);

                resultAccount.FamilyName = selfPerson.Identity.FirstName + " " + selfPerson.Identity.LastName;
            }

            if (hgProfile.UserProfile.FamilyMembers != null && hgProfile.UserProfile.FamilyMembers.Count > 0)
                foreach (var familyMember in hgProfile.UserProfile.FamilyMembers
                    .Where(x => x.TimestampType != TimestampType.Deleted).ToList())
                {
                    var person = ToPerson(familyMember, hgProfile.UserProfile.Providers, resultAccount.AccountId,
                        false);
                    if (person != null)
                    {
                        person.IsActive = true;
                        if (resultAccount.Persons == null) resultAccount.Persons = new List<Models.Person>();
                        resultAccount.Persons.Add(person);
                    }
                }

            resultAccount = GetPersonProviderSpecialties(resultAccount);
            resultAccount.PESApproved = GetPESApproved(hgProfile.CognitoId);
            resultAccount.OASUpcoming = GetUpcomingAppointments(hgProfile.SessionToken);

            return resultAccount;
        }

        private string GetPESApproved(string cognitoId)
        {
            try
            {
                var pesRatings = _PESServiceRepository.GetPESRatings(cognitoId);
                if (pesRatings.Results == null)
                    return "0";
                return pesRatings.Results.Count(x => x.comment?.status == "Approved").ToString();
            }
            catch (Exception)
            {
                return "0";
            }
        }

        private string GetUpcomingAppointments(string sessionToken)
        {
            try
            {
                var loginResponse = _CognitoRepository.GetRefreshAccessToken(sessionToken);

                var oasAppointments = _OASServiceRepository.GetOASAppointments(loginResponse.AccessToken);
                if (oasAppointments == null)
                    return "0";
                return oasAppointments.Count(x => x.Status == "BOOKED" && x.DateTimeUtc >= DateTime.UtcNow).ToString();
            }
            catch (Exception ex)
            {
                ex.Demystify();
                LoggingService.LogError(ex, "Exception was caught at GetUpcomingAppointments for sessionToken = " + sessionToken);
                return "0";
            }
        }

        public HealthgradesProfile ToHealthgradesProfile(AccountSaveRequest accountSaveRequest,
            HealthgradesProfile existingProfile)
        {
            var resultProfile = existingProfile;
            if (resultProfile == null)
            {
                resultProfile = new HealthgradesProfile();
                resultProfile.UserProfile.User = NewDefaultSelfUser();
            }

            //Set the values from save request
            UpdateInfoValueForKeyInEntityInfoList(accountSaveRequest.FamilyName, Models.Constants.Account.FamilyNameKey,
                resultProfile.UserProfile.AccountData);
            UpdateInfoValueForKeyInEntityInfoList(accountSaveRequest.AccountId.ToString(),
                Models.Constants.Account.AccountIdKey, resultProfile.UserProfile.AccountData);
            UpdateInfoValueForKeyInEntityInfoList(accountSaveRequest.IsActive.ToString(), Models.Constants.Account.IsActiveKey,
                resultProfile.UserProfile.AccountData);

            return resultProfile;
        }

        public HealthgradesProfile MergeHealthgradesProfiles(HealthgradesProfile newProfile,
            HealthgradesProfile existingProfile)
        {
            if (existingProfile == null)
            {
                existingProfile = new HealthgradesProfile();
                existingProfile.UserProfile.User = NewDefaultSelfUser();
            }

            var pm = MagicalProfileMergingMachine.CreateMerger();
            return pm.Merge(existingProfile, newProfile);
        }

        public void UpdateUserInfo(PersonSaveRequest personSaveRequest, UserInfo existingUserInfo)
        {
            if (existingUserInfo == null) throw new ArgumentNullException("existingUserInfo");

            existingUserInfo.TimestampDate = DateTime.UtcNow;
            existingUserInfo.TimestampType = TimestampType.Updated;

            if (personSaveRequest.IsActive == false) existingUserInfo.TimestampType = TimestampType.Deleted;

            existingUserInfo.FirstName = personSaveRequest.Identity.FirstName;
            existingUserInfo.LastName = personSaveRequest.Identity.LastName;
            existingUserInfo.Id = personSaveRequest.PersonId == null
                ? Guid.NewGuid().ToString()
                : personSaveRequest.PersonId.Value.ToString();
            existingUserInfo.BirthDate = personSaveRequest.Identity.DOB;

            if (personSaveRequest.Identity.Gender == "M")
                existingUserInfo.Gender = Gender.M;
            else if (personSaveRequest.Identity.Gender == "F")
                existingUserInfo.Gender = Gender.F;
            else
                existingUserInfo.Gender = Gender.U;

            UpdateInfoValueForKeyInEntityInfoList(personSaveRequest.Identity.AddressLine1,
               Models.Constants.Person.AddressLine1Key, existingUserInfo.UserData);
            UpdateInfoValueForKeyInEntityInfoList(personSaveRequest.Identity.AddressLine2,
                Models.Constants.Person.AddressLine2Key, existingUserInfo.UserData);
            UpdateInfoValueForKeyInEntityInfoList(personSaveRequest.Identity.AvatarImagePath,
                Models.Constants.Person.AvatarImagePathKey, existingUserInfo.UserData);
            UpdateInfoValueForKeyInEntityInfoList(personSaveRequest.Identity.City, Models.Constants.Person.CityKey,
                existingUserInfo.UserData);
            UpdateInfoValueForKeyInEntityInfoList(personSaveRequest.Identity.Relationship,
                Models.Constants.Person.RelationshipKey, existingUserInfo.UserData);
            UpdateInfoValueForKeyInEntityInfoList(personSaveRequest.Identity.State, Models.Constants.Person.StateKey,
                existingUserInfo.UserData);
            UpdateInfoValueForKeyInEntityInfoList(personSaveRequest.Identity.ZipCode, Models.Constants.Person.ZipCodeKey,
                existingUserInfo.UserData);
        }

        public EntityInfo GetPersonInsurance(UserInfo person, PersonInsurance request)
        {
            if (person == null) throw new ArgumentNullException("person");

            return FindOrAddNew(ValueOrNull(request.PersonInsuranceId), person.UserData);
        }

        public EntityInfo GetPersonAddress(UserInfo person, PersonAddress request)
        {
            if (person == null) throw new ArgumentNullException("person");

            return FindOrAddNew(ValueOrNull(request.PersonAddressId), person.UserData);
        }

        public void UpdatePersonInsurance(PersonInsurance personInsuranceRequest, EntityInfo personInsuranceToUpdate,
            HealthgradesProfile hgProfile)
        {
            if (personInsuranceToUpdate == null) throw new ArgumentNullException("personInsuranceToUpdate");

            personInsuranceToUpdate.TimestampDate = DateTime.UtcNow;
            personInsuranceToUpdate.TimestampType = TimestampType.Updated;
            personInsuranceToUpdate.InfoType = Insurance.PersonInsuranceInfoTypeKey;

            if (personInsuranceRequest.IsActive == false) personInsuranceToUpdate.TimestampType = TimestampType.Deleted;

            if (personInsuranceRequest.PersonInsuranceId == null)
                personInsuranceRequest.PersonInsuranceId = Guid.Parse(personInsuranceToUpdate.Id);

            UpdateInfoValueForKeyInEntityInfoItem(personInsuranceRequest.CardPathFront, Insurance.CardPathFrontKey,
                personInsuranceToUpdate);
            UpdateInfoValueForKeyInEntityInfoItem(personInsuranceRequest.CardPathBack, Insurance.CardPathBackKey,
                personInsuranceToUpdate);
            UpdateInfoValueForKeyInEntityInfoItem(personInsuranceRequest.InsuranceCode, Insurance.InsuranceCodeKey,
                personInsuranceToUpdate);
            UpdateInfoValueForKeyInEntityInfoItem(personInsuranceRequest.InsuranceName, Insurance.InsuranceNameKey,
                personInsuranceToUpdate);
            UpdateInfoValueForKeyInEntityInfoItem(personInsuranceRequest.InsuranceCarrier,
                Insurance.InsuranceCarrierKey, personInsuranceToUpdate);
            UpdateInfoValueForKeyInEntityInfoItem(personInsuranceRequest.InsurancePlanName,
                Insurance.InsurancePlanNameKey, personInsuranceToUpdate);
            UpdateInfoValueForKeyInEntityInfoItem(personInsuranceRequest.InsuranceSubscriberId,
                Insurance.InsuranceSubscriberIdKey, personInsuranceToUpdate);
            UpdateInfoValueForKeyInEntityInfoItem(personInsuranceRequest.IsDefault, Insurance.InsuranceIsDefault,
                personInsuranceToUpdate);
            ReconcileAccountInsurances(personInsuranceRequest.PersonInsuranceId?.ToString(), hgProfile);
        }

        public void UpdatePersonAddress(PersonAddress personAddressRequest, EntityInfo personAddressToUpdate,
            HealthgradesProfile hgProfile)
        {
            if (personAddressToUpdate == null) throw new ArgumentNullException("personAddressToUpdate");

            personAddressToUpdate.TimestampDate = DateTime.UtcNow;
            personAddressToUpdate.TimestampType = TimestampType.Updated;
            personAddressToUpdate.InfoType = Address.PersonAddressInfoTypeKey;

            if (personAddressRequest.IsActive == false) personAddressToUpdate.TimestampType = TimestampType.Deleted;

            if (personAddressRequest.PersonAddressId == null)
                personAddressRequest.PersonAddressId = Guid.Parse(personAddressToUpdate.Id);
            UpdateInfoValueForKeyInEntityInfoItem(personAddressRequest.AddressName, Address.PersonAddressNameKey,
                personAddressToUpdate);
            UpdateInfoValueForKeyInEntityInfoItem(personAddressRequest.Address, Address.PersonAddressKey,
                personAddressToUpdate);
            UpdateInfoValueForKeyInEntityInfoItem(personAddressRequest.City, Address.PersonAddressCityKey,
                personAddressToUpdate);
            UpdateInfoValueForKeyInEntityInfoItem(personAddressRequest.State, Address.PersonAddressStateKey,
                personAddressToUpdate);
            UpdateInfoValueForKeyInEntityInfoItem(personAddressRequest.Zip, Address.PersonAddressZipKey,
                personAddressToUpdate);
            UpdateInfoValueForKeyInEntityInfoItem(personAddressRequest.IsDefault, Address.PersonAddressIsDefaultKey,
                personAddressToUpdate);
            ReconcileAccountAddresses(personAddressRequest.PersonAddressId?.ToString(), hgProfile);
        }

        /// <summary>
        ///     Assumes the personid exists, will return null if it does not
        /// </summary>
        /// <param name="personId"></param>
        /// <param name="hgProfile"></param>
        /// <returns></returns>
        public UserInfo GetUserInfo(string personId, HealthgradesProfile hgProfile)
        {
            UserInfo userInfoMatch = null;

            if (personId == Guid.Empty.ToString())
                userInfoMatch = FindPerson(hgProfile, personId, "self");
            else if (personId != null) userInfoMatch = FindPerson(hgProfile, personId, null);

            return userInfoMatch;
        }

        /// <summary>
        ///     Returns all the persons in the profile
        /// </summary>
        /// <param name="hgProfile"></param>
        /// <returns></returns>
        public List<UserInfo> GetAllUsersInfo(HealthgradesProfile hgProfile)
        {
            var userInfoMatch = new List<UserInfo>
            {
                hgProfile.UserProfile.User
            };
            foreach (var person in hgProfile.UserProfile.FamilyMembers) userInfoMatch.Add(person);
            return userInfoMatch;
        }

        public EntityInfo GetFacility(UserInfo person, PersonFacility request)
        {
            if (person == null) throw new ArgumentNullException("person");

            string facilityCode = null;

            if (request.FacilityCode != null) facilityCode = request.FacilityCode;

            return FindOrAddNew(facilityCode, person.HospitalIds);
        }

        public void UpdateFacility(PersonFacility facilitySaveRequest, EntityInfo facilityToUpdate,
            HealthgradesProfile hgProfile)
        {
            if (facilityToUpdate == null) throw new ArgumentNullException("facilityToUpdate");

            if (facilitySaveRequest.PersonFacilityId == null) facilitySaveRequest.PersonFacilityId = Guid.NewGuid();

            facilityToUpdate.Id = facilitySaveRequest.FacilityCode;
            facilityToUpdate.TimestampDate = DateTime.UtcNow;
            facilityToUpdate.TimestampType = TimestampType.Updated;
            facilityToUpdate.InfoType = Facility.PersonFacilityInfoTypeKey;

            if (facilitySaveRequest.IsActive == false) facilityToUpdate.TimestampType = TimestampType.Deleted;

            UpdateInfoValueForKeyInEntityInfoItem(facilitySaveRequest.PersonFacilityId, Facility.PersonFacilityIdKey,
                facilityToUpdate);
            ReconcileAccountFacilities(facilitySaveRequest.FacilityCode, hgProfile);
        }

        public void UpdatePersonProvider(PersonProvider personProviderRequest, EntityInfo existingPersonProvider,
            HealthgradesProfile hgProfile)
        {
            if (existingPersonProvider == null) throw new ArgumentNullException("existingPersonProvider");

            if (personProviderRequest.PersonProviderId == null) personProviderRequest.PersonProviderId = Guid.NewGuid();

            existingPersonProvider.Id = personProviderRequest.ProviderCode;
            existingPersonProvider.TimestampDate = DateTime.UtcNow;
            existingPersonProvider.TimestampType = TimestampType.Updated;
            existingPersonProvider.InfoType = Provider.PersonProviderInfoTypeKey;

            if (personProviderRequest.IsActive == false) existingPersonProvider.TimestampType = TimestampType.Deleted;

            UpdateInfoValueForKeyInEntityInfoItem(personProviderRequest.PersonProviderId, Provider.PersonProviderIdKey,
                existingPersonProvider);
            ReconcileProviderNotes(personProviderRequest.ProviderCode, personProviderRequest.Note, hgProfile);
        }

        public EntityInfo GetPersonProvider(UserInfo person, PersonProvider request)
        {
            if (person == null) throw new ArgumentNullException("person");

            string providerId = null;

            if (request.ProviderCode != null) providerId = request.ProviderCode;

            return FindOrAddNew(providerId, person.ProviderIds);
        }

        public EntityInfo GetPersonProviderByProvider(UserInfo person, string providerId)
        {
            if (person == null || providerId == null) throw new ArgumentNullException("person");

            return FindOrAddNew(providerId, person.ProviderIds);
        }

        public EntityInfo GetPersonFacilitiesByFacility(UserInfo person, string facilityId)
        {
            if (person == null || facilityId == null) throw new ArgumentNullException("person");

            return FindOrAddNew(facilityId, person.HospitalIds);
        }

        public UserInfo GetUserInfo(PersonSaveRequest personSaveRequest, HealthgradesProfile hgProfile)
        {
            UserInfo userInfoMatch;
            if (personSaveRequest.PersonId == null)
            {
                userInfoMatch = new UserInfo();
                hgProfile.UserProfile.FamilyMembers.Add(userInfoMatch);
            }
            else
            {
                userInfoMatch = FindPerson(hgProfile, personSaveRequest.PersonId.Value.ToString(),
                    personSaveRequest.Identity.Relationship);
            }

            //If we have not found a match add it as a new family member
            if (userInfoMatch == null)
            {
                userInfoMatch = new UserInfo();
                hgProfile.UserProfile.FamilyMembers.Add(userInfoMatch);
            }

            return userInfoMatch;
        }

        public Guid GetAccountId(HealthgradesProfile hgProfile)
        {
            return GetGuidValueForKeyInEntityInfoList(Models.Constants.Account.AccountIdKey,
                hgProfile.UserProfile.AccountData);
        }

        public static List<EntityInfo> Merge(List<EntityInfo> p1, List<EntityInfo> p2, IEntityInfoMerger entityMerger)
        {
            if (ExclusiveNotEmpty(p1, p2, out var m)) return m;

            if (p1 == null || p2 == null) return null;

            var eic = new EntityInfoComparerById();

            m = new List<EntityInfo>();

            // merge most recent existing that aren't deleted... don't use eic comparer with the union!
            var qToUpdate = from x in p1.Union(p2)
                            group x by x.Id
                into g
                            let mostRecentDate = g.Max(mrd => mrd.TimestampDate)
                            select g.First(xx => xx.TimestampDate == mostRecentDate);

            var toMerge = qToUpdate.Where(x => x.TimestampType != TimestampType.Deleted);

            foreach (var e in toMerge)
            {
                var pp1 = p1.FirstOrDefault(x => eic.Equals(x, e) && x.TimestampType == TimestampType.Updated);
                var pp2 = p2.FirstOrDefault(x => eic.Equals(x, e) && x.TimestampType == TimestampType.Updated);
                m.Add(entityMerger.Merge(pp1, pp2));
            }

            return m;
        }

        private static bool ExclusiveNotEmpty<T>(T a, T b, out T value) where T : class, ICollection
        {
            if (Exclusive(a, b, out value))
            {
                if (value == null)
                    return true;

                if (value.Count > 0)
                    return true;
            }

            value = null;
            return false;
        }

        private static bool Exclusive<T>(T a, T b, out T value) where T : class
        {
            value = null;

            if (a == null && b == null)
                return true;

            if (a != null && b == null)
            {
                value = a;
                return true;
            }

            if (a == null)
            {
                value = b;
                return true;
            }

            return false;
        }

        private Models.Person ToPerson(UserInfo userInfo, List<EntityInfo> providers, Guid accountId, bool isSelf)
        {
            var relationShip = GetValueForKeyInEntityInfoList(Models.Constants.Person.RelationshipKey, userInfo.UserData);

            if (!isSelf && relationShip == "self")
                return null;

            var person = new Models.Person();

            var personId = isSelf ? Guid.Empty : Guid.NewGuid();
            Guid.TryParse(userInfo.Id, out personId);
            person.PersonId = isSelf ? Guid.Empty : personId;
            person.Identity = new Identity
            {
                FirstName = userInfo.FirstName,
                LastName = userInfo.LastName,
                DOB = userInfo.BirthDate
            };
            person.AccountId = accountId;

            if (userInfo.Gender != null && userInfo.Gender == Gender.M)
                person.Identity.Gender = "M";
            else if (userInfo.Gender != null && userInfo.Gender == Gender.F)
                person.Identity.Gender = "F";
            else if (userInfo.Gender != null && userInfo.Gender == Gender.U) person.Identity.Gender = "U";

            person.Identity.AddressLine1 =
                GetValueForKeyInEntityInfoList(Models.Constants.Person.AddressLine1Key, userInfo.UserData);
            person.Identity.AddressLine2 =
                GetValueForKeyInEntityInfoList(Models.Constants.Person.AddressLine2Key, userInfo.UserData);
            person.Identity.AvatarImagePath =
                GetValueForKeyInEntityInfoList(Models.Constants.Person.AvatarImagePathKey, userInfo.UserData);
            person.Identity.City = GetValueForKeyInEntityInfoList(Models.Constants.Person.CityKey, userInfo.UserData);
            person.Identity.Relationship = isSelf && relationShip == null ? "self" : relationShip;
            person.Identity.State = GetValueForKeyInEntityInfoList(Models.Constants.Person.StateKey, userInfo.UserData);
            person.Identity.ZipCode = GetValueForKeyInEntityInfoList(Models.Constants.Person.ZipCodeKey, userInfo.UserData);

            if (userInfo.ProviderIds != null && userInfo.ProviderIds.Count > 0)
            {
                var personProviders =
                    ToPersonProviders(
                        userInfo.ProviderIds.Where(x => x.TimestampType != TimestampType.Deleted).ToList(), providers,
                        person.PersonId.Value);
                if (personProviders != null)
                {
                    if (person.Profile == null) person.Profile = new Models.Profile();
                    person.Profile.Providers = personProviders;
                }
            }

            var personFacilities =
                ToPersonFacilities(userInfo.HospitalIds.Where(x => x.TimestampType != TimestampType.Deleted).ToList(),
                    person.PersonId.Value);
            if (personFacilities != null)
            {
                if (person.Profile == null) person.Profile = new Models.Profile();
                person.Profile.Facilities = personFacilities;
            }

            if (relationShip == "self")
            {
                var personInsurances = ToPersonInsurances(userInfo.UserData, person.PersonId.Value);
                if (personInsurances != null)
                {
                    if (person.Profile == null) person.Profile = new Models.Profile();

                    person.Profile.Insurances = personInsurances;
                }

                var personAddresses = ToPersonAddresses(userInfo.UserData, person.PersonId.Value);
                if (personAddresses != null)
                {
                    if (person.Profile == null) person.Profile = new Models.Profile();

                    person.Profile.Addresses = personAddresses;
                }
            }

            return person;
        }

        private List<PersonInsurance> ToPersonInsurances(List<EntityInfo> userData, Guid personId)
        {
            var insurances = userData.FindAll(entityInfo =>
                entityInfo.InfoType == Insurance.PersonInsuranceInfoTypeKey &&
                entityInfo.TimestampType != TimestampType.Deleted);

            List<PersonInsurance> personInsurances = null;

            if (insurances.Count > 0)
            {
                personInsurances = new List<PersonInsurance>();
                foreach (var insurance in insurances)
                {
                    var personInsurance = new PersonInsurance
                    {
                        PersonInsuranceId = Guid.Parse(insurance.Id),
                        PersonId = personId,
                        CardPathFront = GetValueForKeyInEntityInfoItem(Insurance.CardPathFrontKey, insurance),
                        CardPathBack = GetValueForKeyInEntityInfoItem(Insurance.CardPathBackKey, insurance),

                        InsuranceCode = GetValueForKeyInEntityInfoItem(Insurance.InsuranceCodeKey, insurance),
                        InsuranceName = GetValueForKeyInEntityInfoItem(Insurance.InsuranceNameKey, insurance),
                        InsuranceCarrier = GetValueForKeyInEntityInfoItem(Insurance.InsuranceCarrierKey, insurance),
                        InsurancePlanName = GetValueForKeyInEntityInfoItem(Insurance.InsurancePlanNameKey, insurance),
                        InsuranceSubscriberId =
                            GetValueForKeyInEntityInfoItem(Insurance.InsuranceSubscriberIdKey, insurance),
                        IsDefault = GetBoolValueForKeyInEntityInfoItem(Insurance.InsuranceIsDefault, insurance),
                        IsActive = true
                    };
                    personInsurances.Add(personInsurance);
                }
            }

            return personInsurances;
        }

        private List<PersonAddress> ToPersonAddresses(List<EntityInfo> userData, Guid personId)
        {
            var addresses = userData.FindAll(entityInfo =>
                entityInfo.InfoType == Address.PersonAddressInfoTypeKey &&
                entityInfo.TimestampType != TimestampType.Deleted);

            List<PersonAddress> personAddresses = null;

            if (addresses.Count > 0)
            {
                personAddresses = new List<PersonAddress>();
                foreach (var address in addresses)
                {
                    var personAddress = new PersonAddress
                    {
                        PersonAddressId = Guid.Parse(address.Id),
                        PersonId = personId,
                        AddressName = GetValueForKeyInEntityInfoItem(Address.PersonAddressNameKey, address),
                        Address = GetValueForKeyInEntityInfoItem(Address.PersonAddressKey, address),
                        City = GetValueForKeyInEntityInfoItem(Address.PersonAddressCityKey, address),
                        State = GetValueForKeyInEntityInfoItem(Address.PersonAddressStateKey, address),
                        Zip = GetValueForKeyInEntityInfoItem(Address.PersonAddressZipKey, address),
                        IsDefault = GetBoolValueForKeyInEntityInfoItem(Address.PersonAddressIsDefaultKey, address),
                        IsActive = true
                    };

                    personAddresses.Add(personAddress);
                }
            }

            return personAddresses;
        }

        private List<PersonFacility> ToPersonFacilities(List<EntityInfo> facilities, Guid personId)
        {
            List<PersonFacility> personFacilities = null;

            if (facilities != null && facilities.Count > 0)
            {
                personFacilities = new List<PersonFacility>();
                foreach (var facility in facilities)
                {
                    var personFacility = new PersonFacility
                    {
                        IsActive = true,
                        PersonId = personId,
                        PersonFacilityId = GetGuidValueForKeyInEntityInfoItem(Facility.PersonFacilityIdKey, facility),
                        FacilityCode = facility.Id
                    };
                    personFacilities.Add(personFacility);
                }
            }

            return personFacilities;
        }

        private List<PersonProvider> ToPersonProviders(List<EntityInfo> providerIds, List<EntityInfo> providers,
            Guid personId)
        {
            List<PersonProvider> personProviders = null;

            if (providerIds != null && providerIds.Count > 0)
            {
                personProviders = new List<PersonProvider>();
                foreach (var providerId in providerIds)
                {
                    var personProvider = new PersonProvider
                    {
                        IsActive = true,
                        PersonId = personId,
                        PersonProviderId = GetGuidValueForKeyInEntityInfoItem(Provider.PersonProviderIdKey, providerId),
                        ProviderCode = providerId.Id
                    };

                    if (providers != null)
                    {
                        var provider = providers.FirstOrDefault(p => p.Id.Equals(personProvider.ProviderCode));
                        if (provider != null)
                        {
                            //Info summaryNote = FindInfoForKeyInEntityInfoItem(LabelId.Comments, provider);
                            var summaryNote = FindInfoForCommentsInEntityInfoItem(LabelId.Comments, provider);
                            if (summaryNote != null && !string.IsNullOrEmpty(summaryNote.Value))
                                personProvider.Note = summaryNote.Value;
                        }
                    }

                    personProviders.Add(personProvider);
                }
            }

            return personProviders;
        }

        private string GetValueForKeyInEntityInfoItem(string key, EntityInfo entityInfo)
        {
            var info = FindInfoForKeyInEntityInfoItem(key, entityInfo);

            if (info != null) return info.Value;

            return null;
        }

        private Guid? GetGuidValueForKeyInEntityInfoItem(string key, EntityInfo entityInfo)
        {
            var info = FindInfoForKeyInEntityInfoItem(key, entityInfo);

            if (info != null)
                if (Guid.TryParse(info.Value, out var guidValue))
                    return guidValue;

            return null;
        }

        private bool GetBoolValueForKeyInEntityInfoItem(string key, EntityInfo entityInfo)
        {
            var info = FindInfoForKeyInEntityInfoItem(key, entityInfo);

            if (info != null)
                if (bool.TryParse(info.Value, out var boolValue))
                    return boolValue;

            return false;
        }

        private string ValueOrNull(Guid? guid)
        {
            if (guid != null && guid.HasValue)
                return guid.Value.ToString();
            return null;
        }

        private void ReconcileAccountFacilities(string facilityCode, HealthgradesProfile hgProfile)
        {
            var hospitals = FindOrAddNew(facilityCode, hgProfile.UserProfile.Hospitals);
            hospitals.Id = facilityCode;
        }

        private void ReconcileAccountInsurances(string personInsuranceId, HealthgradesProfile hgProfile)
        {
            var payor = FindOrAddNew(personInsuranceId, hgProfile.UserProfile.User.Payors);
            payor.Id = personInsuranceId;
        }

        private void ReconcileAccountAddresses(string personAddressId, HealthgradesProfile hgProfile)
        {
            var address = FindOrAddNew(personAddressId, hgProfile.UserProfile.User.Addresses);
            address.Id = personAddressId;
        }

        private void ReconcileProviderNotes(string providerId, string notes, HealthgradesProfile hgProfile)
        {
            var providerNotes = FindOrAddNew(providerId, hgProfile.UserProfile.Providers);
            providerNotes.Id = providerId;
            var summaryNote = FindInfoForKeyInEntityInfoItem(LabelId.Comments, providerNotes);

            if (summaryNote == null)
            {
                summaryNote = new Info
                {
                    Value = notes,
                    LabelId = LabelId.Comments
                };

                providerNotes.Items.Add(summaryNote);
            }
            else
            {
                summaryNote.Value = notes;
                summaryNote.TimestampDate = DateTime.UtcNow;
                summaryNote.TimestampType = TimestampType.Updated;
            }
        }

        private void UpdateInfoValueForKeyInEntityInfoItem(bool flag, string key, EntityInfo existingEntityInfo)
        {
            UpdateInfoValueForKeyInEntityInfoItem(flag.ToString(), key, existingEntityInfo);
        }

        private void UpdateInfoValueForKeyInEntityInfoItem(Guid? guid, string key, EntityInfo existingEntityInfo)
        {
            if (guid == null || guid.HasValue == false)
            {
                string nullString = null;
                UpdateInfoValueForKeyInEntityInfoItem(nullString, key, existingEntityInfo);
            }
            else
            {
                UpdateInfoValueForKeyInEntityInfoItem(guid.Value.ToString(), key, existingEntityInfo);
            }
        }

        private EntityInfo FindOrAddNew(string entityInfoId, List<EntityInfo> existingEntityInfoItems)
        {
            EntityInfo entityInfoMatch = null;

            //If we have an id we can search for the object
            if (entityInfoId != null)
                //Get the entity matching the id
                entityInfoMatch = existingEntityInfoItems.Find(entityInfo => entityInfo.Id == entityInfoId);

            //If we still don't have anything then create a new one
            if (entityInfoMatch == null)
            {
                entityInfoMatch = new EntityInfo();
                existingEntityInfoItems.Add(entityInfoMatch);
            }

            return entityInfoMatch;
        }

        private UserInfo FindPerson(HealthgradesProfile hgProfile, string personId, string relationship)
        {
            //We have to at least have a min of personid to find you
            if (personId == null) return null;

            UserInfo userInfoMatch = null;

            //The high level user is the 'self' person
            if (relationship == "self" || personId == hgProfile.UserProfile.User.Id)
                userInfoMatch = hgProfile.UserProfile.User;
            else
                foreach (var person in hgProfile.UserProfile.FamilyMembers)
                    if (personId.ToLower() == person.Id.ToLower())
                    {
                        userInfoMatch = person;
                        break;
                    }

            return userInfoMatch;
        }

        private UserInfo NewDefaultSelfUser()
        {
            var defaultSelfUser = new UserInfo
            {
                Id = Guid.Empty.ToString()
            };
            UpdateInfoValueForKeyInEntityInfoList("self", Models.Constants.Person.RelationshipKey, defaultSelfUser.UserData);

            return defaultSelfUser;
        }

        private Guid GetGuidValueForKeyInEntityInfoList(string key, List<EntityInfo> entityInfoList)
        {
            var stringValue = GetValueForKeyInEntityInfoList(key, entityInfoList);
            var guidValue = Guid.Empty;

            if (stringValue != null) Guid.TryParse(stringValue, out guidValue);

            return guidValue;
        }

        private string GetValueForKeyInEntityInfoList(string key, List<EntityInfo> entityInfoList)
        {
            var info = FindInfoForKeyInEntityInfoList(key, entityInfoList);

            if (info != null) return info.Value;

            return null;
        }

        private void UpdateInfoValueForKeyInEntityInfoList(string value, string key, List<EntityInfo> entityInfoList)
        {
            var infoToUpdate = FindInfoForKeyInEntityInfoList(key, entityInfoList);

            if (infoToUpdate == null && value != null)
            {
                infoToUpdate = new Info
                {
                    Label = key
                };

                var newEntityInfo = new EntityInfo();
                newEntityInfo.Items.Add(infoToUpdate);

                entityInfoList.Add(newEntityInfo);
            }

            if (infoToUpdate != null && value != infoToUpdate.Value)
            {
                infoToUpdate.TimestampDate = DateTime.UtcNow;
                infoToUpdate.Value = value;
            }
        }

        private void UpdateInfoValueForKeyInEntityInfoItem(string value, string key, EntityInfo entityInfoItem)
        {
            var infoToUpdate = FindInfoForKeyInEntityInfoItem(key, entityInfoItem);

            if (infoToUpdate == null && value != null)
            {
                infoToUpdate = new Info
                {
                    Label = key
                };

                entityInfoItem.Items.Add(infoToUpdate);
            }

            if (value == null && infoToUpdate != null)
            {
                infoToUpdate.TimestampDate = DateTime.UtcNow;
                infoToUpdate.TimestampType = TimestampType.Deleted;
            }
            else if (infoToUpdate != null && value != infoToUpdate.Value)
            {
                infoToUpdate.TimestampDate = DateTime.UtcNow;
                infoToUpdate.TimestampType = TimestampType.Updated;
                infoToUpdate.Value = value;
            }
        }

        private Info FindInfoForKeyInEntityInfoList(string key, IEnumerable<EntityInfo> entityInfoList)
        {
            Info foundItem = null;

            foreach (var entityInfoItem in entityInfoList)
            {
                foundItem = FindInfoForKeyInEntityInfoItem(key, entityInfoItem);

                //If we find it get out of the loop
                if (foundItem != null) break;
            }

            return foundItem;
        }

        private Info FindInfoForKeyInEntityInfoItem(string key, EntityInfo entityInfoItem)
        {
            return entityInfoItem.Items.Find(x => x.Label.Equals(key));
        }

        private Info FindInfoForKeyInEntityInfoItem(LabelId key, EntityInfo entityInfoItem)
        {
            return entityInfoItem.Items.Find(x => x.LabelId == key);
        }

        private Info FindInfoForCommentsInEntityInfoItem(LabelId key, EntityInfo entityInfoItem)
        {
            return entityInfoItem.Items.FindLast(x =>
                x.LabelId == key && !string.IsNullOrEmpty(x.Value) && x.TimestampType != TimestampType.Deleted);
        }

        private Models.Account GetPersonProviderSpecialties(Models.Account account)
        {
            if (account != null)
            {
                var pwids = new HashSet<string>();
                foreach (var person in account.Persons)
                    if (person.Profile != null && person.Profile.Providers != null)
                        foreach (var personProvider in person.Profile.Providers)
                            if (!string.IsNullOrEmpty(personProvider.ProviderCode))
                                pwids.Add(personProvider.ProviderCode);

                if (pwids.Count > 0)
                {
                    var pwidArray = new string[pwids.Count];
                    pwids.CopyTo(pwidArray);
                    var result = _ProviderSolrRepository.GetProvidersByPwid(pwidArray);
                    var solrDocs = result.Response.Documents;

                    if (solrDocs.Count > 0)
                        foreach (var person in account.Persons)
                            if (person.Profile != null && person.Profile.Providers != null)
                                foreach (var personProvider in person.Profile.Providers)
                                    if (!string.IsNullOrEmpty(personProvider.ProviderCode))
                                    {
                                        var solrDoc = solrDocs.FirstOrDefault(a =>
                                            a.provider_json.Id.ToUpper().Equals(personProvider.ProviderCode.ToUpper()));
                                        if (solrDoc != null)
                                        {
                                            foreach (var specialty in solrDoc.provider_json
                                                .ProviderPracticingSpecialties)
                                                personProvider.SpecialtyCodes.Add(specialty.PracticingSpecialityCode);

                                            personProvider.VerticalCodes =
                                                GetProviderVerticalCodes(personProvider.SpecialtyCodes);
                                        }
                                    }
                }
            }

            return account;
        }

        private List<string> GetProviderVerticalCodes(List<string> specialtyCodes)
        {
            var verticalCodes = new List<string>();
            if (specialtyCodes.Count > 0)
            {
                var result = _AutosuggestSolrRepository.GetProviderVerticalsByPractingSpecialty(specialtyCodes.ToArray());
                var solrDocs = result.Response.Documents;

                if (solrDocs.Count > 0)
                    foreach (var vertical in solrDocs)
                        verticalCodes.Add(vertical.prac_spec_vert_code);
            }

            return verticalCodes;
        }
    }

    internal class ProviderEntityInfoMerger : EntityInfoMerger
    {
    }

    internal class EntityInfoMerger : IEntityInfoMerger
    {
        public EntityInfo Merge(EntityInfo p1, EntityInfo p2)
        {
            if (Exclusive(p1, p2, out var r)) return r;

            if (!Exclusive(p1.UserLocation, p2.UserLocation, out var latestUserLocation))
                latestUserLocation = p1.TimestampDate >= p2.TimestampDate ? p1.UserLocation : p2.UserLocation;

            if (!Exclusive(p1.SearchLocation, p2.SearchLocation, out var latestSearchLocation))
                latestSearchLocation = p1.TimestampDate >= p2.TimestampDate ? p1.SearchLocation : p2.SearchLocation;

            r = new EntityInfo
            {
                Id = p1.Id,
                InfoType = p1.InfoType,
                TimestampType = TimestampType.Updated,
                TimestampDate = p1.TimestampDate >= p2.TimestampDate ? p1.TimestampDate : p2.TimestampDate,
                UserLocation = latestUserLocation,
                SearchLocation = latestSearchLocation
            };

            if (Exclusive(p1.Items, p2.Items, out var items))
            {
                r.Items = items;
            }
            else
            {
                r.Items = new List<Info>();

                var iec = new InfoEntityComparer();

                var qToUpdate = from x in p1.Items.Union(p2.Items)
                                group x by x.Id
                    into g
                                let mostRecentDate = g.Max(mrd => mrd.TimestampDate)
                                select g.First(xx => xx.TimestampDate == mostRecentDate);

                var toMerge = qToUpdate.Where(x => x.TimestampType != TimestampType.Deleted);

                foreach (var e in toMerge)
                {
                    var pp1 = p1.Items.FirstOrDefault(x =>
                        iec.Equals(x, e) && x.TimestampType == TimestampType.Updated);
                    var pp2 = p2.Items.FirstOrDefault(x =>
                        iec.Equals(x, e) && x.TimestampType == TimestampType.Updated);

                    var p = Merge(pp1, pp2);
                    if (p != null)
                        r.Items.Add(p);
                }
            }

            return r;
        }

        private Info Merge(Info p1, Info p2)
        {
            if (Exclusive(p1, p2, out var p)) return p;

            if (p1.TimestampDate >= p2.TimestampDate) return p1;

            return p2;
        }

        private static bool Exclusive<T>(T a, T b, out T value) where T : class
        {
            value = null;

            if (a == null && b == null)
                return true;

            if (a != null && b == null)
            {
                value = a;
                return true;
            }

            if (a == null)
            {
                value = b;
                return true;
            }

            return false;
        }

        private class InfoEntityComparer : IEqualityComparer<Info>
        {
            public bool Equals(Info x, Info y)
            {
                return string.Compare(x.Id, y.Id) == 0;
            }

            public int GetHashCode(Info obj)
            {
                return obj.Value.GetHashCode();
            }
        }
    }

    internal class EntityInfoComparerById : IEqualityComparer<EntityInfo>
    {
        public bool Equals(EntityInfo x, EntityInfo y)
        {
            return string.Compare(x.Id.ToLower(), y.Id.ToLower(), false) == 0;
        }

        public int GetHashCode(EntityInfo obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}