﻿using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.Net.Http;

namespace Healthgrades.GreyWolfLibrary.Helpers
{
    public class HGAuthentication : IAuthenticationValidation
    {
        private readonly HttpClient _httpClient;
        private readonly string _accountAuthorizationUrl;
        private ILogger<HGAuthentication> LoggingService { get; set; }

        public HGAuthentication(IHttpClientFactory httpClientFactory, ILogger<HGAuthentication> loggingService, string accountAuthorizationUrl)
        {
            _httpClient = httpClientFactory.CreateClient(HttpClientDefinitions.DefaultServiceHttpClient);
            LoggingService = loggingService;
            _accountAuthorizationUrl = accountAuthorizationUrl;
        }

        public Tuple<bool, AuthenticationProps> ValidateAuthentication(string SessionToken)
        {
            try
            {
                var cognitoUrl = _accountAuthorizationUrl + "/getsession";

                var jo = JObject.Parse(
                    _httpClient
                    .PostAsJsonAsync(cognitoUrl, JObject.FromObject(new { sessionToken = SessionToken, renew = false }))
                    .Result
                    .Content
                    .ReadAsStringAsync()
                    .Result);

                if (jo["success"].Value<bool>())
                {
                    var userIdentityId = jo["data"]?["sessionPayload"]?["userIdentityId"]?.Value<string>();
                    var refreshToken = jo["data"]?["sessionPayload"]?["refreshToken"]?.Value<string>();
                    return new Tuple<bool, AuthenticationProps>(true,
                        new AuthenticationProps() { AuthenticationId = userIdentityId, SessionToken = refreshToken });
                }

                return new Tuple<bool, AuthenticationProps>(false, null);
            }
            catch (Exception ex)
            {
                ex.Demystify();
                LoggingService.LogError(ex, "Exception caught on ValidateAuthentication in HGAuthentication");
                return new Tuple<bool, AuthenticationProps>(false, null);
            }
        }
    }
}