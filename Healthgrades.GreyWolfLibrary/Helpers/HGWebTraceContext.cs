﻿using Healthgrades.GreyWolfLibrary.Interfaces;
using Microsoft.AspNetCore.Http;

namespace Healthgrades.GreyWolfLibrary.Helpers
{
    public class HgWebTraceContext : IHGTraceContext
    {
        public HgWebTraceContext(IHttpContextAccessor context)
        {
            AmazonTraceId = context.HttpContext.Request.Headers["X-Amzn-Trace-Id"];
        }

        public string AmazonTraceId { get; }
    }
}