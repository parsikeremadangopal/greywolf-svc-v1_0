﻿using Healthgrades.GreyWolfLibrary.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Healthgrades.GreyWolfLibrary.Helpers
{
    // IProfileMerger
    public interface IProfileMerger
    {
        string ApplicationId { get; }

        string ApplicationName { get; }

        string ApplicationVersion { get; }

        HealthgradesProfile Merge(HealthgradesProfile profile1, HealthgradesProfile profile2);
    }

    public interface IEntityInfoMerger
    {
        EntityInfo Merge(EntityInfo p1, EntityInfo p2);
    }

    public interface IUserInfoMerger
    {
        UserInfo Merge(UserInfo p1, UserInfo p2);
    }

    [Serializable]
    public class ProfileMergeException : Exception
    {
        private readonly string errorSection;

        public ProfileMergeException(string errorSection, string message)
            : base(message)
        {
            this.errorSection = errorSection;
        }

        public override string ToString()
        {
            return string.Format("ProfileMergeException - {0} - {1}", errorSection, base.ToString());
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("info");

            info.AddValue("Text", string.Format("ProfileMergeException - {0} - {1}", errorSection, base.ToString()));
            base.GetObjectData(info, context);
        }
    }

    public class MagicalProfileMergingMachine
    {
        public static IProfileMerger CreateMerger()
        {
            return new HealthgradesProfileMerger();
        }
    }

    // HealthgradesProfileMerger
    internal class HealthgradesProfileMerger : IProfileMerger
    {
        public string ApplicationId { get; private set; }

        public string ApplicationVersion => "1.0";

        public string ApplicationName => "Healthgrades";

        public HealthgradesProfile Merge(HealthgradesProfile p1, HealthgradesProfile p2)
        {
            if (!Exclusive(p1, p2, out var m))
            {
                m = new HealthgradesProfile();
                Merge(p1.UserProfile, p2.UserProfile, m);
            }

            return m;
        }

        //private void ValidateProfileVersion(HealthgradesProfile p)
        //{
        //    if (p.Version != ApplicationVersion || p.Name != ApplicationName)
        //        throw new ProfileMergeException("Version", "HealthgradesProfileMerger does not support application version and/or name indicated in profile.");
        //}

        //bool IsNullOrEmpty<T>(IEnumerable<T> c)
        //{
        //    return c == null || c.Any() == false;
        //}

        //void CheckPrimaryForeignConsistency(int candidate, string primaryName, List<EntityInfo> primary, string foreignName, List<EntityInfo> foreign)
        //{
        //    bool hasForeign = !IsNullOrEmpty(foreign);
        //    if (!hasForeign)
        //        return;

        //    bool hasPrimary = !IsNullOrEmpty(primary);

        //    if (!hasPrimary)
        //        throw new ProfileMergeException("Consistency Check",
        //            string.Format("Candidate {0}: Primary list {1} is empty but foreign list {2} is not.",
        //            candidate, primaryName, foreignName));

        //    var eic = new EntityInfoComparerById();

        //    if (foreign.Intersect(primary, eic).Count() != foreign.Count())
        //        throw new ProfileMergeException("Consistency Check",
        //            string.Format("Candidate {0}: Foreign list {1} has items not in primary list {2}.",
        //            candidate, foreignName, primaryName));

        //    if (foreign.Distinct(eic).Count() != foreign.Count())
        //        throw new ProfileMergeException("Consistency Check",
        //            string.Format("Candidate {0}: Foreign list {1} has duplicate items.",
        //            candidate, foreignName));

        //    var deletedPrimary = primary.Where(x => x.TimestampType == TimestampType.Deleted).ToList();

        //    foreach (var ei in deletedPrimary)
        //    {
        //        var eis = foreign.Where(x => eic.Equals(x, ei)).FirstOrDefault();
        //        if (eis != null && eis.TimestampType != TimestampType.Deleted)
        //            throw new ProfileMergeException("Consistency Check",
        //        string.Format("Candidate {0}: Primary list {1} and foreign list {2} deletions are inconsistent.",
        //        candidate, primaryName, foreignName));

        //    }
        //}

        //private void CheckConsistency(HealthgradesProfile p, int candidate)
        //{
        //    if (p == null)
        //        return;

        //    if (p.UserProfile == null)
        //        return;

        //    if (p.UserProfile.Providers != null)
        //    {
        //        var eic = new EntityInfoComparerById();

        //        if (p.UserProfile.Providers.Distinct(eic).Count() != p.UserProfile.Providers.Count())
        //            throw new ProfileMergeException("Consistency Check",
        //            string.Format("Candidate {0}: Primary list HealthgradesProfile.UserProfile.Providers has duplicate items.",
        //            candidate));
        //    }

        //    if (p.UserProfile.Hospitals != null)
        //    {
        //        var eic = new EntityInfoComparerById();

        //        if (p.UserProfile.Hospitals.Distinct(eic).Count() != p.UserProfile.Hospitals.Count())
        //            throw new ProfileMergeException("Consistency Check",
        //            string.Format("Candidate {0}: Primary list HealthgradesProfile.UserProfile.Hospitals has duplicate items.",
        //            candidate));
        //    }

        //    if (p.UserProfile.User != null)
        //    {
        //        CheckPrimaryForeignConsistency(candidate, "HealthgradesProfile.UserProfile.Providers", p.UserProfile.Providers,
        //               "HealthgradesProfile.UserProfile.User.ProviderIds", p.UserProfile.User.ProviderIds);

        //        CheckPrimaryForeignConsistency(candidate, "HealthgradesProfile.UserProfile.Hospitals", p.UserProfile.Hospitals,
        //           "HealthgradesProfile.UserProfile.User.HospitalIds", p.UserProfile.User.HospitalIds);
        //    }

        //    if (p.UserProfile.FamilyMembers != null)
        //    {
        //        for (int i = 0; i < p.UserProfile.FamilyMembers.Count; ++i)
        //        {
        //            var fm = p.UserProfile.FamilyMembers[i];

        //            CheckPrimaryForeignConsistency(candidate, "HealthgradesProfile.UserProfile.Providers", p.UserProfile.Providers,
        //                string.Format("HealthgradesProfile.UserProfile.FamilyMembers[{0}].ProviderIds", i), fm.ProviderIds);

        //            CheckPrimaryForeignConsistency(candidate, "HealthgradesProfile.UserProfile.Hospitals", p.UserProfile.Hospitals,
        //               string.Format("HealthgradesProfile.UserProfile.FamilyMembers[{0}].HospitalIds", i), fm.HospitalIds);
        //        }
        //    }
        //}

        private static bool Exclusive<T>(T a, T b, out T value) where T : class
        {
            value = null;

            if (a == null && b == null)
                return true;

            if (a != null && b == null)
            {
                value = a;
                return true;
            }

            if (a == null)
            {
                value = b;
                return true;
            }

            return false;
        }

        private static bool ExclusiveNotEmpty<T>(T a, T b, out T value) where T : class, ICollection
        {
            if (Exclusive(a, b, out value))
            {
                if (value == null)
                    return true;

                if (value.Count > 0)
                    return true;
            }

            value = null;
            return false;
        }

        private void Merge(UserProfileInfo p1, UserProfileInfo p2, HealthgradesProfile merged)
        {
            if (Exclusive(p1, p2, out var m))
                merged.UserProfile = m;
            else
                merged.UserProfile = new UserProfileInfo
                {
                    User = Merge(p1.User, p2.User),
                    FamilyMembers = Merge(p1.FamilyMembers, p2.FamilyMembers, new FamilyMemberUserInfoMerger()),
                    Providers = Merge(p1.Providers, p2.Providers, new ProviderEntityInfoMerger()),
                    Hospitals = Merge(p1.Hospitals, p2.Hospitals, new HospitalEntityInfoMerger()),
                    RecommendedProviders = Merge(p1.RecommendedProviders, p2.RecommendedProviders,
                        new RecommendedProviderEntityInfoMerger()),
                    Groups = Merge(p1.Groups, p2.Groups, new GroupEntityInfoMerger()),
                    FacebookThirdPartyIds = Merge(p1.FacebookThirdPartyIds, p2.FacebookThirdPartyIds,
                        new FacebookThirdPartyIdEntityInfoMerger()),
                    InboxAlerts = Merge(p1.InboxAlerts, p2.InboxAlerts),
                    AccountData = Merge(p1.AccountData, p2.AccountData, new EntityInfoMerger())
                };
        }

        private AlertCollectionState Merge(AlertCollectionState p1, AlertCollectionState p2)
        {
            if (Exclusive(p1, p2, out var m))
                return m;

            m = new AlertCollectionState();

            if (!ExclusiveNotEmpty(p1.ReadIds, p2.ReadIds, out var read))
                if (p1.ReadIds != null && p2.ReadIds != null)
                    read = p1.ReadIds.Union(p2.ReadIds).ToList();

            if (!ExclusiveNotEmpty(p1.UnreadIds, p2.UnreadIds, out var unread))
                if (p1.UnreadIds != null && p2.UnreadIds != null)
                    unread = p1.UnreadIds.Union(p2.UnreadIds).ToList();

            if (!ExclusiveNotEmpty(p1.DeletedIds, p2.DeletedIds, out var deleted))
                if (p1.DeletedIds != null && p2.DeletedIds != null)
                    deleted = p1.DeletedIds.Union(p2.DeletedIds).ToList();

            if (read != null && deleted != null)
                read.RemoveAll(x => deleted.Contains(x));

            if (unread != null && deleted != null)
                unread.RemoveAll(x => deleted.Contains(x));

            if (unread != null && read != null)
                unread.RemoveAll(x => read.Contains(x));

            m = new AlertCollectionState
            {
                ReadIds = read,
                UnreadIds = unread,
                DeletedIds = deleted
            };

            return m;
        }

        private UserInfo Merge(UserInfo p1, UserInfo p2)
        {
            if (Exclusive(p1, p2, out var m)) return m;

            m = new UserInfo();

            if (string.IsNullOrEmpty(p2.FirstName.Trim()))
                m.FirstName = p1.FirstName;
            else
                m.FirstName = p1.TimestampDate >= p2.TimestampDate ? p1.FirstName : p2.FirstName;

            if (string.IsNullOrEmpty(p2.LastName.Trim()))
                m.LastName = p1.LastName;
            else
                m.LastName = p1.TimestampDate >= p2.TimestampDate ? p1.LastName : p2.LastName;

            if (p2.Gender == Gender.U)
                m.Gender = p1.Gender;
            else
                m.Gender = p1.TimestampDate >= p2.TimestampDate ? p1.Gender : p2.Gender;

            if (p2.BirthDate == DateTime.MinValue)
                m.BirthDate = p1.BirthDate;
            else
                m.BirthDate = p1.TimestampDate >= p2.TimestampDate ? p1.BirthDate : p2.BirthDate;

            if (p1.Id == Guid.Empty.ToString() && p2.Id != Guid.Empty.ToString())
                m.Id = Guid.Empty.ToString();
            //m = UserInfo.TimestampedClone(p1, p2);

            m.Payors = Merge(p1.Payors, p2.Payors, new PayorEntityInfoMerger());
            m.ProviderIds = Merge(p1.ProviderIds, p2.ProviderIds, new ProviderEntityInfoMerger());
            m.HospitalIds = Merge(p1.HospitalIds, p2.HospitalIds, new HospitalEntityInfoMerger());
            m.UserData = Merge(p1.UserData, p2.UserData, new EntityInfoMerger());
            m.Addresses = Merge(p1.Addresses, p2.Addresses, new AddressEntityInfoMerger());
            return m;
        }

        private List<UserInfo> Merge(List<UserInfo> p1, List<UserInfo> p2, IUserInfoMerger userInfoMerger)
        {
            if (ExclusiveNotEmpty(p1, p2, out var m)) return m;

            if (p1 == null || p2 == null)
                return null;

            var eic = new UserInfoComparerById();

            m = new List<UserInfo>();

            // merge most recent existing that aren't deleted... don't use eic comparer with the union!
            var qToUpdate = from x in p1.Union(p2)
                            group x by new { x.FirstName, x.LastName, x.Gender, x.BirthDate }
                into g
                            let mostRecentDate = g.Max(mrd => mrd.TimestampDate)
                            select g.First(xx => xx.TimestampDate == mostRecentDate);

            var toMerge = qToUpdate.Where(x => x.TimestampType != TimestampType.Deleted);

            foreach (var e in toMerge)
            {
                var pp1 = p1.FirstOrDefault(x => eic.Equals(x, e) && x.TimestampType == TimestampType.Updated);
                var pp2 = p2.FirstOrDefault(x => eic.Equals(x, e) && x.TimestampType == TimestampType.Updated);
                m.Add(userInfoMerger.Merge(pp1, pp2));
            }

            return m;
        }

        public static List<EntityInfo> Merge(List<EntityInfo> p1, List<EntityInfo> p2, IEntityInfoMerger entityMerger)
        {
            if (ExclusiveNotEmpty(p1, p2, out var m)) return m;

            if (p1 == null || p2 == null) return null;

            var eic = new EntityInfoComparerById();

            m = new List<EntityInfo>();

            // merge most recent existing that aren't deleted... don't use eic comparer with the union!
            var qToUpdate = from x in p1.Union(p2)
                            group x by x.Id
                into g
                            let mostRecentDate = g.Max(mrd => mrd.TimestampDate)
                            select g.First(xx => xx.TimestampDate == mostRecentDate);

            var toMerge = qToUpdate.Where(x => x.TimestampType != TimestampType.Deleted);

            foreach (var e in toMerge)
            {
                var pp1 = p1.FirstOrDefault(x => eic.Equals(x, e) && x.TimestampType == TimestampType.Updated);
                var pp2 = p2.FirstOrDefault(x => eic.Equals(x, e) && x.TimestampType == TimestampType.Updated);
                m.Add(entityMerger.Merge(pp1, pp2));
            }

            return m;
        }

        private class InfoEntityComparer : IEqualityComparer<Info>
        {
            public bool Equals(Info x, Info y)
            {
                return string.Compare(x.Id, y.Id) == 0;
            }

            public int GetHashCode(Info obj)
            {
                return obj.Value.GetHashCode();
            }
        }

        private class FamilyMemberUserInfoMerger : IUserInfoMerger
        {
            public UserInfo Merge(UserInfo p1, UserInfo p2)
            {
                if (Exclusive(p1, p2, out var m))
                    return m;
                m = UserInfo.TimestampedClone(p1, p2);

                m.Payors = HealthgradesProfileMerger.Merge(p1.Payors, p2.Payors, new PayorEntityInfoMerger());
                m.ProviderIds =
                    HealthgradesProfileMerger.Merge(p1.ProviderIds, p2.ProviderIds, new ProviderEntityInfoMerger());
                m.HospitalIds =
                    HealthgradesProfileMerger.Merge(p1.HospitalIds, p2.HospitalIds, new HospitalEntityInfoMerger());
                m.UserData = HealthgradesProfileMerger.Merge(p1.UserData, p2.UserData, new EntityInfoMerger());
                m.Addresses =
                    HealthgradesProfileMerger.Merge(p1.Addresses, p2.Addresses, new AddressEntityInfoMerger());

                return m;
            }
        }

        private class EntityInfoMerger : IEntityInfoMerger
        {
            public EntityInfo Merge(EntityInfo p1, EntityInfo p2)
            {
                if (Exclusive(p1, p2, out var r)) return r;

                if (!Exclusive(p1.UserLocation, p2.UserLocation, out var latestUserLocation))
                    latestUserLocation = p1.TimestampDate >= p2.TimestampDate ? p1.UserLocation : p2.UserLocation;

                if (!Exclusive(p1.SearchLocation, p2.SearchLocation, out var latestSearchLocation))
                    latestSearchLocation = p1.TimestampDate >= p2.TimestampDate ? p1.SearchLocation : p2.SearchLocation;

                r = new EntityInfo
                {
                    Id = p1.Id,
                    InfoType = p1.InfoType,
                    TimestampType = TimestampType.Updated,
                    TimestampDate = p1.TimestampDate >= p2.TimestampDate ? p1.TimestampDate : p2.TimestampDate,
                    UserLocation = latestUserLocation,
                    SearchLocation = latestSearchLocation
                };

                if (Exclusive(p1.Items, p2.Items, out var items))
                {
                    r.Items = items;
                }
                else
                {
                    r.Items = new List<Info>();

                    var iec = new InfoEntityComparer();

                    var qToUpdate = from x in p1.Items.Union(p2.Items)
                                    group x by x.Id
                        into g
                                    let mostRecentDate = g.Max(mrd => mrd.TimestampDate)
                                    select g.First(xx => xx.TimestampDate == mostRecentDate);

                    var toMerge = qToUpdate.Where(x => x.TimestampType != TimestampType.Deleted);

                    foreach (var e in toMerge)
                    {
                        var pp1 = p1.Items.FirstOrDefault(x =>
                            iec.Equals(x, e) && x.TimestampType == TimestampType.Updated);
                        var pp2 = p2.Items.FirstOrDefault(x =>
                            iec.Equals(x, e) && x.TimestampType == TimestampType.Updated);

                        var p = Merge(pp1, pp2);
                        if (p != null)
                            r.Items.Add(p);
                    }
                }

                return r;
            }

            private Info Merge(Info p1, Info p2)
            {
                if (Exclusive(p1, p2, out var p)) return p;

                if (p1.TimestampDate >= p2.TimestampDate)
                    return p1;

                return p2;
            }
        }

        // Just using same EntityInfoMerger logic for all entity merging logic for now
        private class ProviderEntityInfoMerger : EntityInfoMerger
        {
        }

        private class HospitalEntityInfoMerger : EntityInfoMerger
        {
        }

        private class PayorEntityInfoMerger : EntityInfoMerger
        {
        }

        private class AddressEntityInfoMerger : EntityInfoMerger
        {
        }

        private class RecommendedProviderEntityInfoMerger : EntityInfoMerger
        {
        }

        private class GroupEntityInfoMerger : EntityInfoMerger
        {
        }

        private class FacebookThirdPartyIdEntityInfoMerger : EntityInfoMerger
        {
        }

        private class EntityInfoComparerById : IEqualityComparer<EntityInfo>
        {
            public bool Equals(EntityInfo x, EntityInfo y)
            {
                return string.Compare(x.Id.ToLower(), y.Id.ToLower(), false) == 0;
            }

            public int GetHashCode(EntityInfo obj)
            {
                return obj.Id.GetHashCode();
            }
        }

        private class UserInfoComparerById : IEqualityComparer<UserInfo>
        {
            public bool Equals(UserInfo x, UserInfo y)
            {
                return string.Compare(x.Id, y.Id, false) == 0;
            }

            public int GetHashCode(UserInfo obj)
            {
                return obj.Id.GetHashCode();
            }
        }
    }
}