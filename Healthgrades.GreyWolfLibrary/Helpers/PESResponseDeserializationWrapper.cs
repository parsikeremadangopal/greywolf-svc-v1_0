﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Healthgrades.GreyWolfLibrary.Helpers
{
    /// <summary>
    /// whenever we deserialize a PES response we always care about what's inside the results
    /// which should correlates to a list<T> where T should have matching properties to the individual document
    /// </summary>
    /// <typeparam name="T">T should have matching properties to the individual document</typeparam>
    public class PESResponseDeserializationWrapper<T> where T : new()
    {
        [JsonProperty("total")]
        public int Total { get; set; }

        [JsonProperty("results")]
        public List<T> Results { get; set; }
    }
}