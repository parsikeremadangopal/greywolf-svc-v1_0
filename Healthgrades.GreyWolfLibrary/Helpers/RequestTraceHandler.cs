﻿using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace Healthgrades.GreyWolfLibrary.Helpers
{
    public class RequestTraceHandler : DelegatingHandler
    {
        private ISystemWebHelper _helper { get; set; }

        public RequestTraceHandler(ISystemWebHelper webHelper)
        {
            _helper = webHelper;
        }

        protected override async Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            var sw = Stopwatch.StartNew();

            var response = await base.SendAsync(request, cancellationToken);

            RequestTrace requestTrace = new RequestTrace { TraceUri = request.RequestUri.AbsoluteUri.ToString(), ContentLength = response.Content.Headers.ContentLength == null ? 0 : (int)response.Content.Headers.ContentLength, ElapsedMilliseconds = (int)sw.ElapsedMilliseconds };
            _helper.AppendRequestTrace(requestTrace);

            return response;
        }
    }
}