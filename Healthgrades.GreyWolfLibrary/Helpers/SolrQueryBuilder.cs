﻿using Healthgrades.GreyWolfLibrary.Interfaces;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Healthgrades.GreyWolfLibrary.Helpers
{
    public class SolrQueryBuilder : ISolrQueryBuilder
    {
        public SolrQueryBuilder()
        {
            FieldList = new List<string>();
            FilterQuery = new List<string>();
        }

        public void ClearQuery()
        {
            FilterQuery.Clear();
            FieldList.Clear();
            Sort = null;
            JSONFieldSuffix = null;
        }

        /// <summary>
        ///     Creates the query syntax based on the properties
        /// </summary>
        /// <returns></returns>
        public string CreateQuery()
        {
            var sb = new StringBuilder();
            sb.Append("/select?");
            sb.Append("q=*");

            // If the field list is populated, add it to the query string
            if (FieldList != null && FieldList.Count() > 0)
            {
                sb.Append("&fl=");
                for (var i = 0; i < FieldList.Count(); i++)
                {
                    if (i > 0) sb.Append(",");
                    sb.Append(FieldList[i]);

                    if (JSONFieldSuffix != null && FieldList[i].EndsWith(JSONFieldSuffix)) sb.Append(":[json]");
                }
            }

            if (FilterQuery != null && FilterQuery.Count() > 0)
            {
                sb.Append("&fq=");
                foreach (var fq in FilterQuery)
                {
                    sb.Append(fq);
                    sb.Append(" ");
                }
            }

            sb.Append("&wt=json");

            sb.Replace(",", "%2C").Replace(" ", "%20").Replace("+", "%2B");

            Debug.WriteLine("Constructed Path: {0}", sb);

            return sb.ToString();
        }

        #region Properties

        public List<string> FieldList { get; set; }

        public List<string> FilterQuery { get; set; }

        public string Sort { get; set; }

        public string JSONFieldSuffix { get; set; }

        #endregion Properties
    }
}