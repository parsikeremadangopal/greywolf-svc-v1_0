﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Healthgrades.GreyWolfLibrary.Helpers
{
    /// <summary>
    ///     whenever we deserialize a solr response we always care about what's inside the docs
    ///     which should correlates to a list<T> where T should have matching properties to the individual document
    /// </summary>
    /// <typeparam name="T">T should have matching properties to the individual document</typeparam>
    public class SolrResponseDeserializationWrapper<T> where T : new()
    {
        public bool HasDocuments => Response != null && Response.Documents.Count > 0;

        [JsonProperty("facet_counts")] public FacetResponse FacetCounts { get; set; }

        [JsonProperty("response")] public ResponseModel Response { get; set; }

        //our solr response always consist of these properties
        public class ResponseModel
        {
            [JsonProperty("docs")] public List<T> Documents { get; set; }

            [JsonProperty("numFound")] public int NumFound { get; set; }

            [JsonProperty("start")] public int Start { get; set; }
        }

        public class FacetResponse
        {
            [JsonProperty("facet_fields")] public JObject FacetFields { get; set; }
        }
    }
}