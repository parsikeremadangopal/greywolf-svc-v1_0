﻿using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;

namespace Healthgrades.GreyWolfLibrary.Helpers
{
    public class SystemWebHelper : ISystemWebHelper
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public SystemWebHelper(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string DecodeHtmlCharacters(string source)
        {
            if (string.IsNullOrEmpty(source)) return null;
            return HttpUtility.HtmlDecode(source);
        }

        public NameValueCollection ParseQueryStringHelper(string queryString)
        {
            return HttpUtility.ParseQueryString(queryString);
        }

        public string GetHttpQueryString(string queryString)
        {
            return _httpContextAccessor.HttpContext.Request.Query[queryString];
        }

        public string GetHttpQueryString()
        {
            return _httpContextAccessor.HttpContext.Request.QueryString.Value;
        }

        public string GetHttpHeaders(string httpHeader)
        {
            return _httpContextAccessor.HttpContext.Request.Headers[httpHeader];
        }

        public string GetHttpUserAgent()
        {
            return _httpContextAccessor.HttpContext.Request.Headers["User-Agent"].ToString();
        }

        public string GetUserHostAddress()
        {
            return _httpContextAccessor.HttpContext.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress.ToString();
        }

        public NameValueCollection GetRequestHeaders()
        {
            var requestHeaders = new NameValueCollection();
            if (_httpContextAccessor.HttpContext.Request.Headers != null)
            {
                foreach (var header in _httpContextAccessor.HttpContext.Request.Headers)
                {
                    requestHeaders.Add(header.Key, header.Value.ToString());
                }
            }
            return requestHeaders;
        }

        public Uri GetRequestUrl()
        {
            var request = _httpContextAccessor.HttpContext.Request;

            var absoluteUri = string.Concat(
                        request.Scheme,
                        "://",
                        request.Host.ToUriComponent(),
                        request.PathBase.ToUriComponent(),
                        request.Path.ToUriComponent(),
                        request.QueryString.ToUriComponent());

            return new Uri(absoluteUri);
        }

        public Uri GetUrlReferrer()
        {
            var referrerUrl = _httpContextAccessor.HttpContext.Request.Headers["Referer"].ToString();
            var uri = !string.IsNullOrWhiteSpace(referrerUrl) && Uri.TryCreate(referrerUrl, UriKind.Absolute, out Uri parsedUri)
                ? parsedUri
                : null;
            return uri;
        }

        public string GetHostUrl()
        {
            return _httpContextAccessor.HttpContext.Request.Host.Host;
        }

        public string GetUrlPath()
        {
            return _httpContextAccessor.HttpContext.Request.Path.Value;
        }

        public void AppendRequestTrace(RequestTrace req)
        {
            if (_httpContextAccessor.HttpContext == null) return;

            if (_httpContextAccessor.HttpContext.Items["requestTrace"] == null)
            {
                _httpContextAccessor.HttpContext.Items["requestTrace"] = new List<RequestTrace>();
            }

            List<RequestTrace> myTrace = (List<RequestTrace>)_httpContextAccessor.HttpContext.Items["requestTrace"];
            myTrace.Add(req);
        }

        public List<RequestTrace> GetRequestTraces()
        {
            return (List<RequestTrace>)_httpContextAccessor.HttpContext.Items["requestTrace"];
        }
    }

    public static class HttpClientDefinitions
    {
        public const string DefaultServiceHttpClient = "DefaultServiceHttpClient";
    }
}