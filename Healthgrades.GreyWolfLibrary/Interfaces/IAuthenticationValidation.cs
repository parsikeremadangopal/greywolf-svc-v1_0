﻿using Healthgrades.GreyWolfLibrary.Models;
using System;

namespace Healthgrades.GreyWolfLibrary.Interfaces
{
    public interface IAuthenticationValidation
    {
        Tuple<bool, AuthenticationProps> ValidateAuthentication(string sessionToken);
    }
}