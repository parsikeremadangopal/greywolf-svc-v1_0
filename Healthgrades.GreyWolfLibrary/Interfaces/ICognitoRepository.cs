﻿using Healthgrades.GreyWolfLibrary.Models;

namespace Healthgrades.GreyWolfLibrary.Interfaces
{
    public interface ICognitoRepository
    {
        /// <summary>
        ///     Gets the current profile of a logged in user
        /// </summary>
        /// <param name="userIdentityId"></param>
        /// <returns></returns>
        HealthgradesProfile GetHealthgradesCognitoProfile(string userIdentityId);

        /// <summary>
        ///     Saves the given profile to the user service data store
        /// </summary>
        /// <param name="userIdentityId"></param>
        /// <param name="profile"></param>
        /// <returns></returns>
        HealthgradesProfile SaveHealthgradesCognitoProfile(string userIdentityId, HealthgradesProfile profile);

        /// <summary>
        ///     Gets the newly created cognitoid with its email and password
        /// </summary>
        /// <param name="accountRegister"></param>
        /// <returns></returns>
        LoginResponse GetRegisterCognitoId(AccountRegister accountRegister);

        /// <summary>
        ///     Gets the cognitoid of the profile with its email and password
        /// </summary>
        /// <param name="accountLogin"></param>
        /// <returns></returns>
        LoginResponse GetLoginCognitoId(AccountLogin accountLogin);

        /// <summary>
        ///     Gets refresh access to all details
        /// </summary>
        /// <param name="sessionToken"></param>
        /// <returns></returns>
        LoginResponse GetRefreshAccessToken(string sessionToken);
    }
}