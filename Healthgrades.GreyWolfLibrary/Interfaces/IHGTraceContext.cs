﻿namespace Healthgrades.GreyWolfLibrary.Interfaces
{
    public interface IHGTraceContext
    {
        string AmazonTraceId { get; }
    }
}