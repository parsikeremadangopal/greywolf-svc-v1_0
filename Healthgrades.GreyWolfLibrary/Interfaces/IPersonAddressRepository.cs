﻿using Healthgrades.GreyWolfLibrary.Models;
using System;

namespace Healthgrades.GreyWolfLibrary.Interfaces
{
    public interface IPersonAddressRepository
    {
        /// <summary>
        ///     Save a person facility object to the database
        /// </summary>
        /// <returns></returns>
        Guid SavePersonAddress(PersonAddress personAddressSaveRequest, string authId);
    }
}