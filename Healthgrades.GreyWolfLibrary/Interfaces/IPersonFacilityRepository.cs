﻿using Healthgrades.GreyWolfLibrary.Models;
using System;

namespace Healthgrades.GreyWolfLibrary.Interfaces
{
    public interface IPersonFacilityRepository
    {
        /// <summary>
        ///     Save a person facility object to the database
        /// </summary>
        /// <returns></returns>
        Guid SavePersonFacility(PersonFacility personFacilitySaveRequest, string authId);
    }
}