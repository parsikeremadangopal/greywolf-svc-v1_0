﻿using Healthgrades.GreyWolfLibrary.Models;
using System;

namespace Healthgrades.GreyWolfLibrary.Interfaces
{
    public interface IPersonInsRepository
    {
        /// <summary>
        ///     Saves a person insurance object to the database
        /// </summary>
        /// <param name="personInsDto"></param>
        /// <param name="authId"></param>
        /// <returns></returns>
        Guid SavePersonIns(PersonInsurance personInsDto, string authId);
    }
}