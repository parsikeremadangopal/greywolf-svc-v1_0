﻿using Healthgrades.GreyWolfLibrary.Models;
using System;

namespace Healthgrades.GreyWolfLibrary.Interfaces
{
    public interface IPersonProviderRepository
    {
        /// <summary>
        ///     Saves a person provider object to the database
        /// </summary>
        /// <param name="personProvider"></param>
        /// <param name="authId"></param>
        /// <returns></returns>
        Guid SavePersonProvider(PersonProvider personProvider, string authId);
    }
}