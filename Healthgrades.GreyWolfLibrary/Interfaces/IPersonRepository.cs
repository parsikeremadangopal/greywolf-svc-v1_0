﻿using Healthgrades.GreyWolfLibrary.Models;
using System;

namespace Healthgrades.GreyWolfLibrary.Interfaces
{
    public interface IPersonRepository
    {
        /// <summary>
        ///     Save a person object back to the database
        /// </summary>
        /// <param name="personSaveRequest"></param>
        /// <param name="sessionToken"></param>
        /// <returns></returns>
        Guid SavePerson(PersonSaveRequest personSaveRequest, string authId);
    }
}