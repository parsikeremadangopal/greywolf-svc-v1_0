﻿using Healthgrades.GreyWolfLibrary.Helpers;
using Healthgrades.GreyWolfLibrary.Models;
using Healthgrades.GreyWolfLibrary.Models.SOLR;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Healthgrades.GreyWolfLibrary.Interfaces
{
    public interface IProviderSolrRepository
    {
        /// <summary>
        ///     Gets a provider from Solr.
        /// </summary>
        /// <param name="pwids"></param>
        /// <returns>ProviderSolrDoc Deserialization Wrapper</returns>
        SolrResponseDeserializationWrapper<ProviderSolrDoc> GetProvidersByPwid(string[] pwids);
    }

    public interface IAutosuggestSolrRepository
    {
        /// <summary>
        ///     Gets prac spec vertical codes from solr
        /// </summary>
        /// <param name="practicingSpecialtyCodes"></param>
        /// <returns></returns>
        SolrResponseDeserializationWrapper<PracticingSpecialtyVerticalCodeSolrDoc>
            GetProviderVerticalsByPractingSpecialty(string[] practicingSpecialtyCodes);
    }

    public interface IPESServiceRepository
    {
        /// <summary>
        ///     Gets all the PES Ratings
        /// </summary>
        /// <param name="accountid"></param>
        /// <returns></returns>
        PESResponseDeserializationWrapper<PESServiceResult> GetPESRatings(string accountid);
    }

    public interface IOASServiceRepository
    {
        /// <summary>
        ///     Gets all the OAS Appointments
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        List<OASAppointment> GetOASAppointments(string accessToken);
    }

    public interface IAccountRepository
    {
        /// <summary>
        ///     Get account by AccountId
        /// </summary>
        /// <param name="authProps"></param>
        /// <returns></returns>
        Account GetAccountByAuthId(AuthenticationProps authProps);

        /// <summary>
        ///     Send Device push notifications
        /// </summary>
        /// <param name="devicePushNotificationRequest"></param>
        /// <returns></returns>
        List<JObject> DevicePushNotification(DevicePushNotificationRequest devicePushNotificationRequest);

        /// <summary>
        ///     Get account by login credentials
        /// </summary>
        /// <param name="accountLogin"></param>
        /// <returns></returns>
        Account Login(AccountLogin accountLogin);

        /// <summary>
        ///     Get account by register parameters
        /// </summary>
        /// <param name="accountRegister"></param>
        /// <returns></returns>
        Account Register(AccountRegister accountRegister);

        /// <summary>
        ///     Saves account data to the database
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        Guid SaveAccount(AccountSaveRequest account);

        /// <summary>
        ///     Saves device registration data to the cognito profile database
        /// </summary>
        /// <param name="accountDeviceRegisterRequest"></param>
        /// <returns></returns>
        Guid RegisterDevice(AccountDeviceRegisterRequest accountDeviceRegisterRequest, string cognitoId);

        /// <summary>
        ///     Deletes an account object and all persons associated with the account in the database
        /// </summary>
        /// <param name="authId"></param>
        /// <returns></returns>
        Guid DeleteAccount(string authId);

        /// <summary>
        ///     Saves profile data to Cognito database
        /// </summary>
        /// <param name="HGProfile"></param>
        /// <param name="sessionToken"></param>
        /// <returns></returns>
        Account SaveAndCreateAccount(Account newAccount);

        /// <summary>
        ///     Saves profile data to Cognito database
        /// </summary>
        /// <param name="HGProfile"></param>
        /// <param name="sessionToken"></param>
        /// <returns></returns>
        Account SaveAndMergeAccount(Account newAccount);
    }
}