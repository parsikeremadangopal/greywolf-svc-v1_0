﻿using System.Collections.Generic;

namespace Healthgrades.GreyWolfLibrary.Interfaces
{
    public interface ISolrQueryBuilder
    {
        string Sort { get; set; }

        /// <summary>
        ///     If this is set, the query builder will convert all fields with this suffix to JSON instead of a string
        /// </summary>
        string JSONFieldSuffix { get; set; }

        List<string> FieldList { get; set; }

        List<string> FilterQuery { get; set; }

        string CreateQuery();

        void ClearQuery();
    }
}