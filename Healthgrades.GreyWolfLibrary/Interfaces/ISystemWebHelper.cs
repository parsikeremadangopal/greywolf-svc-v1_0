﻿using Healthgrades.GreyWolfLibrary.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Healthgrades.GreyWolfLibrary.Interfaces
{
    public interface ISystemWebHelper
    {
        string DecodeHtmlCharacters(string source);

        NameValueCollection ParseQueryStringHelper(string queryString);

        string GetHttpQueryString();

        string GetHttpQueryString(string queryString);

        string GetHttpHeaders(string httpHeader);

        string GetHttpUserAgent();

        string GetUserHostAddress();

        NameValueCollection GetRequestHeaders();

        Uri GetRequestUrl();

        Uri GetUrlReferrer();

        string GetHostUrl();

        string GetUrlPath();

        void AppendRequestTrace(RequestTrace req);

        List<RequestTrace> GetRequestTraces();
    }
}