﻿using Healthgrades.GreyWolfLibrary.Models;
using System;
using System.Collections.Generic;

namespace Healthgrades.GreyWolfLibrary.Interfaces
{
    public interface IAccountTranslator
    {
        HealthgradesProfile ToHealthgradesProfile(AccountSaveRequest accountSaveRequest,
            HealthgradesProfile existingProfile);

        HealthgradesProfile ToHealthgradesProfile(Account newAccount);

        HealthgradesProfile MergeHealthgradesProfiles(HealthgradesProfile newProfile,
            HealthgradesProfile existingProfile);

        Account ToAccount(HealthgradesProfile hgProfile);

        Guid GetAccountId(HealthgradesProfile hgProfile);

        void UpdateUserInfo(PersonSaveRequest personSaveRequest, UserInfo existingUserInfo);

        UserInfo GetUserInfo(string personId, HealthgradesProfile hgProfile);

        UserInfo GetUserInfo(PersonSaveRequest personSaveRequest, HealthgradesProfile hgProfile);

        List<UserInfo> GetAllUsersInfo(HealthgradesProfile hgProfile);

        EntityInfo GetPersonProvider(UserInfo personForProvider, PersonProvider personProviderRequest);

        EntityInfo GetPersonProviderByProvider(UserInfo personForProvider, string providerId);

        EntityInfo GetPersonFacilitiesByFacility(UserInfo personForProvider, string facilityId);

        void UpdatePersonProvider(PersonProvider personProviderRequest, EntityInfo personProviderToUpdate,
            HealthgradesProfile hgProfile);

        EntityInfo GetFacility(UserInfo person, PersonFacility personFacilitySaveRequest);

        void UpdateFacility(PersonFacility facilitySaveRequest, EntityInfo facilityToUpdate,
            HealthgradesProfile hgProfile);

        EntityInfo GetPersonInsurance(UserInfo personForInsurance, PersonInsurance personInsuranceRequest);

        EntityInfo GetPersonAddress(UserInfo personForAddress, PersonAddress personAddressRequest);

        void UpdatePersonInsurance(PersonInsurance personInsuranceRequest, EntityInfo personInsuranceToUpdate,
            HealthgradesProfile hgProfile);

        void UpdatePersonAddress(PersonAddress personAddressRequest, EntityInfo personAddressToUpdate,
            HealthgradesProfile hgProfile);
    }
}