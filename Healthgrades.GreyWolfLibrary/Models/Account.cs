﻿using System;
using System.Collections.Generic;

namespace Healthgrades.GreyWolfLibrary.Models
{
    /// <summary>
    ///     An account that represents a user and all persons the user manages.  Directly related to an authorization
    ///     account.
    /// </summary>
    public class Account
    {
        /// <summary>
        ///     Identifier of this account, specific to the account
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        ///     The authorization id associcated with this account
        /// </summary>
        public string AuthId { get; set; }

        /// <summary>
        ///     A family name that is associated with this account
        /// </summary>
        public string FamilyName { get; set; }

        /// <summary>
        ///     Used to determine if the account is current lactive, if set to false will be considered to be not active
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        ///     Persons that are managed under this account.  One of the persons will be 'self' (the account owner) all
        ///     other persons are persons considered to have thier health be managed by 'self'
        /// </summary>
        public List<Person> Persons { get; set; }

        /// <summary>
        ///     Session Token required for user login
        /// </summary>
        public string SessionToken { get; set; }

        /// <summary>
        ///     Access Token required for user login
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        ///     Id Token required for the user login
        /// </summary>
        public string IdToken { get; set; }

        /// <summary>
        ///     UserId generated at cognito database
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        ///     Email Id of the user
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///     RefreshToken required for login
        /// </summary>
        public string RefreshToken { get; set; }

        /// <summary>
        /// Number of Upcoming appointments in OAS
        /// </summary>
        public string OASUpcoming { get; set; }

        /// <summary>
        /// Number of Approved PES reviews
        /// </summary>
        public string PESApproved { get; set; }

        /// <summary>
        ///     Device tokens required to send push notifications
        /// </summary>
        public string[] DeviceTokens { get; set; }
    }

    /// <summary>
    ///     The neccessary input for an account save request
    /// </summary>
    public class AccountSaveRequest
    {
        /// <summary>
        ///     Identifier of this account, specific to the account
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        ///     The authorization id associcated with this account
        /// </summary>
        public string AuthId { get; set; }

        /// <summary>
        ///     A family name that is associated with this account
        /// </summary>
        public string FamilyName { get; set; }

        /// <summary>
        ///     Used to determine if the account is current lactive, if set to false will be considered to be not active
        /// </summary>
        public bool IsActive { get; set; }
    }

    /// <summary>
    ///     The neccessary input for an account device registration request
    /// </summary>
    public class AccountDeviceRegisterRequest
    {
        /// <summary>
        ///     The device token associated with the account to be added or removed
        /// </summary>
        public string DeviceToken { get; set; }

        /// <summary>
        ///     Used to determine if the account is current lactive, if set to false will be considered to be not active
        /// </summary>
        public bool IsActive { get; set; }
    }

    /// <summary>
    ///     The neccessary input for push notifications to devices
    /// </summary>
    public class DevicePushNotificationRequest
    {
        /// <summary>
        ///     The authorization id associcated with this account
        /// </summary>
        public string AuthId { get; set; }

        /// <summary>
        ///     The device token associated with the account to be added or removed
        /// </summary>
        public string NotificationTitle { get; set; }

        /// <summary>
        ///     Used to determine if the account is current lactive, if set to false will be considered to be not active
        /// </summary>
        public string NotificationMessage { get; set; }

        /// <summary>
        /// Notification Category
        /// </summary>
        public string NotificationCategory { get; set; }

        /// <summary>
        /// Notification Appointment Id
        /// </summary>
        public string AppointmentId { get; set; }
    }

    /// <summary>
    ///     The neccessary input for an account to login
    /// </summary>
    public class AccountLogin
    {
        /// <summary>
        ///     Identifier of this account, specific to the account
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///     The password of this account
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        ///     The provider of this account
        /// </summary>
        public string Provider { get; set; }

        /// <summary>
        ///     The account details
        /// </summary>
        public Account Account { get; set; }
    }

    /// <summary>
    ///     Response of Login
    /// </summary>
    public class LoginResponse
    {
        /// <summary>
        ///     CognitoId
        /// </summary>
        public string CognitoId { get; set; }

        /// <summary>
        ///     SessionToken
        /// </summary>
        public string SessionToken { get; set; }

        /// <summary>
        ///     UserId
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        ///     EmailId
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///     IdToken
        /// </summary>
        public string IdToken { get; set; }

        /// <summary>
        ///     AccessToken
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        ///     RefreshToken
        /// </summary>
        public string RefreshToken { get; set; }
    }

    /// <summary>
    ///     The neccessary input for an account to login
    /// </summary>
    public class AccountRegister
    {
        /// <summary>
        ///     Identifier of this account, specific to the account
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///     The password of this account
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        ///     The provider of this account
        /// </summary>
        public string Provider { get; set; }

        /// <summary>
        ///     The source for creation of account i.e. iOS or Android App
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        ///     The account details
        /// </summary>
        public Account Account { get; set; }
    }
}