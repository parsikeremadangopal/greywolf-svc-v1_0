﻿namespace Healthgrades.GreyWolfLibrary.Models
{
    public class AppSettings
    {
        public string SolrProvider { get; set; }
        public string SolrAutosuggest { get; set; }
        public string account_authorization_server { get; set; }
        public string PESServiceUrl { get; set; }
        public string OASServiceUrl { get; set; }
        public string HostedPath { get; set; }
        public string FCMUrl { get; set; }
        public string FCMProjectId { get; set; }
        public string FCMServerKey { get; set; }
        public string FCMSenderId { get; set; }
        public string ServerRootAddress { get; set; }
    }

    public class AwsSettings
    {
        public string Region { get; set; }
        public string UserPoolClientId { get; set; }
        public string UserPoolId { get; set; }
        public string IdentityPoolId { get; set; }
    }
}