﻿namespace Healthgrades.GreyWolfLibrary.Models
{
    public class AuthenticationProps
    {
        public string AuthenticationId { get; set; }
        public string SessionToken { get; set; }
    }
}