﻿namespace Healthgrades.GreyWolfLibrary.Models
{
    /// <summary>
    ///     An error response when an error has occured
    /// </summary>
    public class ErrorResponse
    {
        /// <summary>
        ///     An error code for returning (not http status code) more like sub status and should all be 1000 or greater.
        ///     Standard Http Errors should match the http status code.  Example: Error401 = Unauthorized.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        ///     A user friendly error message to return in the error
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        ///     Details of an error used for debug, logging purposes
        /// </summary>
        public string ErrorDetails { get; set; }
    }

    public static class ErrorFactory
    {
        private static ErrorResponse Error1001 => new ErrorResponse
        { ErrorCode = 1001, ErrorMessage = "Wellness account has not been created." };

        private static ErrorResponse Error401 => new ErrorResponse { ErrorCode = 401, ErrorMessage = "Unauthorized." };
        private static ErrorResponse Error403 => new ErrorResponse { ErrorCode = 403, ErrorMessage = "Forbidden." };

        private static ErrorResponse Error500 => new ErrorResponse
        { ErrorCode = 500, ErrorMessage = "Internal Server Error." };

        private static ErrorResponse Error404 => new ErrorResponse
        { ErrorCode = 404, ErrorMessage = "Account or Profile not found." };

        private static ErrorResponse Error409 => new ErrorResponse
        { ErrorCode = 409, ErrorMessage = "Account already exists." };

        /// <summary>
        ///     Error Response for Wellness Account Not Created.
        /// </summary>
        public static ErrorResponse ErrorWellnessAccountNotYetCreated => Error1001;

        /// <summary>
        ///     Error Response for Unauthorized.
        /// </summary>
        public static ErrorResponse ErrorUnauthorized => Error401;

        /// <summary>
        ///     Error Response for Forbidden.
        /// </summary>
        public static ErrorResponse ErrorForbidden => Error403;

        /// <summary>
        ///     Error Response for Unhandled Error.
        /// </summary>
        public static ErrorResponse ErrorUnhandledError => Error500;

        /// <summary>
        ///     Error Response for Unhandled Error.
        /// </summary>
        public static ErrorResponse ErrorAccountNotCreatedError => Error404;

        /// <summary>
        ///     Error Response for Already exists.
        /// </summary>
        public static ErrorResponse ErrorConflict => Error409;

        /// <summary>
        ///     Error Response for Unhandled Error with Details.
        /// </summary>
        /// <param name="errorDetails"></param>
        /// <returns>ErrorResponse</returns>
        public static ErrorResponse ErrorUnhandledErrorWithDetails(string errorDetails)
        {
            //ToDo: Do not send stack trace to the client.
            var error = ErrorUnhandledError;
            error.ErrorDetails = errorDetails;
            return error;
        }
    }
}