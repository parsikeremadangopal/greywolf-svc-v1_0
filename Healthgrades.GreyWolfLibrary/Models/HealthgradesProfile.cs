﻿using System;
using System.Collections.Generic;

namespace Healthgrades.GreyWolfLibrary.Models
{
    public enum TimestampType
    {
        Updated,
        Deleted
    }

    public enum LabelId
    {
        Comments, // Only one per provider/hospital
        OfficeLine,
        NurseLine,
        ReferralLine,
        Fax,
        Mobile,
        Website,
        Email,
        Address,
        Other
    }

    public class Timestamped
    {
        public Timestamped()
        {
            Id = Guid.NewGuid().ToString();
            TimestampType = TimestampType.Updated;
            TimestampDate = DateTime.UtcNow;
        }

        public string Id { get; set; }

        public TimestampType TimestampType { get; set; }

        public DateTime TimestampDate { get; set; }
    }

    public class Info : Timestamped
    {
        public Info()
        {
            MimeType = "text/plain";
            Value = "";
            LabelId = LabelId.Other;
        }

        public string MimeType { get; set; } // defaults to text/plain
        public string Value { get; set; }
        public LabelId LabelId { get; set; }
        public string Label { get; set; } // optional
        public bool? IsRef { get; set; } // optional
    }

    public class InfoList : Timestamped
    {
        public InfoList()
        {
            Items = new List<Info>();
        }

        public string InfoType { get; set; }

        public List<Info> Items { get; set; }
    }

    public class Coordinate
    {
        public Coordinate()
        {
            Latitude = 0;
            Longitude = 0;
        }

        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }
    }

    public class EntityInfo : InfoList
    {
        public Coordinate SearchLocation { get; set; }

        public Coordinate UserLocation { get; set; }
    }

    public enum Gender
    {
        F,
        M,
        U
    }

    public class UserInfo : Timestamped, ICloneable
    {
        public UserInfo()
        {
            Payors = new List<EntityInfo>();
            ProviderIds = new List<EntityInfo>();
            HospitalIds = new List<EntityInfo>();
            UserData = new List<EntityInfo>();
            Addresses = new List<EntityInfo>();
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Gender? Gender { get; set; }

        public DateTime? BirthDate { get; set; }

        public List<EntityInfo> Payors { get; set; }

        public List<EntityInfo> Addresses { get; set; }

        public List<EntityInfo> ProviderIds { get; set; }

        public List<EntityInfo> HospitalIds { get; set; }

        /// <summary>
        ///     A set of data that is name value paired sets of data that can be used for any purpose for the user
        /// </summary>
        public List<EntityInfo> UserData { get; set; }

        public object Clone()
        {
            var c = new UserInfo
            {
                BirthDate = BirthDate,
                FirstName = FirstName,
                LastName = LastName,
                Gender = Gender,
                TimestampDate = TimestampDate,
                TimestampType = TimestampType,
                Id = Id,
                Payors = Payors,
                ProviderIds = ProviderIds,
                HospitalIds = HospitalIds,
                UserData = UserData
            };
            return c;
        }

        public static UserInfo TimestampedClone(UserInfo value1, UserInfo value2)
        {
            return (value1.TimestampDate >= value2.TimestampDate ? value1.Clone() : value2.Clone()) as UserInfo;
        }
    }

    public class AlertCollectionState
    {
        public AlertCollectionState()
        {
            ReadIds = new List<long>();
            UnreadIds = new List<long>();
            DeletedIds = new List<long>();
        }

        public List<long> ReadIds { get; set; }

        public List<long> UnreadIds { get; set; }

        public List<long> DeletedIds { get; set; }
    }

    public class UserProfileInfo
    {
        public UserProfileInfo()
        {
            User = new UserInfo();
            FamilyMembers = new List<UserInfo>();
            Providers = new List<EntityInfo>();
            Hospitals = new List<EntityInfo>();
            RecommendedProviders = new List<EntityInfo>();
            Groups = new List<EntityInfo>();
            FacebookThirdPartyIds = new List<EntityInfo>();
            InboxAlerts = new AlertCollectionState();
            AccountData = new List<EntityInfo>();
        }

        public UserInfo User { get; set; }

        public List<UserInfo> FamilyMembers { get; set; }

        public List<EntityInfo> Providers { get; set; }

        public List<EntityInfo> Hospitals { get; set; }

        public List<EntityInfo> RecommendedProviders { get; set; }

        public List<EntityInfo> Groups { get; set; }

        public List<EntityInfo> FacebookThirdPartyIds { get; set; }

        public AlertCollectionState InboxAlerts { get; set; }

        public List<EntityInfo> AccountData { get; set; }
    }

    public class HealthgradesProfile
    {
        public HealthgradesProfile()
        {
            Version = "1.0";
            Name = "Healthgrades";
            UserProfile = new UserProfileInfo();
        }

        public UserProfileInfo UserProfile { get; set; }

        public string CognitoId { get; set; }

        public string SessionToken { get; set; }

        public string IdToken { get; set; }

        public string AccessToken { get; set; }

        public string UserId { get; set; }

        public string Email { get; set; }

        public string RefreshToken { get; set; }

        public string Version { get; set; }

        public string Name { get; set; }

        public string[] DeviceTokens { get; set; }
    }
}