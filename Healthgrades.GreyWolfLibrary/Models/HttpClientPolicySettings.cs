﻿namespace Healthgrades.GreyWolfLibrary.Models
{
    public class HttpClientPolicySettings
    {
        public int TimeoutMilliseconds { get; set; }
    }
}