﻿using System;
using System.Collections.Generic;

namespace Healthgrades.GreyWolfLibrary.Models
{
    /// <summary>
    ///     A person object contains profile information about a person associated with an account.
    /// </summary>
    public class Person
    {
        /// <summary>
        ///     The identifier of this person.  When null will indicate that this is a new person and when being
        ///     saved will create a new person for the account.
        /// </summary>
        public Guid? PersonId { get; set; }

        /// <summary>
        ///     The authorization id associated with this persons account
        /// </summary>
        public string AuthId { get; set; }

        /// <summary>
        ///     The account id this person is associated with
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        ///     Determines if this person is currently an active person on this account (when removing a person this should be set
        ///     to false on a save)
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        ///     Properties of a person that represent identifiable information
        /// </summary>
        public Identity Identity { get; set; }

        /// <summary>
        ///     Properties of a person that represent healthcare information of the person
        /// </summary>
        public Profile Profile { get; set; }

        /// <summary>
        ///     Used to determine if the person has completed onboarding
        /// </summary>
        public bool IsOnboarded =>
            Identity != null
            && Identity.FirstName != null
            && Identity.LastName != null
            && Identity.DOB != null;
    }

    /// <summary>
    ///     PersonSaveRequest
    /// </summary>
    public class PersonSaveRequest
    {
        /// <summary>
        ///     The identifier of this person.  When null will indicate that this is a new person and when being
        ///     saved will create a new person for the account.
        /// </summary>
        public Guid? PersonId { get; set; }

        /// <summary>
        ///     The account id this person is associated with
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        ///     Determines if this person is currently an active person on this account (when removing a person this should be set
        ///     to false on a save)
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        ///     Properties of a person that represent identifiable information
        /// </summary>
        public IdentitySaveRequest Identity { get; set; }
    }

    /// <summary>
    ///     Identifiable information for a person
    /// </summary>
    public class IdentitySaveRequest
    {
        /// <summary>
        ///     First name of this person
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        ///     Last name of this person
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        ///     Date of Birth of this person.  Used to calculate age.
        /// </summary>
        public DateTime? DOB { get; set; }

        /// <summary>
        ///     Image url for this persons avatar.
        /// </summary>
        public string AvatarImagePath { get; set; }

        /// <summary>
        ///     This persons gender (Male,Female)
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        ///     The relationship this person has to the account owner ('self' is the account owner)
        /// </summary>
        public string Relationship { get; set; }

        /// <summary>
        ///     First line of this persons home address
        /// </summary>
        public string AddressLine1 { get; set; }

        /// <summary>
        ///     Second line of this persons home address
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        ///     City of this persons home address
        /// </summary>
        public string City { get; set; }

        /// <summary>
        ///     State of this persons home address
        /// </summary>
        public string State { get; set; }

        /// <summary>
        ///     Zip code of this persons home address
        /// </summary>
        public string ZipCode { get; set; }
    }

    /// <summary>
    ///     Identifiable information for a person
    /// </summary>
    public class Identity
    {
        /// <summary>
        ///     First name of this person
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        ///     Last name of this person
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        ///     Date of Birth of this person.  Used to calculate age.
        /// </summary>
        public DateTime? DOB { get; set; }

        /// <summary>
        ///     A calculated property of the age of the person based on current date
        /// </summary>
        public int Age
        {
            get
            {
                // Save today's date.
                var now = DateTime.Today;

                var age = 0;
                if (DOB.HasValue)
                {
                    var birthDate = DOB.Value;

                    // Calculate the age.
                    age = now.Year - birthDate.Year;

                    if (now.Month < birthDate.Month || now.Month == birthDate.Month && now.Day < birthDate.Day) age--;
                }

                return age;
            }
        }

        /// <summary>
        ///     Image url for this persons avatar.
        /// </summary>
        public string AvatarImagePath { get; set; }

        /// <summary>
        ///     This persons gender (Male,Female)
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        ///     The relationship this person has to the account owner ('self' is the account owner)
        /// </summary>
        public string Relationship { get; set; }

        /// <summary>
        ///     First line of this persons home address
        /// </summary>
        public string AddressLine1 { get; set; }

        /// <summary>
        ///     Second line of this persons home address
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        ///     City of this persons home address
        /// </summary>
        public string City { get; set; }

        /// <summary>
        ///     State of this persons home address
        /// </summary>
        public string State { get; set; }

        /// <summary>
        ///     Zip code of this persons home address
        /// </summary>
        public string ZipCode { get; set; }
    }

    /// <summary>
    ///     Healthcare profile information for a person
    /// </summary>
    public class Profile
    {
        /// <summary>
        ///     List of insurances that this person is unsured under (this person's payors)
        /// </summary>
        public List<PersonInsurance> Insurances { get; set; }

        /// <summary>
        ///     List of providers that have been saved and designated as this persons care team members
        /// </summary>
        public List<PersonProvider> Providers { get; set; }

        /// <summary>
        ///     List of facilities that have been saved and designated as this persons facilities
        /// </summary>
        public List<PersonFacility> Facilities { get; set; }

        /// <summary>
        ///     List of Addresses that this person has
        /// </summary>
        public List<PersonAddress> Addresses { get; set; }
    }
}