﻿using System;

namespace Healthgrades.GreyWolfLibrary.Models
{
    /// <summary>
    ///     Address associated with a person
    /// </summary>
    public class PersonAddress
    {
        /// <summary>
        ///     Identifier of the this instance of an address associated to a person (when adding a new address to a person
        ///     this will be set to null to indicate it is new)
        /// </summary>
        public Guid? PersonAddressId { get; set; }

        /// <summary>
        ///     The identifier of the person that this address is associated with
        /// </summary>
        public Guid PersonId { get; set; }

        /// <summary>
        ///     The code for the address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        ///     The name of the address
        /// </summary>
        public string AddressName { get; set; }

        /// <summary>
        ///     The city of the address
        /// </summary>
        public string City { get; set; }

        /// <summary>
        ///     The state of the Address
        /// </summary>
        public string State { get; set; }

        /// <summary>
        ///     The Zip code of the address
        /// </summary>
        public string Zip { get; set; }

        /// <summary>
        ///     Determines if this address card is active (set to true when removing this card from the persons profile)
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        ///     Determines if this address card is default or not
        /// </summary>
        public bool IsDefault { get; set; }
    }
}