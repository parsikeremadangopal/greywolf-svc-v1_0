﻿using System;

namespace Healthgrades.GreyWolfLibrary.Models
{
    /// <summary>
    ///     A facility that a person is associated with.  A persons saved facility.
    /// </summary>
    public class PersonFacility
    {
        /// <summary>
        ///     An identifier of the person facility relationship when null indicates a new person facility relationship
        /// </summary>
        public Guid? PersonFacilityId { get; set; }

        /// <summary>
        ///     The identifier of the person that this facility is associated with
        /// </summary>
        public Guid PersonId { get; set; }

        /// <summary>
        ///     The facility identifier
        /// </summary>
        public string FacilityCode { get; set; }

        /// <summary>
        ///     Determines if this facility is active, when set to false this facility is not intended to be listed as a saved
        ///     facility for the person
        /// </summary>
        public bool IsActive { get; set; }
    }
}