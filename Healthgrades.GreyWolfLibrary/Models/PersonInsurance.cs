﻿using System;

namespace Healthgrades.GreyWolfLibrary.Models
{
    /// <summary>
    ///     Insurance (payor) associated with a person
    /// </summary>
    public class PersonInsurance
    {
        /// <summary>
        ///     Identifier of the this instance of an insurance associated to a person (when adding a new insurance to a person
        ///     this will be set to null to indicate it is new)
        /// </summary>
        public Guid? PersonInsuranceId { get; set; }

        /// <summary>
        ///     The identifier of the person that this insurance (payor) is associated with
        /// </summary>
        public Guid PersonId { get; set; }

        /// <summary>
        ///     The code for the payor
        /// </summary>
        public string InsuranceCode { get; set; }

        /// <summary>
        ///     The name of the payor
        /// </summary>
        public string InsuranceName { get; set; }

        /// <summary>
        ///     The url path for the front image of the insurance card
        /// </summary>
        public string CardPathFront { get; set; }

        /// <summary>
        ///     The url path for the back image of the insurance card
        /// </summary>
        public string CardPathBack { get; set; }

        /// <summary>
        ///     The insurance Carrier
        /// </summary>
        public string InsuranceCarrier { get; set; }

        /// <summary>
        ///     The insurance Plan Name
        /// </summary>
        public string InsurancePlanName { get; set; }

        /// <summary>
        ///     The insurance Subscriber Id
        /// </summary>
        public string InsuranceSubscriberId { get; set; }

        /// <summary>
        ///     Determines if this insurance card is active (set to true when removing this card from the persons profile)
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        ///     Determines if this insurance card is default or not
        /// </summary>
        public bool IsDefault { get; set; }
    }
}