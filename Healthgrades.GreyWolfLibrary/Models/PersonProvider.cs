﻿using System;
using System.Collections.Generic;

namespace Healthgrades.GreyWolfLibrary.Models
{
    /// <summary>
    ///     A PersonProvider links a person with a provider and indicates that the provider is a member of the persons care
    ///     team
    /// </summary>
    public class PersonProvider
    {
        private List<string> _specialtyCodes = new List<string>();
        private List<string> _verticalCodes = new List<string>();

        /// <summary>
        ///     The identifier of this instance of a person provider link
        /// </summary>
        public Guid? PersonProviderId { get; set; }

        /// <summary>
        ///     The identifier of the person a provider belongs to
        /// </summary>
        public Guid PersonId { get; set; }

        /// <summary>
        ///     The provider code (PWID) identifying the provider that is linked to a person
        /// </summary>
        public string ProviderCode { get; set; }

        /// <summary>
        ///     Notes about this provider.
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        ///     The specialty codes identifying the provider's primary specialties.
        /// </summary>
        public List<string> SpecialtyCodes
        {
            get
            {
                if (_specialtyCodes == null) _specialtyCodes = new List<string>();
                return _specialtyCodes;
            }
            set => _specialtyCodes = value;
        }

        /// <summary>
        ///     The vertical codes identifying the provider's vertical codes associated with the providers specialty codes
        /// </summary>
        public List<string> VerticalCodes
        {
            get
            {
                if (_verticalCodes == null) _verticalCodes = new List<string>();
                return _verticalCodes;
            }
            set => _verticalCodes = value;
        }

        /// <summary>
        ///     Set IsActive to false to indicate this provider should be removed as part the persons care team
        /// </summary>
        public bool IsActive { get; set; }
    }
}