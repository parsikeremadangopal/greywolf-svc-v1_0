﻿namespace Healthgrades.GreyWolfLibrary.Models
{
    public class RequestTrace
    {
        public int ContentLength { get; set; }

        public int ElapsedMilliseconds { get; set; }

        public string TraceUri { get; set; }
    }
}