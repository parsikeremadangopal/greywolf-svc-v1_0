﻿using System;

namespace Healthgrades.GreyWolfLibrary.Models
{
    /// <summary>
    ///     A response class indicating person id
    /// </summary>
    public class PersonIdResponse
    {
        /// <summary>
        ///     The id of the person created or updated
        /// </summary>
        public Guid PersonId { get; set; }
    }

    /// <summary>
    ///     A response class indicating an account id
    /// </summary>
    public class AccountIdResponse
    {
        /// <summary>
        ///     The id of the account created or updated
        /// </summary>
        public Guid AccountId { get; set; }
    }

    /// <summary>
    ///     A response class indicating a person's facility was created or updated
    /// </summary>
    public class PersonFacilityIdResponse
    {
        /// <summary>
        ///     The id of the person this facility was created or updated for
        /// </summary>
        public Guid PersonId { get; set; }

        /// <summary>
        ///     The id of the record of the person facility created or updated
        /// </summary>
        public Guid PersonFacilityId { get; set; }
    }

    /// <summary>
    ///     A response class indicating a person's address was created or updated
    /// </summary>
    public class PersonAddressIdResponse
    {
        /// <summary>
        ///     The id of the person this facility was created or updated for
        /// </summary>
        public Guid PersonId { get; set; }

        /// <summary>
        ///     The id of the record of the person facility created or updated
        /// </summary>
        public Guid PersonAddressId { get; set; }
    }

    /// <summary>
    ///     A response class indicating a person's insurance was created or updated
    /// </summary>
    public class PersonInsuranceIdResponse
    {
        /// <summary>
        ///     The id of the person this insurance was created or updated for
        /// </summary>
        public Guid PersonId { get; set; }

        /// <summary>
        ///     The id of the record of the person insurance created or updated
        /// </summary>
        public Guid PersonInsuranceId { get; set; }
    }

    /// <summary>
    ///     A response class indicating a person's provider was created or updated
    /// </summary>
    public class PersonProviderIdResponse
    {
        /// <summary>
        ///     The id of the person this provider was created or updated for
        /// </summary>
        public Guid PersonId { get; set; }

        /// <summary>
        ///     The id of the record of the person provider created or updated
        /// </summary>
        public Guid PersonProviderId { get; set; }
    }
}