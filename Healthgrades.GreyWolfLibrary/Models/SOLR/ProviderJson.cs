﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Healthgrades.GreyWolfLibrary.Models.SOLR
{
    /// <summary>
    /// </summary>
    [Serializable]
    [DataContract]
    public class ProviderJson
    {
        private IEnumerable<ProviderPracticingSpecialty> _specialty = new List<ProviderPracticingSpecialty>();

        /// <summary>
        ///     Gets or sets the First Name of the Person.
        /// </summary>
        [DataMember]
        public string Id { get; set; }

        /// <summary>
        ///     Gets or sets the First Name of the Person.
        /// </summary>
        [DataMember]
        public string FirstName { get; set; }

        /// <summary>
        ///     Gets or sets the persons middle name.
        /// </summary>
        [DataMember]
        public string MiddleName { get; set; }

        /// <summary>
        ///     Gets or sets the persons last name.
        /// </summary>
        [DataMember]
        public string LastName { get; set; }

        /// <summary>
        ///     Gets or sets the persons degree.
        /// </summary>
        [DataMember]
        public string Degree { get; set; }

        /// <summary>
        ///     Gets or sets the person's date of birth.
        /// </summary>
        [DataMember]
        public DateTime Dob { get; set; }

        /// <summary>
        ///     IEnumerable list of ProviderPracticingSpecialty.
        /// </summary>
        [DataMember]
        public IEnumerable<ProviderPracticingSpecialty> ProviderPracticingSpecialties
        {
            get => _specialty ?? new List<ProviderPracticingSpecialty>();
            set => _specialty = value;
        }
    }
}