﻿using System;
using System.Runtime.Serialization;

namespace Healthgrades.GreyWolfLibrary.Models.SOLR
{
    /// <summary>
    ///     List of Provider Practicing Specialty
    /// </summary>
    [Serializable]
    [DataContract]
    public class ProviderPracticingSpecialty
    {
        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        [DataMember]
        public string PracticingSpecialityName { get; set; }

        /// <summary>
        ///     Gets or sets the code.
        /// </summary>
        [DataMember]
        public string PracticingSpecialityCode { get; set; }

        /// <summary>
        ///     Gets or sets the rank.
        /// </summary>
        [DataMember]
        public string PracticingSpecialityRank { get; set; }

        /// <summary>
        ///     Gets or sets the search 1 = true, 0 = false.
        /// </summary>
        [DataMember]
        public int PracticingSpecialitySearch { get; set; }

        /// <summary>
        ///     Gets or sets the singluar name for the specialty.
        /// </summary>
        [DataMember]
        public string PracticingSpecialityNameIst { get; set; }

        /// <summary>
        ///     Gets or sets the plural name for the specialty.
        /// </summary>
        [DataMember]
        public string PracticingSpecialityNameIsts { get; set; }
    }
}