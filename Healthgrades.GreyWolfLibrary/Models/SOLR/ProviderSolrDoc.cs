﻿using System;
using System.Runtime.Serialization;

namespace Healthgrades.GreyWolfLibrary.Models.SOLR
{
    [DataContract]
    public class ProviderSolrDoc
    {
        [DataMember] public ProviderJson provider_json { get; set; }
    }

    [DataContract]
    public class PracticingSpecialtyVerticalCodeSolrDoc
    {
        /// <summary>
        ///     The practicing specialty vertical code
        /// </summary>
        [DataMember]
        public string prac_spec_vert_code { get; set; }
    }

    [DataContract]
    public class PESServiceResult
    {
        [DataMember(IsRequired = false)]
        public Comment comment { get; set; }
    }

    public class Comment
    {
        [DataMember(IsRequired = false)]
        public int statusId { get; set; }

        [DataMember(IsRequired = false)]
        public string status { get; set; }
    }

    [DataContract]
    public class OASAppointment
    {
        [DataMember(IsRequired = false)]
        public int AppointmentId { get; set; }

        [DataMember(IsRequired = false)]
        public DateTime DateTime { get; set; }

        [DataMember(IsRequired = false)]
        public DateTime DateTimeUtc { get; set; }

        [DataMember(IsRequired = false)]
        public string Status { get; set; }

        [DataMember(IsRequired = false)]
        public OASAppointment Response { get; set; }
    }
}