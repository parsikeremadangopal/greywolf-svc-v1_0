﻿using Healthgrades.GreyWolfLibrary.Helpers;
using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace Healthgrades.GreyWolfLibrary.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private readonly ICognitoRepository _CognitoRepository;
        private readonly IAccountTranslator _AccountTranslator;
        private readonly HttpClient _Requestor;
        private ILogger<AccountRepository> LoggingService;

        //private readonly string _FCMProjectId;
        private readonly string _FCMServerKey;

        private readonly string _FCMSenderId;
        private readonly string _FCMUrl;

        /// <summary>
        ///     Constructor for Account Repository
        /// </summary>
        /// <param name="logging"></param>
        /// <param name="cognitoRepository"></param>
        /// <param name="accountTranslator"></param>
        public AccountRepository(ICognitoRepository cognitoRepository,
            IAccountTranslator accountTranslator,
            IHttpClientFactory clientFactory,
            ILogger<AccountRepository> loggingService,
            string fcmUrl,
            //string fcmProjectId,
            string fcmServerKey,
            string fcmSenderId)
        {
            _CognitoRepository = cognitoRepository;
            _AccountTranslator = accountTranslator;
            _Requestor = clientFactory.CreateClient(HttpClientDefinitions.DefaultServiceHttpClient);
            LoggingService = loggingService;
            _FCMUrl = fcmUrl;
            //_FCMProjectId = fcmProjectId;
            _FCMServerKey = fcmServerKey;
            _FCMSenderId = fcmSenderId;
        }

        /// <summary>
        ///     Get account from Cognito
        /// </summary>
        /// <param name="authId"></param>
        /// <returns></returns>
        public Account GetAccountByAuthId(AuthenticationProps authProps)
        {
            var hgProfile = _CognitoRepository.GetHealthgradesCognitoProfile(authProps.AuthenticationId);
            if (hgProfile == null)
            {
                LoggingService.LogError("Cognito Id = " + authProps.AuthenticationId + " seems to be created but profile doesn't exist on IdentityPool ");
                return null;
            }
            hgProfile.SessionToken = authProps.SessionToken;
            return _AccountTranslator.ToAccount(hgProfile);
        }

        /// <summary>
        ///     Send Device Push Notifications
        /// </summary>
        /// <param name="devicePushNotificationRequest"></param>
        /// <returns></returns>
        public List<JObject> DevicePushNotification(DevicePushNotificationRequest devicePushNotificationRequest)
        {
            var hgProfile = _CognitoRepository.GetHealthgradesCognitoProfile(devicePushNotificationRequest.AuthId);
            if (hgProfile == null)
            {
                throw new Exception("User not found");
            }

            List<JObject> responses = new List<JObject>();

            if (hgProfile.DeviceTokens != null && hgProfile.DeviceTokens.Length > 0)
            {
                _Requestor.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("key={0}", _FCMServerKey));
                _Requestor.DefaultRequestHeaders.TryAddWithoutValidation("Sender", string.Format("id={0}", _FCMSenderId));

                foreach (string deviceToken in hgProfile.DeviceTokens)
                {
                    var jo = JObject.Parse(
                                _Requestor
                                .PostAsJsonAsync(_FCMUrl, JObject.FromObject(new
                                {
                                    notification = new
                                    {
                                        title = devicePushNotificationRequest.NotificationTitle,
                                        body = devicePushNotificationRequest.NotificationMessage,
                                        sound = "default"
                                    },
                                    data = new
                                    {
                                        category = devicePushNotificationRequest.NotificationCategory,
                                        appointmentId = devicePushNotificationRequest.AppointmentId,
                                        cognitoId = devicePushNotificationRequest.AuthId,
                                    },
                                    to = deviceToken,
                                    priority = "high",
                                    content_available = true,
                                }))
                                .Result
                                .Content
                                .ReadAsStringAsync()
                                .Result);

                    responses.Add(jo);
                }
            }
            return responses;
        }

        /// <summary>
        ///     Get account from Cognito
        /// </summary>
        /// <param name="authId"></param>
        /// <returns></returns>
        public Account Login(AccountLogin accountLogin)
        {
            var loginResponse = _CognitoRepository.GetLoginCognitoId(accountLogin);
            var existingProfile = _CognitoRepository.GetHealthgradesCognitoProfile(loginResponse.CognitoId);
            if (accountLogin.Account != null)
            {
                var newProfile = _AccountTranslator.ToHealthgradesProfile(accountLogin.Account);

                existingProfile = existingProfile != null
                    ? _AccountTranslator.MergeHealthgradesProfiles(newProfile, existingProfile)
                    : newProfile;

                existingProfile =
                    _CognitoRepository.SaveHealthgradesCognitoProfile(loginResponse.CognitoId, existingProfile);
            }

            existingProfile.SessionToken = loginResponse.RefreshToken; // This is purposely added
            existingProfile.AccessToken = loginResponse.AccessToken;
            existingProfile.CognitoId = loginResponse.CognitoId;
            existingProfile.Email = loginResponse.Email;
            existingProfile.IdToken = loginResponse.IdToken;
            existingProfile.RefreshToken = loginResponse.RefreshToken;
            existingProfile.UserId = loginResponse.UserId;
            return _AccountTranslator.ToAccount(existingProfile);
        }

        /// <summary>
        ///     Get account from Cognito
        /// </summary>
        /// <param name="accountRegister"></param>
        /// <returns></returns>
        public Account Register(AccountRegister accountRegister)
        {
            var loginResponse = _CognitoRepository.GetRegisterCognitoId(accountRegister);

            var newProfile = accountRegister.Account == null
                ? new HealthgradesProfile
                {
                    UserProfile = new UserProfileInfo
                    {
                        AccountData = new List<EntityInfo>(),
                        User = new UserInfo
                        {
                            UserData = new List<EntityInfo>()
                        }
                    }
                }
                : _AccountTranslator.ToHealthgradesProfile(accountRegister.Account);

            newProfile = _CognitoRepository.SaveHealthgradesCognitoProfile(loginResponse.CognitoId, newProfile);
            newProfile.SessionToken = loginResponse.SessionToken;
            newProfile.AccessToken = loginResponse.AccessToken;
            newProfile.CognitoId = loginResponse.CognitoId;
            newProfile.Email = loginResponse.Email;
            newProfile.IdToken = loginResponse.IdToken;
            newProfile.RefreshToken = loginResponse.RefreshToken;
            newProfile.UserId = loginResponse.UserId;
            return _AccountTranslator.ToAccount(newProfile);
        }

        /// <summary>
        ///     Save account into Cognito
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public Guid SaveAccount(AccountSaveRequest account)
        {
            var existingProfile = _CognitoRepository.GetHealthgradesCognitoProfile(account.AuthId);

            if (account.AccountId == Guid.Empty)
                account.AccountId = Guid.NewGuid();

            var hgProfile = _AccountTranslator.ToHealthgradesProfile(account, existingProfile);

            return _AccountTranslator.GetAccountId(_CognitoRepository
                .SaveHealthgradesCognitoProfile(account.AuthId, hgProfile));
        }

        /// <summary>
        ///     Save Device information into cognito account profile
        /// </summary>
        /// <param name="accountDeviceRegisterRequest"></param>
        /// <returns></returns>
        public Guid RegisterDevice(AccountDeviceRegisterRequest accountDeviceRegisterRequest, string cognitoId)
        {
            var existingProfile = _CognitoRepository.GetHealthgradesCognitoProfile(cognitoId);

            if (existingProfile == null)
            {
                return Guid.Empty;
            }

            if (accountDeviceRegisterRequest.IsActive)
            {
                if (existingProfile.DeviceTokens != null
                    && existingProfile.DeviceTokens.Length > 0
                    && existingProfile.DeviceTokens.Contains(accountDeviceRegisterRequest.DeviceToken))
                {
                    return _AccountTranslator.GetAccountId(existingProfile);
                }

                if (existingProfile.DeviceTokens == null || existingProfile.DeviceTokens.Length == 0)
                {
                    existingProfile.DeviceTokens = new string[1] { accountDeviceRegisterRequest.DeviceToken };
                }
                else
                {
                    var deviceTokenList = existingProfile.DeviceTokens.ToList();
                    deviceTokenList.Add(accountDeviceRegisterRequest.DeviceToken);
                    existingProfile.DeviceTokens = deviceTokenList.ToArray();
                }
            }
            else
            {
                if (existingProfile.DeviceTokens != null
                    && existingProfile.DeviceTokens.Length > 0
                    && existingProfile.DeviceTokens.Contains(accountDeviceRegisterRequest.DeviceToken))
                {
                    var deviceTokenList = existingProfile.DeviceTokens.ToList();
                    deviceTokenList.Remove(accountDeviceRegisterRequest.DeviceToken);
                    existingProfile.DeviceTokens = deviceTokenList.ToArray();
                }
            }
            return _AccountTranslator
                .GetAccountId(_CognitoRepository
                .SaveHealthgradesCognitoProfile(cognitoId, existingProfile));
        }

        public Account SaveAndCreateAccount(Account newAccount)
        {
            if (newAccount.AccountId == Guid.Empty)
                newAccount.AccountId = Guid.NewGuid();
            var newProfile = _AccountTranslator.ToHealthgradesProfile(newAccount);
            var resultProfile = _CognitoRepository.SaveHealthgradesCognitoProfile(newAccount.AuthId, newProfile);
            return _AccountTranslator.ToAccount(resultProfile);
        }

        public Account SaveAndMergeAccount(Account newAccount)
        {
            var existingProfile = _CognitoRepository.GetHealthgradesCognitoProfile(newAccount.AuthId);
            var newProfile = _AccountTranslator.ToHealthgradesProfile(newAccount);
            HealthgradesProfile mergedProfile = existingProfile != null
            ? _AccountTranslator.MergeHealthgradesProfiles(newProfile, existingProfile)
            : newProfile;
            var resultProfile = _CognitoRepository.SaveHealthgradesCognitoProfile(newAccount.AuthId, mergedProfile);
            return _AccountTranslator.ToAccount(resultProfile);
        }

        public Guid DeleteAccount(string authId)
        {
            LoggingService.LogInformation("The Delete account is not implemented. But it was triggerred for " + authId);
            throw new NotImplementedException();
        }
    }
}