﻿using Healthgrades.GreyWolfLibrary.Helpers;
using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models.SOLR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Net.Http;

namespace Healthgrades.GreyWolfLibrary.Repositories
{
    /// <summary>
    ///     Gets a Solr Provider.
    /// </summary>
    public class AutosuggestSolrRepository : IAutosuggestSolrRepository
    {
        private readonly HttpClient _requestor;
        private readonly string _solrAutosuggestUrl;
        private ILogger<AutosuggestSolrRepository> LoggingService;

        public AutosuggestSolrRepository(IHttpClientFactory clientFactory, ILogger<AutosuggestSolrRepository> loggingService, string solrAutosuggestUrl)
        {
            _requestor = clientFactory.CreateClient(HttpClientDefinitions.DefaultServiceHttpClient);
            LoggingService = loggingService;
            _solrAutosuggestUrl = solrAutosuggestUrl;
        }

        public SolrResponseDeserializationWrapper<PracticingSpecialtyVerticalCodeSolrDoc>
            GetProviderVerticalsByPractingSpecialty(string[] practicingSpecialtyCodes)
        {
            string url = string.Format(
                    "{0}/select?q=practicing_specialties_codes%3A({1})&wt=json&rows=60&fq=auto_type%3APRACSPECVERTICAL&fl=prac_spec_vert_code",
                    _solrAutosuggestUrl, string.Join("%20OR%20", practicingSpecialtyCodes));
            try
            {
                return JsonConvert
                   .DeserializeObject<SolrResponseDeserializationWrapper<PracticingSpecialtyVerticalCodeSolrDoc>>(
                       _requestor
                       .GetStringAsync(url)
                       .Result);
            }
            catch (Exception ex)
            {
                ex.Demystify();
                LoggingService.LogError(ex, "Exception caught while calling Solr Autosuggest url = " + url);
                throw;
            }
        }
    }
}