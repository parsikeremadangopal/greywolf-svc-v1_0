﻿using Amazon;
using Amazon.CognitoSync;
using Amazon.CognitoSync.Model;
using Healthgrades.GreyWolfLibrary.Helpers;
using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;

namespace Healthgrades.GreyWolfLibrary.Repositories
{
    public class SuccessTuple<T> : Tuple<bool, T>
    {
        public SuccessTuple(bool success, T value) : base(success, value)
        {
        }

        public bool Success => Item1;

        public T Value => Item2;
    }

    public class CognitoRepository : ICognitoRepository
    {
        private readonly HttpClient _requestor;
        private readonly string _accountAuthorizationUrl;
        private readonly string _identityPoolId;
        private readonly string _awsRegion;
        private ILogger<CognitoRepository> LoggingService;

        public CognitoRepository(IHttpClientFactory clientFactory, ILogger<CognitoRepository> loggingService, string accountAuthorizationUrl, string identityPoolId, string awsRegion)
        {
            _requestor = clientFactory.CreateClient(HttpClientDefinitions.DefaultServiceHttpClient);
            LoggingService = loggingService;
            _accountAuthorizationUrl = accountAuthorizationUrl;
            _identityPoolId = identityPoolId;
            _awsRegion = awsRegion;
        }

        public HealthgradesProfile GetHealthgradesCognitoProfile(string userIdentityId)
        {
            HealthgradesProfile hgProfile = null;

            if (userIdentityId != null)
            {
                var profileResult = GetProfile(userIdentityId);

                if (profileResult.Success)
                {
                    hgProfile = profileResult.Value;
                }
            }

            return hgProfile;
        }

        public HealthgradesProfile SaveHealthgradesCognitoProfile(string userIdentityId, HealthgradesProfile profile)
        {
            HealthgradesProfile updatedProfile = null;

            if (userIdentityId != null)
            {
                var profileResult = SaveProfile(userIdentityId, profile);
                if (profileResult.Success)
                {
                    updatedProfile = profileResult.Value;
                }
            }

            return updatedProfile;
        }

        public LoginResponse GetLoginCognitoId(AccountLogin accountLogin)
        {
            var cognitoUrl = _accountAuthorizationUrl + "/login";

            var jo = JObject.Parse(
                        _requestor
                        .PostAsJsonAsync(cognitoUrl, JObject.FromObject(new
                        {
                            email = accountLogin.Email,
                            password = accountLogin.Password,
                            provider = accountLogin.Provider
                        }))
                        .Result
                        .Content
                        .ReadAsStringAsync()
                        .Result);

            if (jo["success"].Value<bool>())
            {
                var loginResponse = new LoginResponse
                {
                    Email = jo["data"]?["email"]?.Value<string>(),
                    UserId = jo["data"]?["userId"]?.Value<string>(),
                    IdToken = jo["data"]?["IdToken"]?.Value<string>(),
                    AccessToken = jo["data"]?["accessToken"]?.Value<string>(),
                    SessionToken = jo["data"]?["session"]?["sessionToken"]?.Value<string>(),
                    CognitoId = jo["data"]?["session"]?["sessionPayload"]?["userIdentityId"]?.Value<string>(),
                    RefreshToken = jo["data"]?["session"]?["sessionPayload"]?["refreshToken"]?.Value<string>()
                };
                return loginResponse;
            }

            var error = jo["validationErrors"][0].Value<string>();
            if (error == "NotAuthorizedException")
                throw new Exception("User is not authorized");
            if (error == "UserLambdaValidationException")
                throw new Exception("User Lambda Validation");
            throw new Exception("User not found");
        }

        public LoginResponse GetRegisterCognitoId(AccountRegister accountRegister)
        {
            var cognitoUrl = _accountAuthorizationUrl + "/register";

            var jo = JObject.Parse(
                        _requestor
                        .PostAsJsonAsync(cognitoUrl, JObject.FromObject(new
                        {
                            email = accountRegister.Email,
                            password = accountRegister.Password,
                            provider = accountRegister.Provider,
                            source = accountRegister.Source
                        }))
                        .Result
                        .Content
                        .ReadAsStringAsync()
                        .Result);

            if (jo["success"].Value<bool>())
            {
                var accountLogin = new AccountLogin
                {
                    Email = accountRegister.Email,
                    Password = accountRegister.Password,
                    Provider = accountRegister.Provider
                };
                return GetLoginCognitoId(accountLogin);
            }

            if (jo["validationErrors"][0].Value<string>() == "UsernameExistsException")
                throw new Exception("Account already exists");
            throw new Exception("Internal server error");
        }

        private SuccessTuple<HealthgradesProfile> GetProfile(string userIdentityId)
        {
            return SaveProfile(userIdentityId, null);
        }

        private HealthgradesProfile GetCognitoProfile(string userIdentityId)
        {
            try
            {
                using (var awsSync = new AmazonCognitoSyncClient(RegionEndpoint.GetBySystemName(_awsRegion)))
                {
                    HealthgradesProfile profileData = null;
                    var awsRecords = awsSync.ListRecordsAsync(new ListRecordsRequest
                    {
                        DatasetName = "userData",
                        IdentityId = userIdentityId,
                        IdentityPoolId = _identityPoolId
                    });
                    if (awsRecords.Result.Records.Any())
                    {
                        var profileDataString = awsRecords.Result.Records[0].Value;
                        if (profileDataString.Length > 0)
                            profileData = JsonConvert.DeserializeObject<HealthgradesProfile>(profileDataString);
                        profileData.CognitoId = userIdentityId;
                        return profileData;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                ex.Demystify();
                LoggingService.LogError(ex, "Exception was caught in Get Cognito Profile");
                return null;
            }
        }

        private HealthgradesProfile SaveCognitoProfile(string userIdentityId, HealthgradesProfile profile)
        {
            try
            {
                using (var awsSync = new AmazonCognitoSyncClient(RegionEndpoint.GetBySystemName(_awsRegion)))
                {
                    var userRecord = awsSync.ListRecordsAsync(new ListRecordsRequest
                    {
                        DatasetName = "userData",
                        IdentityId = userIdentityId,
                        IdentityPoolId = _identityPoolId
                    });

                    var awsRecord = awsSync.UpdateRecordsAsync(new UpdateRecordsRequest
                    {
                        DatasetName = "userData",
                        IdentityId = userIdentityId,
                        IdentityPoolId = _identityPoolId,
                        RecordPatches = new List<RecordPatch>(1)
                        {
                            new RecordPatch
                            {
                                Key = "userProfile",
                                Value = JsonConvert.SerializeObject(profile),
                                Op = "replace",
                                SyncCount = userRecord.Result.DatasetSyncCount,
                                DeviceLastModifiedDate = DateTime.UtcNow
                            }
                        },
                        SyncSessionToken = userRecord.Result.SyncSessionToken
                    });

                    return (awsRecord.Result.HttpStatusCode == System.Net.HttpStatusCode.OK)
                    ? GetCognitoProfile(userIdentityId)
                    : null;
                }
            }
            catch (Exception ex)
            {
                ex.Demystify();
                LoggingService.LogError(ex, "Exception was caught in Save Cognito Profile");
                return null;
            }
        }

        /// <summary>
        ///     Saves the given profile and returns the updated profile
        /// </summary>
        /// <param name="userIdentityId"></param>
        /// <param name="profile">If null will simply get the existing profile</param>
        /// <returns></returns>
        private SuccessTuple<HealthgradesProfile> SaveProfile(string userIdentityId, HealthgradesProfile profile)
        {
            HealthgradesProfile profileData = profile == null
                ? GetCognitoProfile(userIdentityId)
                : SaveCognitoProfile(userIdentityId, profile);

            return profileData != null
                ? new SuccessTuple<HealthgradesProfile>(true, profileData)
                : new SuccessTuple<HealthgradesProfile>(false, profileData);
        }

        public LoginResponse GetRefreshAccessToken(string sessionToken)
        {
            var cognitoUrl = _accountAuthorizationUrl + "/refreshaccess";

            var jo = JObject.Parse(
                        _requestor
                        .PostAsJsonAsync(cognitoUrl, JObject.FromObject(new { token = sessionToken }))
                        .Result
                        .Content
                        .ReadAsStringAsync()
                        .Result);

            if (jo["success"].Value<bool>())
            {
                LoginResponse loginResponse = new LoginResponse
                {
                    Email = jo["data"]?["session"]?["sessionPayload"]?["userEmail"]?.Value<string>(),
                    AccessToken = jo["data"]?["session"]?["accessToken"]?.Value<string>(),
                    SessionToken = jo["data"]?["session"]?["sessionToken"]?.Value<string>(),
                    CognitoId = jo["data"]?["session"]?["sessionPayload"]?["userIdentityId"]?.Value<string>()
                };
                return loginResponse;
            }
            var error = jo["validationErrors"][0].Value<string>();
            if (error == "NotAuthorizedException")
                throw new Exception("User is not authorized");
            if (error == "UserLambdaValidationException")
                throw new Exception("User Lambda Validation");
            throw new Exception("User not found");
        }
    }
}