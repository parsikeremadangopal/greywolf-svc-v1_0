﻿using Healthgrades.GreyWolfLibrary.Helpers;
using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models.SOLR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;

namespace Healthgrades.GreyWolfLibrary.Repositories
{
    /// <summary>
    ///     Gets a Solr Provider.
    /// </summary>
    public class OASServiceRepository : IOASServiceRepository
    {
        private readonly HttpClient _requestor;
        private readonly string _oasServiceUrl;
        private ILogger<OASServiceRepository> LoggingService;

        public OASServiceRepository(IHttpClientFactory clientFactory, ILogger<OASServiceRepository> loggingService, string oasServiceUrl)
        {
            _requestor = clientFactory.CreateClient(HttpClientDefinitions.DefaultServiceHttpClient);
            LoggingService = loggingService;
            _oasServiceUrl = oasServiceUrl;
        }

        /// <summary>
        /// Gets all reviews from PES Service.
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public List<OASAppointment> GetOASAppointments(string accessToken)
        {
            Uri oasUri = new Uri(_oasServiceUrl);
            _requestor.DefaultRequestHeaders.Add("Authorization", string.Format("HGAuth {0}", accessToken));

            try
            {
                return JsonConvert.DeserializeObject<List<OASAppointment>>
               (_requestor.GetStringAsync(oasUri).Result);
            }
            catch (Exception ex)
            {
                ex.Demystify();
                LoggingService.LogError(ex, "Exception was caught while calling Oas Service Url = " + oasUri.AbsoluteUri);
                throw;
            }
        }
    }
}