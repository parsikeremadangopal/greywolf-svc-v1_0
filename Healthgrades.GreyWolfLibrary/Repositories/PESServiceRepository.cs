﻿using Healthgrades.GreyWolfLibrary.Helpers;
using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models.SOLR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net.Http;

namespace Healthgrades.GreyWolfLibrary.Repositories
{
    /// <summary>
    ///     Gets a Solr Provider.
    /// </summary>
    public class PESServiceRepository : IPESServiceRepository
    {
        private readonly HttpClient _requestor;
        private readonly string _pesServiceUrl;
        private ILogger<PESServiceRepository> LoggingService;

        public PESServiceRepository(IHttpClientFactory clientFactory, ILogger<PESServiceRepository> loggingService, string pesServiceUrl)
        {
            _requestor = clientFactory.CreateClient(HttpClientDefinitions.DefaultServiceHttpClient);
            LoggingService = loggingService;
            _pesServiceUrl = pesServiceUrl;
        }

        /// <summary>
        /// Gets all reviews from PES Service.
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public PESResponseDeserializationWrapper<PESServiceResult> GetPESRatings(string accountId)
        {
            Uri pesUri = new Uri(string.Format("{0}?AccountId={1}&IncludeUnpublished=true&IncludeAllAnswers=true&Page=1&PerPage=10000",
                _pesServiceUrl,
                accountId));
            try
            {
                return JsonConvert
                    .DeserializeObject<PESResponseDeserializationWrapper<PESServiceResult>>(
                    _requestor
                    .GetStringAsync(pesUri)
                    .Result);
            }
            catch (Exception ex)
            {
                LoggingService.LogError(ex, "Exception was caught while calling PES Service url = " + pesUri.AbsoluteUri);
                throw;
            }
        }
    }
}