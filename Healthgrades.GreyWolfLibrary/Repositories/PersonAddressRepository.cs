﻿using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models;
using Healthgrades.GreyWolfLibrary.Models.Constants;
using System;

namespace Healthgrades.GreyWolfLibrary.Repositories
{
    public class PersonAddressRepository : IPersonAddressRepository
    {
        private readonly ICognitoRepository _CognitoRepository;
        private readonly IAccountTranslator _AccountTranslator;

        public PersonAddressRepository(ICognitoRepository cognitoRepository,
            IAccountTranslator accountTranslator)
        {
            _CognitoRepository = cognitoRepository;
            _AccountTranslator = accountTranslator;
        }

        public Guid SavePersonAddress(PersonAddress personAddressSaveRequest, string authId)
        {
            var hgProfile = _CognitoRepository.GetHealthgradesCognitoProfile(authId);
            //Get a reference to the correct person
            var person = _AccountTranslator.GetUserInfo(personAddressSaveRequest.PersonId.ToString(), hgProfile);

            var addressUpdateToDefault = hgProfile.UserProfile.User.UserData.FindAll(x =>
                x.InfoType == Address.PersonAddressInfoTypeKey
                && x.TimestampType != TimestampType.Deleted);

            if (personAddressSaveRequest.IsDefault && addressUpdateToDefault.Count > 0)
                foreach (var addressUpdate in addressUpdateToDefault)
                    if (addressUpdate.Items.Count > 0)
                    {
                        var personAddress = new PersonAddress
                        {
                            PersonAddressId = Guid.Parse(addressUpdate.Id),
                            PersonId = personAddressSaveRequest.PersonId,
                            AddressName = addressUpdate.Items.Find(x =>
                                x.Label == Address.PersonAddressNameKey)?.Value,
                            Address = addressUpdate.Items.Find(x =>
                                x.Label == Address.PersonAddressKey)?.Value,
                            City = addressUpdate.Items.Find(x =>
                                x.Label == Address.PersonAddressCityKey)?.Value,
                            State = addressUpdate.Items.Find(x =>
                                x.Label == Address.PersonAddressStateKey)?.Value,
                            Zip = addressUpdate.Items.Find(x =>
                                x.Label == Address.PersonAddressZipKey)?.Value,
                            IsDefault = false,
                            IsActive = true
                        };
                        var personAddressToUpdate = _AccountTranslator.GetPersonAddress(person, personAddress);
                        _AccountTranslator.UpdatePersonAddress(personAddress, personAddressToUpdate, hgProfile);
                    }

            var addressToUpdate = _AccountTranslator.GetPersonAddress(person, personAddressSaveRequest);
            _AccountTranslator.UpdatePersonAddress(personAddressSaveRequest, addressToUpdate, hgProfile);
            _CognitoRepository.SaveHealthgradesCognitoProfile(authId, hgProfile);

            return personAddressSaveRequest.PersonAddressId.Value;
        }
    }
}