﻿using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models;
using System;

namespace Healthgrades.GreyWolfLibrary.Repositories
{
    public class PersonFacilityRepository : IPersonFacilityRepository
    {
        private readonly ICognitoRepository _CognitoRepository;
        private readonly IAccountTranslator _AccountTranslator;

        public PersonFacilityRepository(ICognitoRepository cognitoRepository,
            IAccountTranslator accountTranslator)
        {
            _CognitoRepository = cognitoRepository;
            _AccountTranslator = accountTranslator;
        }

        /// <summary>
        ///     Save person facility
        /// </summary>
        /// <param name="facilitySaveRequest"></param>
        /// <param name="authId"></param>
        /// <param name="sessionToken"></param>
        /// <returns></returns>
        public Guid SavePersonFacility(PersonFacility facilitySaveRequest, string authId)
        {
            var hgProfile = _CognitoRepository.GetHealthgradesCognitoProfile(authId);

            if (facilitySaveRequest.IsActive)
            {
                //Get a reference to the correct person
                var person = _AccountTranslator.GetUserInfo(facilitySaveRequest.PersonId.ToString(), hgProfile);
                var facilityToUpdate = _AccountTranslator.GetFacility(person, facilitySaveRequest);
                _AccountTranslator.UpdateFacility(facilitySaveRequest, facilityToUpdate, hgProfile);
            }
            else
            {
                var person = _AccountTranslator.GetAllUsersInfo(hgProfile);
                var FacilityIds = facilitySaveRequest.FacilityCode.Split(',');
                foreach (var personForTimeline in person)
                    foreach (var FacilityId in FacilityIds)
                    {
                        var facilityToUpdate =
                            _AccountTranslator.GetPersonFacilitiesByFacility(personForTimeline, FacilityId);
                        facilitySaveRequest.FacilityCode = FacilityId;
                        _AccountTranslator.UpdateFacility(facilitySaveRequest, facilityToUpdate, hgProfile);
                    }
            }

            _ = _CognitoRepository.SaveHealthgradesCognitoProfile(authId, hgProfile);
            return facilitySaveRequest.PersonFacilityId.Value;
        }
    }
}