﻿using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models;
using Healthgrades.GreyWolfLibrary.Models.Constants;
using System;

namespace Healthgrades.GreyWolfLibrary.Repositories
{
    public class PersonInsuranceRepository : IPersonInsRepository
    {
        private readonly ICognitoRepository _CognitoRepository;
        private readonly IAccountTranslator _AccountTranslator;

        public PersonInsuranceRepository(ICognitoRepository cognitoRepository,
            IAccountTranslator accountTranslator)
        {
            _CognitoRepository = cognitoRepository;
            _AccountTranslator = accountTranslator;
        }

        public Guid SavePersonIns(PersonInsurance personInsRequest, string authId)
        {
            var hgProfile = _CognitoRepository.GetHealthgradesCognitoProfile(authId);
            //Get a reference to the correct person
            var person = _AccountTranslator.GetUserInfo(personInsRequest.PersonId.ToString(), hgProfile);

            var insuranceUpdateToDefault = hgProfile.UserProfile.User.UserData.FindAll(x =>
                x.InfoType == Insurance.PersonInsuranceInfoTypeKey
                && x.TimestampType != TimestampType.Deleted);

            if (personInsRequest.IsDefault && insuranceUpdateToDefault.Count > 0)
                foreach (var insuranceUpdate in insuranceUpdateToDefault)
                    if (insuranceUpdate.Items.Count > 0)
                    {
                        var personInsurance = new PersonInsurance
                        {
                            PersonInsuranceId = Guid.Parse(insuranceUpdate.Id),
                            PersonId = personInsRequest.PersonId,
                            CardPathFront = insuranceUpdate.Items.Find(x =>
                                x.Label == Insurance.CardPathFrontKey)?.Value,
                            CardPathBack = insuranceUpdate.Items.Find(x =>
                                x.Label == Insurance.CardPathBackKey)?.Value,
                            InsuranceCarrier = insuranceUpdate.Items.Find(x =>
                                x.Label == Insurance.InsuranceCarrierKey)?.Value,
                            InsuranceCode = insuranceUpdate.Items.Find(x =>
                                x.Label == Insurance.InsuranceCodeKey)?.Value,
                            InsuranceName = insuranceUpdate.Items.Find(x =>
                                x.Label == Insurance.InsuranceNameKey)?.Value,
                            InsurancePlanName = insuranceUpdate.Items.Find(x =>
                                x.Label == Insurance.InsurancePlanNameKey)?.Value,
                            InsuranceSubscriberId = insuranceUpdate.Items.Find(x =>
                                x.Label == Insurance.InsuranceSubscriberIdKey)?.Value,
                            IsDefault = false,
                            IsActive = true
                        };
                        var personInsuranceToUpdate =
                            _AccountTranslator.GetPersonInsurance(person, personInsurance);
                        _AccountTranslator.UpdatePersonInsurance(personInsurance, personInsuranceToUpdate,
                            hgProfile);
                    }

            var insToUpdate = _AccountTranslator.GetPersonInsurance(person, personInsRequest);
            _AccountTranslator.UpdatePersonInsurance(personInsRequest, insToUpdate, hgProfile);
            _CognitoRepository.SaveHealthgradesCognitoProfile(authId, hgProfile);

            return personInsRequest.PersonInsuranceId.Value;
        }
    }
}