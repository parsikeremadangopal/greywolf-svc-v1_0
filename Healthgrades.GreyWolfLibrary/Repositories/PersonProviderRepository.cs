﻿using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models;
using System;

namespace Healthgrades.GreyWolfLibrary.Repositories
{
    public class PersonProviderRepository : IPersonProviderRepository
    {
        private readonly ICognitoRepository _CognitoRepository;
        private readonly IAccountTranslator _AccountTranslator;

        public PersonProviderRepository(ICognitoRepository cognitoRepository,
            IAccountTranslator accountTranslator)
        {
            _CognitoRepository = cognitoRepository;
            _AccountTranslator = accountTranslator;
        }

        /// <summary>
        ///     Save Person Provider
        /// </summary>
        /// <param name="personProviderRequest"></param>
        /// <param name="authId"></param>
        /// <param name="sessionToken"></param>
        /// <returns></returns>
        public Guid SavePersonProvider(PersonProvider personProviderRequest, string authId)
        {
            var hgProfile = _CognitoRepository.GetHealthgradesCognitoProfile(authId);

            if (personProviderRequest.IsActive)
            {
                //Get a reference to the correct person
                var personForTimeline =
                    _AccountTranslator.GetUserInfo(personProviderRequest.PersonId.ToString(), hgProfile);
                var personProviderToUpdate =
                    _AccountTranslator.GetPersonProvider(personForTimeline, personProviderRequest);
                _AccountTranslator.UpdatePersonProvider(personProviderRequest, personProviderToUpdate, hgProfile);
            }
            else
            {
                var personsForTimeline = _AccountTranslator.GetAllUsersInfo(hgProfile);
                var ProviderIds = personProviderRequest.ProviderCode.Split(',');
                foreach (var personForTimeline in personsForTimeline)
                    foreach (var ProviderId in ProviderIds)
                    {
                        var personProviderToUpdate =
                            _AccountTranslator.GetPersonProviderByProvider(personForTimeline, ProviderId);
                        personProviderRequest.ProviderCode = ProviderId;
                        _AccountTranslator.UpdatePersonProvider(personProviderRequest, personProviderToUpdate,
                            hgProfile);
                    }
            }

            _ = _CognitoRepository.SaveHealthgradesCognitoProfile(authId, hgProfile);

            return personProviderRequest.PersonProviderId.Value;
        }
    }
}