﻿using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models;
using System;

namespace Healthgrades.GreyWolfLibrary.Repositories
{
    public class PersonRepository : IPersonRepository
    {
        private readonly ICognitoRepository _CognitoRepository;
        private readonly IAccountTranslator _AccountTranslator;

        public PersonRepository(ICognitoRepository cognitoRepository,
            IAccountTranslator accountTranslator)
        {
            _CognitoRepository = cognitoRepository;
            _AccountTranslator = accountTranslator;
        }

        /// <summary>
        ///     Save person details
        /// </summary>
        /// <returns></returns>
        public Guid SavePerson(PersonSaveRequest personSaveRequest, string authId)
        {
            var hgProfile = _CognitoRepository.GetHealthgradesCognitoProfile(authId);

            //Get a reference to the correct person
            var profileToUpdate = _AccountTranslator.GetUserInfo(personSaveRequest, hgProfile);
            _AccountTranslator.UpdateUserInfo(personSaveRequest, profileToUpdate);
            _ = _CognitoRepository.SaveHealthgradesCognitoProfile(authId, hgProfile);

            return Guid.Parse(profileToUpdate.Id);
        }
    }
}