﻿using Healthgrades.GreyWolfLibrary.Helpers;
using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models.SOLR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Net.Http;

namespace Healthgrades.GreyWolfLibrary.Repositories
{
    /// <summary>
    ///     Gets a Solr Provider.
    /// </summary>
    public class ProviderSolrRepository : IProviderSolrRepository
    {
        private readonly HttpClient _requestor;
        private readonly string _solrProviderUrl;
        private ILogger<ProviderSolrRepository> LoggingService;

        public ProviderSolrRepository(IHttpClientFactory clientFactory, ILogger<ProviderSolrRepository> loggingService, string solrProviderUrl)
        {
            _requestor = clientFactory.CreateClient(HttpClientDefinitions.DefaultServiceHttpClient);
            LoggingService = loggingService;
            _solrProviderUrl = solrProviderUrl;
        }

        /// <summary>
        ///     Gets a provider from Solr.
        /// </summary>
        /// <param name="pwids"></param>
        /// <returns>ProviderSolrDoc DeserializationWrapper</returns>
        public SolrResponseDeserializationWrapper<ProviderSolrDoc> GetProvidersByPwid(string[] pwids)
        {
            string url = string.Format(
                    "{0}/select?omitHeader=true&wt=json&fl=provider_json:[json]&rows={1}&q=pwid:({2})",
                    _solrProviderUrl,
                    pwids.Length,
                    string.Concat("%22", string.Join("%22,%22", pwids), "%22"));
            try
            {
                return JsonConvert.DeserializeObject<SolrResponseDeserializationWrapper<ProviderSolrDoc>>
                    (_requestor.GetStringAsync(url).Result);
            }
            catch (Exception ex)
            {
                ex.Demystify();
                LoggingService.LogError(ex, "Exception caught while calling Solr Provider url = " + url);
                throw;
            }
        }
    }
}