using Healthgrades.Diagnostics;
using Healthgrades.Diagnostics.Interfaces;
using Healthgrades.Extensions.Logging;
using Healthgrades.Extensions.Logging.AspNetCore;
using Healthgrades.GreyWolfLibrary.Helpers;
using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models;
using Healthgrades.GreyWolfLibrary.Repositories;
using Healthgrades.GreyWolfServicesCore.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Net.Http;

namespace Healthgrades.GreyWolfServicesCore
{
    /// <summary>
    /// AutofacConfig dependancy injections
    /// </summary>
    public static class AutofacConfig
    {
        /// <summary>
        /// Autofac register framework components
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void RegisterComponents(IServiceCollection services, IConfiguration configuration)
        {
            var appSettings = configuration.GetSection("AppSettings").Get<AppSettings>();
            var awsSettings = configuration.GetSection("AWS").Get<AwsSettings>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<ISystemWebHelper, SystemWebHelper>();
            services.AddSingleton<Serilog.Core.ILogEventEnricher, HttpRequestEnricher>();
            services.AddTransient<LogEventEnricherMiddleware>();

            services.AddScoped<IHGTraceContext, HgWebTraceContext>();
            services.AddScoped<ISolrQueryBuilder, SolrQueryBuilder>();
            services.AddScoped<IDiagnosticTraceCollection, DiagnosticTraceCollection>();

            services.AddScoped<IAuthenticationValidation, HGAuthentication>(c => new HGAuthentication(
               c.GetService<IHttpClientFactory>(),
               c.GetService<ILogger<HGAuthentication>>(),
               appSettings.account_authorization_server));

            services.AddScoped<IAutosuggestSolrRepository, AutosuggestSolrRepository>(c => new AutosuggestSolrRepository(
               c.GetService<IHttpClientFactory>(),
               c.GetService<ILogger<AutosuggestSolrRepository>>(),
               appSettings.SolrAutosuggest));

            services.AddScoped<IProviderSolrRepository, ProviderSolrRepository>(c => new ProviderSolrRepository(
                c.GetService<IHttpClientFactory>(),
                c.GetService<ILogger<ProviderSolrRepository>>(),
                appSettings.SolrProvider));

            services.AddScoped<ICognitoRepository, CognitoRepository>(c => new CognitoRepository(
                c.GetService<IHttpClientFactory>(),
                c.GetService<ILogger<CognitoRepository>>(),
                appSettings.account_authorization_server,
                awsSettings.IdentityPoolId,
                awsSettings.Region));

            services.AddScoped<IOASServiceRepository, OASServiceRepository>(c => new OASServiceRepository(
                c.GetService<IHttpClientFactory>(),
                c.GetService<ILogger<OASServiceRepository>>(),
                appSettings.OASServiceUrl));

            services.AddScoped<IPESServiceRepository, PESServiceRepository>(c => new PESServiceRepository(
                c.GetService<IHttpClientFactory>(),
                c.GetService<ILogger<PESServiceRepository>>(),
                appSettings.PESServiceUrl));

            services.AddScoped<IAccountTranslator, AccountTranslator>(c => new AccountTranslator(
               c.GetService<IProviderSolrRepository>(),
               c.GetService<IAutosuggestSolrRepository>(),
               c.GetService<IOASServiceRepository>(),
               c.GetService<IPESServiceRepository>(),
               c.GetService<ICognitoRepository>(),
               c.GetService<ILogger<AccountTranslator>>()));

            services.AddScoped<IAccountRepository, AccountRepository>(c => new AccountRepository(
                c.GetService<ICognitoRepository>(),
                c.GetService<IAccountTranslator>(),
                c.GetService<IHttpClientFactory>(),
                c.GetService<ILogger<AccountRepository>>(),
                appSettings.FCMUrl,
                //appSettings.FCMProjectId,
                appSettings.FCMServerKey,
                appSettings.FCMSenderId));

            services.AddScoped<IPersonRepository, PersonRepository>();
            services.AddScoped<IPersonProviderRepository, PersonProviderRepository>();
            services.AddScoped<IPersonFacilityRepository, PersonFacilityRepository>();
            services.AddScoped<IPersonInsRepository, PersonInsuranceRepository>();
            services.AddScoped<IPersonAddressRepository, PersonAddressRepository>();
            services.AddScoped<IActionFilter, HGAuthententicateAttribute>();
        }
    }
}