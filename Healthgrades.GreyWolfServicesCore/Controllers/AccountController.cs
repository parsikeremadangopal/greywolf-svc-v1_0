﻿using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models;
using Healthgrades.GreyWolfServicesCore.Filters;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Healthgrades.GreyWolfServicesCore.Controllers
{
    /// <summary>
    /// Account endpoing provides access to account data.  An account is the highest level object for
    /// an authorized user.  Most information about an account is accessed through the list of persons
    /// that are on the account.  Each person has its collection of identity and profile information.
    ///
    /// The authentication system of choice provides the AuthId which links the auth system to the account
    /// data.
    /// </summary>
    [Route("Account")]
    [Produces("application/json")]
    public class AccountController : ControllerBase
    {
        private readonly IAccountRepository _AccountRepository;

        /// <summary>
        ///
        /// </summary>
        /// <param name="repository"></param>
        public AccountController(IAccountRepository repository)
        {
            _AccountRepository = repository;
        }

        /// <summary>
        /// Retrieves a full set of account data for the session token that is passed in via the Authorization header.  This data includes both
        /// identity information and profile information for each person the account has associated to it.
        /// </summary>
        [HttpGet]
        [TypeFilter(typeof(HGAuthententicateAttribute))]
        public ActionResult<Account> GetAccount()
        {
            if (!(HttpContext.Items["AuthenticationProps"] is AuthenticationProps props))
            {
                return Unauthorized(ErrorFactory.ErrorUnauthorized);
            }

            Account account = _AccountRepository.GetAccountByAuthId(props);

            //If the account is null or the accountId is empty (in the case of an already existing account from user service data store)
            //then the user has not created a wellness account for the purposes of greywolf services.
            if (account != null && account.AccountId != Guid.Empty)
            {
                return Ok(account);
            }
            else
            {
                return NotFound(ErrorFactory.ErrorAccountNotCreatedError);
            }
        }

        /// <summary>
        /// Adds/Updates the given account data to the account database.
        /// </summary>
        /// <param name="account">Information to be saved for the account.</param>
        /// <returns></returns>
        [HttpPost]
        [TypeFilter(typeof(HGAuthententicateAttribute))]
        public ActionResult<AccountIdResponse> SaveAccount([FromBody]AccountSaveRequest account)
        {
            AuthenticationProps props = HttpContext.Items["AuthenticationProps"] as AuthenticationProps;
            account.AuthId = props.AuthenticationId;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var response = new AccountIdResponse()
            {
                AccountId = _AccountRepository.SaveAccount(account)
            };

            return CreatedAtRoute("", new { id = response.AccountId }, response);
        }

        /// <summary>
        /// Adds/Updates the given device token to the cognito account profile database.
        /// </summary>
        /// <param name="accountDeviceRegisterRequest">Device information to be saved for the account.</param>
        /// <returns></returns>
        [Route("RegisterDeviceToken")]
        [HttpPost]
        [TypeFilter(typeof(HGAuthententicateAttribute))]
        public ActionResult<AccountIdResponse> RegisterDeviceToken([FromBody]AccountDeviceRegisterRequest accountDeviceRegisterRequest)
        {
            AuthenticationProps props = HttpContext.Items["AuthenticationProps"] as AuthenticationProps;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var response = new AccountIdResponse()
            {
                AccountId = _AccountRepository.RegisterDevice(accountDeviceRegisterRequest,
                                                              props.AuthenticationId)
            };

            return CreatedAtRoute("", new { id = response.AccountId }, response);
        }

        /// <summary>
        /// Sends push Notifications to devices registered for the account.
        /// </summary>
        /// <param name="devicePushNotificationRequest">Information to be send notification for the account.</param>
        /// <returns>The AccountId for the created or updated account</returns>
        [HttpPost]
        [Route("DevicePushNotification")]
        public ActionResult<Account> DevicePushNotification([FromBody]DevicePushNotificationRequest devicePushNotificationRequest)
        {
            if (!ModelState.IsValid || string.IsNullOrEmpty(devicePushNotificationRequest.AuthId))
            {
                return BadRequest(ModelState);
            }
            try
            {
                return Ok(_AccountRepository.DevicePushNotification(devicePushNotificationRequest));
            }
            catch (Exception e)
            {
                if (e.Message == "User not found")
                    return NotFound(ErrorFactory.ErrorAccountNotCreatedError);
                else
                    return BadRequest(ErrorFactory.ErrorUnhandledError);
            }
        }

        /// <summary>
        /// Adds/Updates the given account data to the account database.
        /// </summary>
        /// <param name="accountLogin">Information to be saved for the account.</param>
        /// <returns>The AccountId for the created or updated account</returns>
        [HttpPost]
        [Route("Login")]
        public ActionResult<Account> Login([FromBody]AccountLogin accountLogin)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                return Ok(_AccountRepository.Login(accountLogin));
            }
            catch (Exception e)
            {
                if (e.Message == "User is not authorized")
                    return Unauthorized(ErrorFactory.ErrorUnauthorized);
                else if (e.Message == "User not found")
                    return NotFound(ErrorFactory.ErrorAccountNotCreatedError);
                else
                    return BadRequest(ErrorFactory.ErrorUnhandledError);
            }
        }

        /// <summary>
        /// Register a new account
        /// </summary>
        /// <param name="accountRegister">Information to be saved for the account.</param>
        /// <returns>The AccountId for the created or updated account</returns>
        [HttpPost]
        [Route("Register")]
        public ActionResult<Account> Register([FromBody]AccountRegister accountRegister)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                return Ok(_AccountRepository.Register(accountRegister));
            }
            catch (Exception e)
            {
                if (e.Message == "Account already exists")
                    return Conflict(ErrorFactory.ErrorConflict);
                else
                    return BadRequest(ErrorFactory.ErrorUnhandledError);
            }
        }

        /// <summary>
        /// Adds/Updates the given account data to the account database.
        /// </summary>
        /// <param name="newAccount">Information to be saved for the account.</param>
        /// <returns>The AccountId for the created or updated account</returns>
        [Route("Onboard")]
        [HttpPost]
        public ActionResult<Account> OnboardAccount([FromBody]Account newAccount)
        {
            if (!ModelState.IsValid || newAccount == null || string.IsNullOrEmpty(newAccount.AuthId))
            {
                return BadRequest(ModelState);
            }

            Account resultAccount = _AccountRepository.SaveAndMergeAccount(newAccount);
            //If the account is null or the accountId is empty (in the case of an already existing account from user service data store)
            if (resultAccount != null && resultAccount.AccountId != Guid.Empty)
            {
                return Ok(resultAccount);
            }
            else
            {
                return NotFound(ErrorFactory.ErrorAccountNotCreatedError);
            }
        }

        /// <summary>
        /// Adds/Updates the given account data to the account database.
        /// </summary>
        /// <param name="newAccount">Information to be saved for the account.</param>
        /// <returns>The AccountId for the created or updated account</returns>
        [Route("update")]
        [HttpPost]
        [TypeFilter(typeof(HGAuthententicateAttribute))]
        public ActionResult<Account> SaveAndMergeAccount([FromBody]Account newAccount)
        {
            if (newAccount != null
                && newAccount.AuthId == null
                && HttpContext.Items["AuthenticationProps"] is AuthenticationProps props
                && props.AuthenticationId != null)
                newAccount.AuthId = props.AuthenticationId;

            if (!ModelState.IsValid || newAccount == null)
            {
                return BadRequest(ModelState);
            }

            Account resultAccount = _AccountRepository.SaveAndMergeAccount(newAccount);
            //If the account is null or the accountId is empty (in the case of an already existing account from user service data store)
            if (resultAccount != null && resultAccount.AccountId != Guid.Empty)
            {
                return Ok(resultAccount);
            }
            else
            {
                return NotFound(ErrorFactory.ErrorAccountNotCreatedError);
            }
        }

        /// <summary>
        /// Adds/Updates the given account data to the account database.
        /// </summary>
        /// <param name="newAccount">Information to be saved for the account.</param>
        /// <returns>The AccountId for the created or updated account</returns>
        [Route("create")]
        [HttpPost]
        [TypeFilter(typeof(HGAuthententicateAttribute))]
        public ActionResult<AccountIdResponse> SaveAndCreateAccount([FromBody]Account newAccount)
        {
            if (newAccount != null
                && newAccount.AuthId == null
                && HttpContext.Items["AuthenticationProps"] is AuthenticationProps props
                && props.AuthenticationId != null)
                newAccount.AuthId = props.AuthenticationId;

            if (!ModelState.IsValid || newAccount == null)
            {
                return BadRequest(ModelState);
            }

            Account resultAccount = _AccountRepository.SaveAndCreateAccount(newAccount);
            //If the account is null or the accountId is empty (in the case of an already existing account from user service data store)
            if (resultAccount != null && resultAccount.AccountId != Guid.Empty)
            {
                return Ok(resultAccount);
            }
            else
            {
                return NotFound(ErrorFactory.ErrorAccountNotCreatedError);
            }
        }

        /// <summary>
        /// Deletes the account with the given auth id, including all persons associated with the account
        /// </summary>
        /// <param name="authId">Authorization Id from the authorization service the account was created with</param>
        /// <returns></returns>
        [HttpDelete]
        [TypeFilter(typeof(HGAuthententicateAttribute))]
        public ActionResult<AccountIdResponse> Delete([FromBody]string authId)
        {
            if (!(HttpContext.Items["AuthenticationProps"] is AuthenticationProps props)
                || props.AuthenticationId != authId)
            {
                return Unauthorized(ErrorFactory.ErrorUnauthorized);
            }

            return Ok(new AccountIdResponse()
            {
                AccountId = _AccountRepository.DeleteAccount(authId)
            });
        }
    }
}