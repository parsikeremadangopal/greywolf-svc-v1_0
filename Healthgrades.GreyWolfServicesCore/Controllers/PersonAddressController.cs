﻿using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models;
using Healthgrades.GreyWolfServicesCore.Filters;
using Microsoft.AspNetCore.Mvc;

namespace Healthgrades.GreyWolfServicesCore.Controllers
{
    /// <summary>
    /// Create / Save a persons addresses
    /// </summary>
    [Route("PersonAddress")]
    [Produces("application/json")]
    public class PersonAddressController : ControllerBase
    {
        private readonly IPersonAddressRepository _PersonAddressRepository;

        /// <summary>
        /// PersonAddressController constructor
        /// </summary>
        /// <param name="rep"></param>
        public PersonAddressController(IPersonAddressRepository rep)
        {
            _PersonAddressRepository = rep;
        }

        /// <summary>
        /// Saves an address for the given person
        /// </summary>
        /// <param name="personAddress"></param>
        /// <returns></returns>
        [HttpPost]
        [TypeFilter(typeof(HGAuthententicateAttribute))]
        public ActionResult<PersonAddressIdResponse> SavePersonAddress([FromBody]PersonAddress personAddress)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!(HttpContext.Items["AuthenticationProps"] is AuthenticationProps props))
            {
                return Unauthorized(ErrorFactory.ErrorUnauthorized);
            }

            var i = _PersonAddressRepository.SavePersonAddress(personAddress, props.AuthenticationId);
            var idResponse = new PersonAddressIdResponse() { PersonId = personAddress.PersonId, PersonAddressId = i };

            return CreatedAtRoute("", new { id = personAddress.PersonAddressId }, idResponse);
        }
    }
}