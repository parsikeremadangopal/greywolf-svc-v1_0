﻿using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models;
using Healthgrades.GreyWolfServicesCore.Filters;
using Microsoft.AspNetCore.Mvc;

namespace Healthgrades.GreyWolfServicesCore.Controllers
{
    /// <summary>
    /// Person Controller
    /// </summary>
    [Route("Person")]
    [Produces("application/json")]
    public class PersonController : ControllerBase
    {
        private IPersonRepository _PersonRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        public PersonController(IPersonRepository personRepository)
        {
            _PersonRepository = personRepository;
        }

        /// <summary>
        /// Saves a person to the account.  If the person does not exist on the account (PersonId is empty)
        /// a new person will be created and added.
        /// </summary>
        /// <param name="person">The details of the person to be added/saved to an account.</param>
        /// <returns></returns>
        [HttpPost]
        [TypeFilter(typeof(HGAuthententicateAttribute))]
        public ActionResult<PersonIdResponse> SavePerson([FromBody]PersonSaveRequest person)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!(HttpContext.Items["AuthenticationProps"] is AuthenticationProps props))
            {
                return Unauthorized(ErrorFactory.ErrorUnauthorized);
            }

            var i = _PersonRepository.SavePerson(person, props.AuthenticationId);

            var personId = new PersonIdResponse() { PersonId = i };

            return CreatedAtRoute("", new { id = personId.PersonId }, personId);
        }
    }
}