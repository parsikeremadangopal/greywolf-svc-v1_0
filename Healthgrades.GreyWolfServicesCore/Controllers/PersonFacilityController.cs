﻿using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models;
using Healthgrades.GreyWolfServicesCore.Filters;
using Microsoft.AspNetCore.Mvc;

namespace Healthgrades.GreyWolfServicesCore.Controllers
{
    /// <summary>
    /// Create / Save a persons facilities
    /// </summary>
    [Route("PersonFacility")]
    [Produces("application/json")]
    public class PersonFacilityController : ControllerBase
    {
        private IPersonFacilityRepository _PersonFacilityRepository;

        /// <summary>
        /// PersonFacilityController constructor
        /// </summary>
        /// <param name="rep"></param>
        public PersonFacilityController(IPersonFacilityRepository rep)
        {
            _PersonFacilityRepository = rep;
        }

        /// <summary>
        /// Saves a facility for the given person
        /// </summary>
        /// <param name="personFacility"></param>
        /// <returns></returns>
        [HttpPost]
        [TypeFilter(typeof(HGAuthententicateAttribute))]
        public ActionResult<PersonFacilityIdResponse> SavePersonFacility([FromBody]PersonFacility personFacility)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!(HttpContext.Items["AuthenticationProps"] is AuthenticationProps props))
            {
                return Unauthorized(ErrorFactory.ErrorUnauthorized);
            }

            var i = _PersonFacilityRepository.SavePersonFacility(personFacility, props.AuthenticationId);
            var idResponse = new PersonFacilityIdResponse() { PersonId = personFacility.PersonId, PersonFacilityId = i };

            return CreatedAtRoute("", new { id = personFacility.PersonFacilityId }, idResponse);
        }
    }
}