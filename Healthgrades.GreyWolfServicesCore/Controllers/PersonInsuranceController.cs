﻿using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models;
using Healthgrades.GreyWolfServicesCore.Filters;
using Microsoft.AspNetCore.Mvc;

namespace Healthgrades.GreyWolfServicesCore.Controllers
{
    /// <summary>
    /// Saves / Creates an insurance related to a person
    /// </summary>
    [Route("PersonInsurance")]
    [Produces("application/json")]
    public class PersonInsuranceController : ControllerBase
    {
        private IPersonInsRepository _PersonInsRepository;

        /// <summary>
        /// PersonInsuranceController constructor
        /// </summary>
        /// <param name="rep"></param>
        public PersonInsuranceController(IPersonInsRepository rep)
        {
            _PersonInsRepository = rep;
        }

        /// <summary>
        /// Saves / Creates a person insurance relation
        /// </summary>
        /// <param name="personInsurance"></param>
        /// <returns></returns>
        [HttpPost]
        [TypeFilter(typeof(HGAuthententicateAttribute))]
        public ActionResult<PersonInsuranceIdResponse> SavePersonInsurance([FromBody]PersonInsurance personInsurance)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!(HttpContext.Items["AuthenticationProps"] is AuthenticationProps props))
            {
                return Unauthorized(ErrorFactory.ErrorUnauthorized);
            }

            var i = _PersonInsRepository.SavePersonIns(personInsurance, props.AuthenticationId);
            var idResponse = new PersonInsuranceIdResponse() { PersonId = personInsurance.PersonId, PersonInsuranceId = i };

            return CreatedAtRoute("", new { id = personInsurance.PersonInsuranceId }, idResponse);
        }
    }
}