﻿using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models;
using Healthgrades.GreyWolfServicesCore.Filters;
using Microsoft.AspNetCore.Mvc;

namespace Healthgrades.GreyWolfServicesCore.Controllers
{
    /// <summary>
    /// Saves / Creates a provider for a person (adds the provider to the persons care team)
    /// </summary>
    [Route("PersonProvider")]
    [Produces("application/json")]
    public class PersonProviderController : ControllerBase
    {
        private IPersonProviderRepository _PersonProviderRepository;

        /// <summary>
        /// PersonProviderController constructor
        /// </summary>
        /// <param name="rep"></param>
        public PersonProviderController(IPersonProviderRepository rep)
        {
            _PersonProviderRepository = rep;
        }

        /// <summary>
        /// Add / Update a member of the persons care team
        /// </summary>
        /// <param name="personProvider"></param>
        /// <returns></returns>
        [HttpPost]
        [TypeFilter(typeof(HGAuthententicateAttribute))]
        public ActionResult<PersonProviderIdResponse> SavePersonProvider([FromBody]PersonProvider personProvider)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!(HttpContext.Items["AuthenticationProps"] is AuthenticationProps props))
            {
                return Unauthorized(ErrorFactory.ErrorUnauthorized);
            }

            var authId = props.AuthenticationId;
            var i = _PersonProviderRepository.SavePersonProvider(personProvider, authId);
            var idResponse = new PersonProviderIdResponse() { PersonId = personProvider.PersonId, PersonProviderId = i };

            return CreatedAtRoute("", new { id = personProvider.PersonProviderId }, idResponse);
        }
    }
}