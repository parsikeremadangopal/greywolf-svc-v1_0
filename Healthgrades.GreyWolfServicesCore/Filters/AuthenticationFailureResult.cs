﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Healthgrades.GreyWolfServicesCore.Filters
{
    internal class AuthenticationFailureResult : IActionResult
    {
        private readonly HttpRequest request;
        private readonly string v;

        public AuthenticationFailureResult(string v, HttpRequest request)
        {
            this.v = v;
            this.request = request;
        }

        public Task ExecuteResultAsync(ActionContext context)
        {
            Task task = Task.Run(() =>
            {
                return Task.CompletedTask;
            });

            return task;
        }
    }
}