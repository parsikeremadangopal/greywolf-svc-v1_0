﻿using Healthgrades.GreyWolfLibrary.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;

namespace Healthgrades.GreyWolfServicesCore.Filters
{
    /// <summary>
    /// HGAuthententicateAttribute
    /// </summary>
    public class HGAuthententicateAttribute : IActionFilter
    {
        /// <summary>
        /// OnActionExecuting
        /// </summary>
        /// <param name="context"></param>
        public void OnActionExecuting(ActionExecutingContext context)
        {
            var authorizationHeader = context.HttpContext?.Request?.Headers["Authorization"];
            if (!authorizationHeader.HasValue)
            {
                context.Result = new UnauthorizedObjectResult("Authorization is null");
                return;
            }

            if (!context.ModelState.IsValid)
            {
                context.Result = new BadRequestObjectResult(context.ModelState);
                return;
            }

            var authValues = authorizationHeader.Value.FirstOrDefault()?.Split(" ");

            //Make sure the schema is correct
            if (authValues == null || authValues.Count() != 2 || authValues[0] != "HGAuth")
            {
                context.Result = new UnauthorizedObjectResult("Authorization is not in the right format");
                return;
            }

            if (string.IsNullOrEmpty(authValues[1]))
            {
                // Authentication was attempted but failed. Set ErrorResult to indicate an error.
                context.Result = new AuthenticationFailureResult("Missing session token", context.HttpContext.Request);
                return;
            }

            var service = (IAuthenticationValidation)context.HttpContext.RequestServices.GetService(typeof(IAuthenticationValidation));
            var auth = service.ValidateAuthentication(authValues[1]);

            if (auth.Item1)
            {
                context.HttpContext.Items.Add("AuthenticationProps", auth.Item2);
            }
            else
            {
                context.Result = new AuthenticationFailureResult("Invalid authorization token.", context.HttpContext.Request);
            }
        }

        /// <summary>
        /// OnActionExecuted
        /// </summary>
        /// <param name="context"></param>
        public void OnActionExecuted(ActionExecutedContext context)
        {
        }
    }
}