﻿using Healthgrades.GreyWolfLibrary.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;

namespace Healthgrades.GreyWolfServicesCore.Filters
{
    /// <summary>
    /// ApiError Class
    /// </summary>
    public class ApiError
    {
        /// <summary>
        /// message
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// isError
        /// </summary>
        public bool isError { get; set; }

        /// <summary>
        /// detail
        /// </summary>
        public string detail { get; set; }

        /// <summary>
        /// ApiError Constructor
        /// </summary>
        /// <param name="message"></param>
        public ApiError(string message)
        {
            this.message = message;
            isError = true;
        }
    }

    /// <summary>
    /// HGExceptionAttribute
    /// </summary>
    public class HGExceptionAttribute : ExceptionFilterAttribute
    {
        /// <summary>
        /// OnException
        /// </summary>
        /// <param name="actionExecutedContext"></param>
        public override void OnException(ExceptionContext actionExecutedContext)
        {
            ApiError apiError;
            if (actionExecutedContext.Exception.Message.Contains("User is not authorized"))
            {
                ErrorResponse response = ErrorFactory.ErrorForbidden;
                apiError = new ApiError(actionExecutedContext.Exception.Message);
                actionExecutedContext.HttpContext.Response.StatusCode = 403;
            }
            else
            {
                ErrorResponse response = ErrorFactory.ErrorUnhandledErrorWithDetails(actionExecutedContext.Exception.ToString());
                var param = JsonConvert.SerializeObject(response);
                HttpContent content = new StringContent(param, Encoding.UTF8, "application/json");
                apiError = new ApiError(Convert.ToString(content));
                actionExecutedContext.HttpContext.Response.StatusCode = 500;
            }
            actionExecutedContext.Result = new JsonResult(apiError);
            base.OnException(actionExecutedContext);
        }
    }
}