﻿using Healthgrades.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using System.Collections.Generic;

namespace Healthgrades.GreyWolfServicesCore
{
    /// <summary>
    /// Program class
    /// </summary>
    public class Program
    {
        // ***** Logging
        private const string localOutputFormat = "----------------------------------------------{NewLine}[{Timestamp:u} {Level:u3}] {Message:lj} {NewLine}{Properties:j}{NewLine}{Exception}----------------------------------------------{NewLine}";

        // The default level is Verbose (log everything)
        // Switches override the defaultMinimumLevel for a namespace
        // and all e sub-namespaces below them (e.g. any Healthgrades.SomeSubNamespace will be LogEventLevel.Information)
        // Output is determined by the HGSerilog:mode settings in appsettings
        private static GlobalLoggingLevelSwitches logControlSwitches = new GlobalLoggingLevelSwitches(
            defaultMinimumLevel: LogEventLevel.Verbose,
            switches: new[]
            {
                new KeyValuePair<string, LogEventLevel> ( "Healthgrades", LogEventLevel.Verbose),
                new KeyValuePair<string, LogEventLevel> ( "System", LogEventLevel.Warning),
                new KeyValuePair<string, LogEventLevel> ( "Microsoft", LogEventLevel.Warning),

                new KeyValuePair<string, LogEventLevel> ( "System.Net.Http.HttpClient", LogEventLevel.Warning)
            }
        );

        private static void InitSerilog(HostBuilderContext hostContext, LoggerConfiguration loggerConfiguration)
        {
            var logConfigSection = hostContext.Configuration.GetSection("HGSerilog");

            loggerConfiguration
              .Enrich.FromLogContext()
              .Enrich.With<SystemPropertiesLogEnricher>()
              .Enrich.With<AwsContextEnricher>()
              .Enrich.WithProperty("application_name", logConfigSection.GetValue<string>("applicationName"));

            if (logConfigSection.GetValue<string>("mode") == "console")
            {
                loggerConfiguration = loggerConfiguration.WriteTo.Console(outputTemplate: localOutputFormat);
            }

            if (logConfigSection.GetValue<string>("mode") == "file")
            {
                loggerConfiguration = loggerConfiguration.WriteTo.File(formatter: new Serilog.Formatting.Compact.CompactJsonFormatter(),
                    path: logConfigSection.GetSection("file").GetValue<string>("path"),
                    buffered: false,
                    shared: true,
                    flushToDiskInterval: null,
                    fileSizeLimitBytes: null
                );
            }

            // the rootLogger and config should always be set to verbose, so that they don't filter anything
            loggerConfiguration = loggerConfiguration.MinimumLevel.ControlledBy(logControlSwitches.DefaultMinimumLevel);

            foreach (var logSwitch in logControlSwitches)
            {
                loggerConfiguration = loggerConfiguration.MinimumLevel.Override(logSwitch.Key, logSwitch.Value);
            }
        }

        // *****
        /// <summary>
        /// Program Main
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            try
            {
                CreateHostBuilder(args).Build().Run();
            }
            finally
            {
                Serilog.Log.CloseAndFlush();
            }
        }

        /// <summary>
        /// CreateWebHostBuilder
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices(services => services.AddSingleton(logControlSwitches))
                .UseSerilog(InitSerilog)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}