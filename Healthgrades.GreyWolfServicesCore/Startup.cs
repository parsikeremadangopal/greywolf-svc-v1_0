﻿using Healthgrades.GreyWolfLibrary.Helpers;
using Healthgrades.GreyWolfLibrary.Interfaces;
using Healthgrades.GreyWolfLibrary.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using Polly;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;

namespace Healthgrades.GreyWolfServicesCore
{
    /// <summary>
    /// Startup Class
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Startup Constructor
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Configuration
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services
                .AddMvc(options =>
                            {
                                options.RespectBrowserAcceptHeader = true; // false by default
                                options.OutputFormatters.RemoveType<TextOutputFormatter>();
                                options.OutputFormatters.RemoveType<HttpNoContentOutputFormatter>();
                            })
                .AddNewtonsoftJson(o => o.SerializerSettings.ContractResolver = new DefaultContractResolver());

            services.AddRazorPages();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Greywolf API",
                    Version = "v1",
                    Contact = new OpenApiContact
                    {
                        Name = "Healthgrades - Tesla Squad",
                        Url = new Uri("https://git.aws.healthgrades.zone/hgweb/services/greywolf-svc-v1_0"),
                        Email = "squad_tesla@healthgrades.com"
                    }
                });
                var xmlPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "GreyWolfServicesCore.xml");
                c.IncludeXmlComments(xmlPath);
            });

            AutofacConfig.RegisterComponents(services, Configuration);
            ConfigureHttpClientFactoryPolicies(services);

            services.AddOptions();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var appSettings = Configuration.GetSection("AppSettings").Get<AppSettings>();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseRouting();

            app.UseEndpoints(c =>
            {
                c.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(appSettings.ServerRootAddress + "/swagger/v1/swagger.json", "GreyWolfService v1.0");
                c.RoutePrefix = string.Empty;
            });

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void ConfigureHttpClientFactoryPolicies(IServiceCollection services)
        {
            var defaultServiceSettings = Configuration.GetSection("DefaultServiceSettings").Get<HttpClientPolicySettings>();

            services.AddTransient<RequestTraceHandler>();

            services.AddHttpClient(HttpClientDefinitions.DefaultServiceHttpClient)
              .AddHttpMessageHandler<RequestTraceHandler>()
              .ConfigureHttpClient((sp, options) => EnrichHttpClientWithCustomHeaders(sp, options))
              .ConfigureHttpClient((sp, options) => options.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json")))
              .SetHandlerLifetime(TimeSpan.FromMinutes(3))
              .AddPolicyHandler(Policy.TimeoutAsync<HttpResponseMessage>(defaultServiceSettings.TimeoutMilliseconds));
        }

        /// <summary>
        /// add custom objects to request
        /// </summary>
        /// <param name="sp">service provider</param>
        /// <param name="options">httpclient</param>
        public void EnrichHttpClientWithCustomHeaders(IServiceProvider sp, HttpClient options)
        {
            var traceContext = sp.GetService<ISystemWebHelper>();
            var amazonTraceId = traceContext.GetHttpHeaders("X-Amzn-Trace-Id");
            if (!string.IsNullOrEmpty(amazonTraceId))
            {
                options.DefaultRequestHeaders.TryAddWithoutValidation("X-Amzn-Trace-Id", amazonTraceId);
            }
        }
    }
}