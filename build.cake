#tool "nuget:?package=GitVersion.CommandLine&version=5.0.1"
#addin nuget:?package=Cake.Json&version=4.0.0
#addin nuget:?package=Newtonsoft.Json&version=11.0.2
#addin "Cake.FileHelpers&version=3.2.1"
#tool "nuget:?package=OctopusTools"

//////////////////////////////////////////////////////////////////////
// ARGUMENTS
//////////////////////////////////////////////////////////////////////

var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");

var octopusServerUrl = Argument("OctoupsServerUrl", "https://octopus.aws.healthgrades.zone");
var octopusApiKey = Argument("OctopusApiKey", (string)null) ?? EnvironmentVariable("OctopusMasterFeedApiKey") ?? (string)null;
var solutionFile = Argument("SolutionFile", "Healthgrades.GreyWolfServices.sln");

var msbuildIgnoreWarnings = new List<String>()
{
    // XML comment warnings
    "CS1591",
    "CS1572",
    "CS1573",
    "CS1570",
    "CS1574",
    "CS1587",
    "CS1734"
};

if(Jenkins.IsRunningOnJenkins){
    msbuildIgnoreWarnings.AddRange(new []
    {
        // obsolete messages
        "CS0618"
    });
}

//////////////////////////////////////////////////////////////////////
// PREPARATION
//////////////////////////////////////////////////////////////////////

GitVersion gitVersionInfo = null;

// Define directories.
var buildToolsDirectory = Directory("build-tools");
var testResultsDirectory = buildToolsDirectory + Directory("test-results");
var buildArtifactsDirectory = buildToolsDirectory + Directory("build-artifacts");
var buildMetaDataDirectory = buildToolsDirectory + Directory("build-metadata");
var buildLogDirectory = buildToolsDirectory + Directory("build-log");
var octoupsPublishDirectory = buildArtifactsDirectory + Directory("octopus");
var packageStagingDirectory = buildToolsDirectory + Directory("package-staging");


//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////

Task("Clean")
    .Does(() =>
{
    CleanDirectory(buildToolsDirectory);
    CleanDirectory(buildArtifactsDirectory);
    CleanDirectory(buildMetaDataDirectory);
    CleanDirectory(testResultsDirectory);
    CleanDirectory(buildLogDirectory);
});

Task("Calculate-SemVer")
    .IsDependentOn("Clean")
    .Does(()=>
{
    gitVersionInfo = GitVersion(new GitVersionSettings{
        UpdateAssemblyInfo=false,
        OutputType=GitVersionOutput.Json
    });
    var gvTxt = SerializeJsonPretty(gitVersionInfo);
    var gvTxtFileName = buildMetaDataDirectory + File("gitversion.json");
    FileWriteText(gvTxtFileName, gvTxt);
    Information(FileReadText(gvTxtFileName));

    var isJenkinsBuild = Jenkins.IsRunningOnJenkins;
    if(isJenkinsBuild) {
        Information("In Jenkins");
        var jobName = $"\"{Jenkins.Environment.Job.JobName}\"";
        var buildNumber = Jenkins.Environment.Build.BuildNumber;
        var displayname = gitVersionInfo.FullSemVer;
        var jenkinsUrl = Jenkins.Environment.JenkinsUrl;
        var javaCliFile = buildToolsDirectory + File("jenkins-cli.jar");

        if(!jenkinsUrl.StartsWith("http")){
            jenkinsUrl = "https://" + jenkinsUrl;
        }

        DownloadFile($"{jenkinsUrl}/jnlpJars/jenkins-cli.jar", javaCliFile);
        
        var settings = new ProcessSettings()
            .WithArguments(args=>{
                args.Append("-jar")
                    .Append(javaCliFile.Path.FullPath)
                    .Append("-s")
                    .Append(jenkinsUrl)
                    .Append("-auth")
                    .Append($"jenkinscli:{EnvironmentVariable("JENKINS_CLI_APIKEY")}")
                    .Append("set-build-display-name")
                    .Append(jobName)
                    .Append(buildNumber.ToString())
                    .Append(displayname);
            });

        var exitCode = StartProcess("java", settings);
        if(exitCode != 0 ){
            throw new Exception("Unable to set build name");
        }
    }
});

Task("Create-BuildTxt")
    .IsDependentOn("Calculate-SemVer")
    .Does(()=>
{
    var buildTxt = new [] {
        $@"build-number: {gitVersionInfo.InformationalVersion}",
        $@"commit-date: {gitVersionInfo.CommitDate}",
        $@"build-date: {DateTimeOffset.UtcNow:r}",
    };
    var buildTxtFileName = buildMetaDataDirectory + File("build.txt");
    FileWriteLines(buildTxtFileName, buildTxt);
    Information(FileReadText(buildTxtFileName));
});

Task("Create-BuildMetadata")
    .IsDependentOn("Clean")
    .IsDependentOn("Calculate-SemVer")
    .IsDependentOn("Create-BuildTxt");

Task("Set-Version")
    .IsDependentOn("Create-BuildMetadata")
    .Does(()=>
{
    var projects = new List<FilePath>();

    var sln = ParseSolution(solutionFile);
    foreach(var project in sln.Projects.Where(p=>p.Type != "{2150E333-8FDC-42A3-9474-1A3956D46DE8}")){       
       XmlPoke(project.Path, "/Project/PropertyGroup[1]/Version", gitVersionInfo.InformationalVersion);
    }
});


Task("Restore-Packages")
    .IsDependentOn("Set-Version")
    .Does(() =>
{
    var settings = new DotNetCoreRestoreSettings{
        MSBuildSettings = new DotNetCoreMSBuildSettings()
            .WithProperty("NoWarn", new [] {String.Join(",",msbuildIgnoreWarnings)})
            .AddFileLogger(
                new MSBuildFileLoggerSettings{
                    AppendToLogFile = true,
                    LogFile = buildLogDirectory + File("dotnet-restore.log.txt")
                })
    };
    DotNetCoreRestore(settings);
});

Task("Compile-Sources")
    .IsDependentOn("Restore-Packages")
    .Does(() =>
{
    var settings = new DotNetCoreBuildSettings{
        Configuration = configuration,
        NoRestore = true,
        MSBuildSettings = new DotNetCoreMSBuildSettings()
            .WithProperty("NoWarn", new [] {String.Join(",",msbuildIgnoreWarnings)})
            .SetVersion(gitVersionInfo.InformationalVersion)
            .AddFileLogger(
                new MSBuildFileLoggerSettings{
                    AppendToLogFile = true,
                    LogFile = buildLogDirectory + File("dotnet-build.log.txt")
                })
    };
    DotNetCoreBuild("./", settings );
});

Task("Publish-App")
    .IsDependentOn("Compile-Sources")
    .Does(()=>
{
    var settings = new DotNetCorePublishSettings
    {
        Configuration = configuration,
        NoRestore = true,
        NoBuild = true,        
        MSBuildSettings = new DotNetCoreMSBuildSettings()
        .WithProperty("NoWarn", new [] {String.Join(",",msbuildIgnoreWarnings)})
        .SetVersion(gitVersionInfo.InformationalVersion)
        .AddFileLogger(
            new MSBuildFileLoggerSettings{
                AppendToLogFile = true,
                LogFile = buildLogDirectory + File("dotnet-publish.log.txt")
            })
    };
	DotNetCorePublish("./", settings);    
});

Task("Run-Unit-Tests")
    .IsDependentOn("Compile-Sources")
    .Does(() =>
{
    var settings = new DotNetCoreTestSettings
    {
        Configuration = configuration,
        NoBuild = true,
        NoRestore = true,
        Logger = "trx",
        ResultsDirectory = testResultsDirectory
    };   
   
    DotNetCoreTest("./", settings);
   
});

Task("OctoPack")
    .IsDependentOn("Compile-Sources")
    .IsDependentOn("Publish-App")
    .Does(()=>
{
    var packageDirs = GetSubDirectories(packageStagingDirectory);
    foreach(var packageDir in packageDirs){        
        var pckgBaseName = packageDir.GetDirectoryName();        
        foreach(var osDir in GetSubDirectories(packageDir)){            
            var osName = osDir.GetDirectoryName();
            if(osName == "teamserver")
                continue;
            var pkgId = String.Join(".", new [] {pckgBaseName, osName});       
        
            OctoPack(pkgId, new OctopusPackSettings {
                BasePath = osDir,
                OutFolder = octoupsPublishDirectory,
                Version = gitVersionInfo.FullSemVer
            });
        }
    }
});

Task("Push-Octopus")
    .Does(()=>
{
    var settings = new OctopusPushSettings {
        ReplaceExisting = false
    };

    foreach(var file in GetFiles(octoupsPublishDirectory.ToString() + "/**/*.nupkg")){
        OctoPush(octopusServerUrl, octopusApiKey, file.FullPath, settings);
    }
});

Task("Build")
    .IsDependentOn("OctoPack")
    .IsDependentOn("Run-Unit-Tests");

//////////////////////////////////////////////////////////////////////
// TASK TARGETS
//////////////////////////////////////////////////////////////////////

Task("Default")
    .IsDependentOn("Build")
    .Finally(() =>
{  
    var sln = ParseSolution(solutionFile);
    foreach(var project in sln.Projects.Where(p=>p.Type != "{2150E333-8FDC-42A3-9474-1A3956D46DE8}")){
       XmlPoke(project.Path, "/Project/PropertyGroup[1]/Version", "");
    }
});

Task("Jenkins")
    .IsDependentOn("Default")
    .IsDependentOn("Push-Octopus");

//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////

RunTarget(target);
