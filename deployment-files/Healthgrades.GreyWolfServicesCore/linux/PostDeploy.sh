#!/bin/bash
set -e -o pipefail

umask 027

svcuser=`get_octopusvariable "service_name"`
basedir=`get_octopusvariable "PackageInstallDirectory"`
port=`get_octopusvariable "NODE_ENV_PORT"`
svcname="${svcuser}-${port}"
vers=`get_octopusvariable "Octopus.Release.Number"`

sudo useradd -U -r -s /usr/sbin/nologin $svcuser 2>&1 || true
sudo chmod 640 "$basedir" -R
sudo find "$basedir" -type d -exec chmod 2755 '{}' \;
sudo chown $USER:$svcuser "$basedir/app-publish" -R

sudo mkdir -p "/var/data/applogs/$svcuser"
sudo chmod o+rx /var/data/applogs
sudo chown "root:$svcuser" "/var/data/applogs/$svcuser"
sudo chmod 2770  "/var/data/applogs/$svcuser"
sudo mkdir -p /etc/applogs/logrotate.d
sudo cp etc/applogs/logrotate.d/* /etc/applogs/logrotate.d/

sudo mkdir -p /etc/applogs/filebeat.d
sudo cp etc/applogs/filebeat.d/* /etc/applogs/filebeat.d/

CookieSignCert=`get_octopusvariable "CookieSignCert.RawOriginal"`
echo "$CookieSignCert" | base64 --decode | sudo tee "$basedir/app-publish/cookie-sign-cert.pfx" >/dev/null

sudo cp "etc/systemd/system/${svcuser}.service" "/etc/systemd/system/${svcname}.service"
sudo systemctl daemon-reload 2>&1
sudo systemctl enable "${svcname}.service" 2>&1
sudo systemctl start ${svcname}.service

if [ -f /etc/nginx/sites-enabled/default ]; then
  sudo rm /etc/nginx/sites-enabled/default
fi
if [ -f /etc/nginx/sites-enabled/$svcuser ]; then
  sudo rm /etc/nginx/sites-enabled/$svcuser
fi

sudo cp "etc/nginx/sites-available/$svcuser" "/etc/nginx/sites-available/$svcname"
sudo ln -s "/etc/nginx/sites-available/$svcname" /etc/nginx/sites-enabled/$svcuser
sudo nginx -s reload

services=`sudo systemctl list-unit-files --no-pager "$svcuser-*" | awk ' { print $1 }' | grep $svcuser`
for i in $services
do
    if [ "$i" != "${svcname}.service" ]; then
        sudo systemctl stop $i 2>&1
        sudo systemctl disable $i 2>&1
        sudo rm -rf "/etc/systemd/system/$i" 2>&1
        sudo systemctl daemon-reload 2>&1
    fi
done
