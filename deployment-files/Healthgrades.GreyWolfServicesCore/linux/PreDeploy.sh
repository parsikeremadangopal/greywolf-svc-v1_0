#!/bin/bash

inuse="inuse"

while [ ! -z "$inuse" ];
do
  port=$((5000 + $RANDOM % 5000))
  items=$(netstat -unat | awk '{print $4}' | grep [0-9]*$ -o)
  inuse=$(echo $items | grep $port -o )
done
set_octopusvariable "NODE_ENV_PORT" $port