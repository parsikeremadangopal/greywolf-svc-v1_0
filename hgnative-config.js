exports.handler = (event, context, callback) => {

    var config = {'errorCode':'1001', 'errorDescription':'No Configuration'};
    var statusCode = 404;

    var parameters = event.pathParameters;
    if (parameters === undefined) {
        parameters = event;
    }

    if (parameters.application.toUpperCase() === 'greywolf'.toUpperCase()) {
        var greywolfConfig = getGreywolfConfig(parameters, config, statusCode);
        config = greywolfConfig.config;
        statusCode = greywolfConfig.statusCode;
    }
    else if (parameters.application.toUpperCase() === 'hgmobile'.toUpperCase()) {
        var hgmobileConfig = getHGMobileConfig(parameters, config, statusCode);
        config = hgmobileConfig.config;
        statusCode = hgmobileConfig.statusCode;
    }

    Object.assign(config, {'configFor':parameters});

    var returnValue = {
        'statusCode': statusCode,
        'headers': { 'Content-Type': 'application/json' },
        'body': JSON.stringify(config)
    };

    if (event.pathParameters === undefined) {
        callback(null, config);
    }
    else {
        callback(null, returnValue);
    }
};

function getHGMobileConfig(parameters, configDefault, statusCodeDefault) {

    if (parameters.platform.toUpperCase() === 'iOS'.toUpperCase()) {

        if (parameters.application_version.toUpperCase() === '2.0'.toUpperCase()) {

            var config = {};

            addSegmentWriteKeySetting(parameters.environment, parameters.platform, config);
            addHgAuthUrlBaseSetting(parameters.environment, parameters.platform, config);
            addImageUrlBaseSetting(config);
            addTermsAndConditionsSettings(parameters.environment, config);
            addOASLandingUrlBaseSetting(parameters.environment, parameters.platform, config);
            addGreyWolfServicesUrlBaseSettings('v1_0', parameters.environment, config);
            addAutoSuggestUrlBaseSettings('v3_1', parameters.environment, config);
            addProviderSearchUrlBaseSettings('v3_4', parameters.environment, config);
            addProviderDetailsUrlBaseSettings('v4_1', parameters.environment, config);
            addUserServiceUrlBaseSettings('v3_0', parameters.environment, config);
            addHGMobileServiceUrlBaseSettings('v2_0', parameters.environment, config);
            addMatrixServiceUrlBaseSettings('v3_3', parameters.environment, config);
            addOARServiceUrlBaseSettings('v3_0', parameters.environment, config);
            addContentServiceUrlBaseSettings('v3_0', parameters.environment, config);
            addFacilityServiceUrlBaseSettings('v2_2', parameters.environment, config);
            addPESServiceUrlBaseSettings('v4_1', parameters.environment, config);

            //Add feature settings
            Object.assign(config, {'feature_oar_enabled':true});
            Object.assign(config, {'feature_uber_enabled':true});
            Object.assign(config, {'uber_client_id':''});

            return { config:config, statusCode:200 };
        }
    }

    return { config:configDefault, statusCode:statusCodeDefault };
}

function getGreywolfConfig(parameters, configDefault, statusCodeDefault) {

    if (parameters.application_version.toUpperCase() === '1.0'.toUpperCase()
         && (parameters.platform.toUpperCase() === 'iOS'.toUpperCase() || parameters.platform.toUpperCase() === 'Android'.toUpperCase())
       )
    {
        var config = {};

        addSegmentWriteKeySetting(parameters.environment, parameters.platform, config);
        addHgAuthUrlBaseSetting(parameters.environment, parameters.platform, config);
        addImageUrlBaseSetting(config);
        addTermsAndConditionsSettings(parameters.environment, config);
        addOASLandingUrlBaseSetting(parameters.environment, parameters.platform, config);
        addGreyWolfServicesUrlBaseSettings('v1_0', parameters.environment, config);
        addAutoSuggestUrlBaseSettings('v3_1', parameters.environment, config);
        addProviderSearchUrlBaseSettings('v3_4', parameters.environment, config);
        addProviderDetailsUrlBaseSettings('v4_0', parameters.environment, config);

        return { config:config, statusCode:200 };
    }
    else {
        return { config:configDefault, statusCode:statusCodeDefault };
    }
}

function addSegmentWriteKeySetting(environment, platform, config) {

    var iosDevWriteKey = 'GTmJvSW0p1vRD6zceO6OOMi7Fr1tAlcR';
    var iosProdWriteKey = '';

    var androidDevWriteKey = 'xsgwPrMlvArj5cfcqMM0vLVF0HpZmVaG';
    var androidProdWriteKey = '';

    var devWriteKey = '';
    var prodWriteKey = '';

    switch (platform.toUpperCase()) {
        case 'ios'.toUpperCase():
            devWriteKey = iosDevWriteKey;
            prodWriteKey = iosProdWriteKey;
            break;
        case 'android'.toUpperCase():
            devWriteKey = androidDevWriteKey;
            prodWriteKey = androidProdWriteKey;
            break;
    }

    switch (environment.toUpperCase()) {
        case 'prod'.toUpperCase(): Object.assign(config, {'segment_write_key':prodWriteKey}); break;
        case 'test'.toUpperCase(): Object.assign(config, {'segment_write_key':devWriteKey}); break;
        case 'dev'.toUpperCase() : Object.assign(config, {'segment_write_key':devWriteKey}); break;
    }
}

function addImageUrlBaseSetting(config) {

    Object.assign(config, {'image_url_base':'https://img.healthgrades.com'});
}

function addOASLandingUrlBaseSetting(environment, platform, config) {

    switch (environment.toUpperCase()) {
        case 'prod'.toUpperCase(): Object.assign(config, {'oas_landing_url_base':'https://www.healthgrades.com/partner/landing'}); break;
        case 'test'.toUpperCase(): Object.assign(config, {'oas_landing_url_base':'https://www.healthgrades.com/partner/landing'}); break;
        case 'dev'.toUpperCase() : Object.assign(config, {'oas_landing_url_base':'https://www.healthgrades.com/partner/landing'}); break;
    }
}

function addAutoSuggestUrlBaseSettings(serviceVersion, environment, config) {
    var apiUrlBase = getApiUrlBase(environment);
    Object.assign(config, {'auto_suggest_url_base':apiUrlBase + '/AutoSuggestService/' + serviceVersion + '/Api/Search'});
}

function addProviderSearchUrlBaseSettings(serviceVersion, environment, config) {
    var apiUrlBase = getApiUrlBase(environment);
    Object.assign(config, {'provider_search_url_base':apiUrlBase + '/ProviderSearch/' + serviceVersion + '/Search'});
    Object.assign(config, {'provider_facet_url_base':apiUrlBase + '/ProviderSearch/' + serviceVersion + '/Facet'});
}

function addProviderDetailsUrlBaseSettings(serviceVersion, environment, config) {
    var apiUrlBase = getApiUrlBase(environment);
    Object.assign(config, {'provider_details_url_base':apiUrlBase + '/Provider/' + serviceVersion + '/Details'});
    Object.assign(config, {'provider_inactive_url_base':apiUrlBase + '/Provider/' + serviceVersion + '/Inactive'});
}

function addUserServiceUrlBaseSettings(serviceVersion, environment, config) {
    var apiUrlBase = getApiUrlBase(environment);

    Object.assign(config, {'userservice_authorize_url_base':apiUrlBase + '/UserService/' + serviceVersion + '/api/Authorize'});
    Object.assign(config, {'userservice_configuration_url_base':apiUrlBase + '/UserService/' + serviceVersion + '/api/Configuration'});
    Object.assign(config, {'userservice_inbox_alert_url_base':apiUrlBase + '/UserService/' + serviceVersion + '/api/InboxAlert'});
    Object.assign(config, {'userservice_login_url_base':apiUrlBase + '/UserService/' + serviceVersion + '/api/Login'});
    Object.assign(config, {'userservice_credential_url_base':apiUrlBase + '/UserService/' + serviceVersion + '/api/Credential'});
    Object.assign(config, {'userservice_security_question_url_base':apiUrlBase + '/UserService/' + serviceVersion + '/api/SecurityQuestion'});
    Object.assign(config, {'userservice_profile_url_base':apiUrlBase + '/UserService/' + serviceVersion + '/api/Profile'});
    Object.assign(config, {'userservice_group_url_base':apiUrlBase + '/UserService/' + serviceVersion + '/api/Group'});
    Object.assign(config, {'userservice_refresh_access_url_base':apiUrlBase + '/UserService/' + serviceVersion + '/api/RefreshAccess'});
    Object.assign(config, {'userservice_user_url_base':apiUrlBase + '/UserService/' + serviceVersion + '/api/User'});
    Object.assign(config, {'userservice_action_url_base':apiUrlBase + '/UserService/' + serviceVersion + '/api/Action'});
    Object.assign(config, {'userservice_statistics_rss_url_base':apiUrlBase + '/UserService/' + serviceVersion + '/api/Statistics/RSS'});
    Object.assign(config, {'userservice_password_reset_url_base':apiUrlBase + '/UserService/' + serviceVersion + '/api/PasswordReset'});
}

function addHGMobileServiceUrlBaseSettings(serviceVersion, environment, config) {
    var apiUrlBase = getApiUrlBase(environment);

    Object.assign(config, {'hgmobileservice_background_updates_url_base':apiUrlBase + '/HGMobileService/' + serviceVersion + '/api/BackgroundUpdates'});
}

function addMatrixServiceUrlBaseSettings(serviceVersion, environment, config) {
    var apiUrlBase = getApiUrlBase(environment);

    Object.assign(config, {'matrix_layout_find_url_base':apiUrlBase + '/Matrix/' + serviceVersion + '/Layout/Find'});
}

function addOARServiceUrlBaseSettings(serviceVersion, environment, config) {
    var apiUrlBase = getApiUrlBase(environment);

    Object.assign(config, {'oar_appointment_designated_provider_appointment_form_url_base':apiUrlBase + '/oar/' + serviceVersion + '/api/appointment/designatedproviderappointmentform'});
    Object.assign(config, {'oar_appointment_submit_oar_url_base':apiUrlBase + '/oar/' + serviceVersion + '/api/appointment/SubmitOar'});
}

function addContentServiceUrlBaseSettings(serviceVersion, environment, config) {
    var apiUrlBase = getApiUrlBase(environment);

    Object.assign(config, {'contentservice_ratings_url_base':apiUrlBase + '/ContentService/' + serviceVersion + '/api/Ratings'});
    Object.assign(config, {'contentservice_practicing_specialties_url_base':apiUrlBase + '/ContentService/' + serviceVersion + '/api/PracticingSpecialties'});
}

function addFacilityServiceUrlBaseSettings(serviceVersion, environment, config) {
    var apiUrlBase = getApiUrlBase(environment);

    Object.assign(config, {'facility_search_url_base':apiUrlBase + '/Facility/' + serviceVersion + '/Search'});
    Object.assign(config, {'facility_get_url_base':apiUrlBase + '/Facility/' + serviceVersion + '/Get'});
}

function addPESServiceUrlBaseSettings(serviceVersion, environment, config) {
    var apiUrlBase = getApiUrlBase(environment);

    Object.assign(config, {'pes_surveys_submit_url_base':apiUrlBase + '/pes/' + serviceVersion + '/Surveys/Submit'});
    Object.assign(config, {'pes_fetch_surveys_template_url_base':apiUrlBase + '/pes/' + serviceVersion + '/FetchSurveys/Template'});
    Object.assign(config, {'pes_flag_submit_url_base':apiUrlBase + '/pes/' + serviceVersion + '/Flag'});
    Object.assign(config, {'pes_helpful_submit_url_base':apiUrlBase + '/pes/' + serviceVersion + '/Helpful'});
    Object.assign(config, {'pes_surveys_provider_results_url_base':apiUrlBase + '/pes/' + serviceVersion + '/Surveys/ProviderResults'});
}

function addHgAuthUrlBaseSetting(environment, platform, config) {

    switch (environment.toUpperCase()) {
        case 'prod'.toUpperCase(): Object.assign(config, {'hgauth_url_base':'https://apigw.healthgrades.com/account'}); break;
        case 'test'.toUpperCase(): Object.assign(config, {'hgauth_url_base':'https://apigwdev.healthgrades.com/account-test'}); break;
        case 'dev'.toUpperCase() : Object.assign(config, {'hgauth_url_base':'https://apigwdev.healthgrades.com/account-dev'}); break;
    }

    var hgAuthProvider = 'cognito-identity.amazonaws.com';
    var fbAuthProvider = 'graph.facebook.com';
    var authSource = '';

    switch (platform.toUpperCase()) {
        case 'ios'.toUpperCase(): authSource = 'native-app-ios'; break;
        case 'android'.toUpperCase():  authSource = 'native-app-android'; break;
    }

    Object.assign(config, {'hgauth_healthgrades_provider':hgAuthProvider});
    Object.assign(config, {'hgauth_facebook_provider':fbAuthProvider});
    Object.assign(config, {'hgauth_source':authSource});
}

function addGreyWolfServicesUrlBaseSettings(serviceVersion, environment, config) {
    var apiUrlBase = getApiUrlBase(environment);

    switch (environment.toUpperCase()) {
        case 'prod'.toUpperCase(): Object.assign(config, {'greywolf_services_url_base':apiUrlBase + '/greywolf/' + serviceVersion}); break;
        case 'test'.toUpperCase(): Object.assign(config, {'greywolf_services_url_base':apiUrlBase + '/greywolf/' + serviceVersion}); break;
        case 'dev'.toUpperCase() : Object.assign(config, {'greywolf_services_url_base':'http://s-dn-dv-tsla-02/svc/' + serviceVersion + '/greywolf'}); break;
    }
}

function getApiUrlBase(environment) {
    var apiUrlBase = 'https://apitest.healthgrades.com/api/v2_0';

    switch (environment.toUpperCase()) {
        case 'prod'.toUpperCase(): apiUrlBase = 'https://api.healthgrades.com/api/v2_0'; break;
        case 'test'.toUpperCase(): apiUrlBase = 'https://apitest.healthgrades.com/api/v2_0'; break;
        case 'dev'.toUpperCase() : apiUrlBase = 'https://apitest.healthgrades.com/api/v2_0'; break;
    }

    return apiUrlBase;
}

function addTermsAndConditionsSettings(environment, config) {

    Object.assign(config, {'hg_user_agreement_url':'https://www.healthgrades.com/content/user-agreement'});
    Object.assign(config, {'hg_privacy_policy_url':'https://www.healthgrades.com/content/privacy-policy'});
}
